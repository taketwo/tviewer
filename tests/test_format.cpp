#include "test.h"

#include <tviewer/tviewer.h>

TEST_CASE("Format with single argument is a pass-through") {
  REQUIRE(tviewer::detail::format("foo") == "foo");
}

TEST_CASE("Format with multiple arguments is a wrapper around boost::format/boost::str") {
  REQUIRE(tviewer::detail::format("%1% %2% %3%", 1, 2, 3) == "1 2 3");
  REQUIRE(tviewer::detail::format("number = %.2f", 0.42f) == "number = 0.42");
}
