#include "test.h"

#include <tviewer/tviewer.h>

TEST_CASE("Empty argument list is supported") {
  REQUIRE(tviewer::detail::collectWidgetNames().empty());
}

TEST_CASE("String-like widget names are supported") {
  SECTION("Char* is supported") {
    auto names = tviewer::detail::collectWidgetNames("foo");
    REQUIRE(names.size() == 1);
    REQUIRE(names.count("foo") == 1);
    names = tviewer::detail::collectWidgetNames("foo", "bar", "baz");
    REQUIRE(names.size() == 3);
    REQUIRE(names.count("foo") == 1);
    REQUIRE(names.count("bar") == 1);
    REQUIRE(names.count("baz") == 1);
  }
  SECTION("String is supported") {
    auto names = tviewer::detail::collectWidgetNames(std::string("foo"));
    REQUIRE(names.size() == 1);
    REQUIRE(names.count("foo") == 1);
    names = tviewer::detail::collectWidgetNames(std::string("foo"), std::string("bar"));
    REQUIRE(names.size() == 2);
    REQUIRE(names.count("foo") == 1);
    REQUIRE(names.count("bar") == 1);
  }
}

TEST_CASE("WidgetProperties objects are supported") {
  tviewer::TViewer viewer;
  auto foo = viewer.addPointCloud("foo");
  auto bar = viewer.addSpinner("bar");
  auto names = tviewer::detail::collectWidgetNames(foo, bar);
  REQUIRE(names.size() == 2);
  REQUIRE(names.count("foo") == 1);
  REQUIRE(names.count("bar") == 1);
}

TEST_CASE("Mix of WidgetProperties objects and string-like objects are supported") {
  tviewer::TViewer viewer;
  auto bar = viewer.addPointCloud("bar");
  auto names = tviewer::detail::collectWidgetNames(std::string("foo"), bar, "baz");
  REQUIRE(names.size() == 3);
  REQUIRE(names.count("foo") == 1);
  REQUIRE(names.count("bar") == 1);
  REQUIRE(names.count("baz") == 1);
}

