Internals
=========

The following classes are not a part of the public API, however are important
for the internal working of the *tviewer* library.

TODO: create a small table of contents here?

Widgets:
* a
* b
* c

Other:
* x
* y

.. doxygenclass:: tviewer::Widget
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. doxygenclass:: tviewer::WidgetState
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. doxygenclass:: tviewer::Property
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. doxygenclass:: tviewer::PropertyLock
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. doxygenclass:: tviewer::VisualizationWidget
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. doxygenclass:: tviewer::DataVisualizationWidget
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. doxygenclass:: tviewer::SpinnerWidget
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
