.. role:: hidden
    :class: hidden-section

Widgets
=======

Widgets are created and managed by TViewer. The user can get access only to
handles to these objects. The lifetime of the handle is independent of it's
corresponding widget. You may copy, store, or destroy handle objects.

Widget is an umbrella term for objects that have:
* internal state
* present in visualization window
* react to keyboard events

Widgets have unique name and are bound to a TViewer instance. They are created
through that instance and incapsulated inside. In fact, in user
code you can not even get access to widget objects, only to opaque handles
that provide a fluent interface for changing widget's properties.

Widgets are created using widget instantiation methods of TViewer. These methods
accept a single parameter--widget name--that has to be unique, and return a
handle to adjust widget properties.

Handles provide a fluent interface to set the properties of the associated
visualization object.

:hidden:`PointCloud`
~~~~~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::PointCloudWidgetProperties
   :members:

:hidden:`NormalCloud`
~~~~~~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::NormalCloudWidgetProperties
   :members:

:hidden:`PolyData`
~~~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::PolyDataWidgetProperties
   :members:

:hidden:`PrimitiveObjects`
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::PrimitiveObjectsWidgetProperties
   :members:

:hidden:`Text2D`
~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::Text2DWidgetProperties
   :members:

:hidden:`Spinner`
~~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::SpinnerWidgetProperties
   :members:

:hidden:`Toggle`
~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::ToggleWidgetProperties
   :members:

:hidden:`Trigger`
~~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::TriggerWidgetProperties
   :members:

:hidden:`PointSelectorWidget`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. doxygenclass:: tviewer::PointSelectorWidgetProperties
   :members:
