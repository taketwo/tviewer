#!/usr/bin/env python3

# Build the documentation.
# This script is adapted from fmtlib with additions from pipx.

import os
import sys
import argparse
from subprocess import check_call, CalledProcessError, Popen, PIPE


class Venv:
    def __init__(self, path="venv", verbose=False, python=sys.executable):
        self.root = path
        self._python = python
        self.bin_path = os.path.join(path, "bin")
        self.python_path = os.path.join(self.bin_path, "python")
        self.verbose = verbose
        self.create()

    def create(self):
        if not os.path.exists(self.root):
            check_call([self._python, "-m", "venv", self.root])
            self.upgrade_package("pip")

    def install(self, package, commit=None):
        """Install package using pip."""
        if commit:
            package = "git+https://github.com/{}.git@{}".format(package, commit)
        self._run_pip(["install", package])

    def upgrade_package(self, package):
        self._run_pip(["install", "--upgrade", package])

    def run(self, cmd):
        cmd[0] = os.path.join(self.bin_path, cmd[0])
        check_call(cmd)

    def _run_pip(self, cmd):
        cmd = [self.python_path, "-m", "pip"] + cmd
        if not self.verbose:
            cmd.append("-q")
        check_call(cmd)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--python-api", action="store_true", help="include Python API")
    parser.add_argument("--verbose", action="store_true", help="verbose")
    args = parser.parse_args()

    # Setup virtual environment
    venv = Venv(verbose=args.verbose)
    venv.install("sphinx")
    venv.install("sphinx_rtd_theme")
    venv.install("breathe")
    if args.python_api:
        venv.install("numpy")
        venv.install("numpydoc")

    # Run doxygen
    doc_dir = os.path.dirname(os.path.realpath(__file__))
    work_dir = "."
    include_dir = os.path.join(
        os.path.dirname(doc_dir), "tviewer", "include", "tviewer"
    )
    src_dir = os.path.join(os.path.dirname(doc_dir), "tviewer", "src")
    cmd = ["doxygen", "-"]
    p = Popen(cmd, stdin=PIPE)
    doxyxml_dir = os.path.join(work_dir, "doxyxml")
    p.communicate(
        input=r"""
      PROJECT_NAME      = tviewer
      GENERATE_LATEX    = NO
      GENERATE_MAN      = NO
      GENERATE_RTF      = NO
      CASE_SENSE_NAMES  = NO
      INLINE_INHERITED_MEMB = YES
      INPUT             = {0} {0}/widgets {1} {1}/widgets
      QUIET             = YES
      JAVADOC_AUTOBRIEF = YES
      AUTOLINK_SUPPORT  = YES
      MARKDOWN_SUPPORT  = YES
      GENERATE_HTML     = NO
      GENERATE_XML      = YES
      XML_OUTPUT        = {2}
      ALIASES           = "rst=\verbatim embed:rst"
      ALIASES          += "endrst=\endverbatim"
      MACRO_EXPANSION   = YES
    """.format(
            include_dir, src_dir, doxyxml_dir
        ).encode(
            "UTF-8"
        )
    )
    if p.returncode != 0:
        raise CalledProcessError(p.returncode, cmd)

    # Run sphinx with breathe
    version = "dev"
    cmd = (
        "sphinx-build -Dbreathe_projects.tviewer={0} -Dversion={1} -Drelease={1} "
        "-Aversion={1} -b html {4} {2} {3}".format(
            os.path.abspath(doxyxml_dir),
            version,
            doc_dir,
            os.path.join(work_dir, "html"),
            "-t python" if args.python_api else "",
        )
    )
    venv.run(cmd.split())
