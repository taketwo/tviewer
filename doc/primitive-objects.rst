.. role:: hidden
    :class: hidden-section

Primitive objects
=================

Primitive object widget can visualize a collection of such objects.

:hidden:`Arrow`
~~~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::Arrow
   :members:

:hidden:`Axes`
~~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::Axes
   :members:

:hidden:`Box`
~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::Box
   :members:

:hidden:`Cone`
~~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::Cone
   :members:

:hidden:`Cylinder`
~~~~~~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::Cylinder
   :members:

:hidden:`LineSegment`
~~~~~~~~~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::LineSegment
   :members:

:hidden:`Plane`
~~~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::Plane
   :members:

:hidden:`Sphere`
~~~~~~~~~~~~~~~~

.. doxygenstruct:: tviewer::Sphere
   :members:
