=======
tviewer
=======

*tviewer* is a small visualization library built on top of PCL Visualizer.

.. toctree::
   :maxdepth: 1
   :hidden:

   structure

.. toctree::
   :maxdepth: 1
   :caption: C++ API reference

   tviewer
   widgets
   primitive-objects
   exceptions
   internals

.. only:: python

   .. toctree::
      :maxdepth: 1
      :caption: Python API reference

      python/point-cloud
      python/tviewer
      python/widgets
