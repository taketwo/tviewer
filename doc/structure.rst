Structuring your application
----------------------------

tviewer supports two approaches to structuring your application, let's call them
process-visualize and interactive-pipeline.

As the name suggests, the process-visualize type of application consists of two logical parts:
processing and visualization. In the first part all necessary computations are
done, and in the second part the results are presented to the user for visual
evaluation.

The second type of application interweaves computations and user interactions.
The visualization setup is altered in the process (new visualization objects are
added, and no longer relevant objects are removed).
