Exceptions
==========

.. doxygenclass:: tviewer::DuplicateWidgetNameException

.. doxygenclass:: tviewer::WidgetNotFoundException

.. doxygenclass:: tviewer::WidgetTypeMismatchException
