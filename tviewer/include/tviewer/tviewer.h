/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <chrono>
#include <memory>
#include <string>
#include <vector>

#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <tviewer/fwd.h>

#include <tviewer/color.h>
#include <tviewer/exceptions.h>
#include <tviewer/kwargs.h>
#include <tviewer/primitive_objects.h>
#include <tviewer/widgets/correspondences_widget_properties.h>
#include <tviewer/widgets/normal_cloud_widget_properties.h>
#include <tviewer/widgets/point_cloud_widget_properties.h>
#include <tviewer/widgets/point_selector_widget_properties.h>
#include <tviewer/widgets/poly_data_widget_properties.h>
#include <tviewer/widgets/polygons_widget_properties.h>
#include <tviewer/widgets/primitive_objects_widget_properties.h>
#include <tviewer/widgets/spinner_widget_properties.h>
#include <tviewer/widgets/text_2d_widget_properties.h>
#include <tviewer/widgets/toggle_widget_properties.h>
#include <tviewer/widgets/trigger_widget_properties.h>
#include <tviewer/widgets/variant_widget_properties.h>
#include <tviewer/detail/format.hpp>

namespace tviewer {

class TViewerImpl;

/// This is the main class of the library.
///
/// TViewer manages a collection of "widgets" the visualization objects (e.g. point clouds,
/// polygons) that you want to display, user interaction, and the underlying PCL Visualizer.
///
/// Terminology:
///  * _scene_ a configuration of visualization objects and their properties
///  * _visualizer_ is an instance of PCL Visualizer used to display the scene.
///  * _visualizer state_ the state of the PCL Visualizer (displayed shapes and their properties).
///  * to synchronize visualization means to add/remove PCL shapes such that the visualization
///  presented to the user matches the scene
class TViewer {
 public:
  /// Construct a new TViewer instance.
  /// \note This does not create a visualization window. Use run() to do that.
  TViewer();

  /// Construct a new TViewer instance.
  /// This parses relevant command line arguments and configures the TViewer instance.
  /// \note This does not create a visualization window. Use run() to do that.
  TViewer(int argc, const char** argv);

  ~TViewer();

  /// \name Widget creation
  ///
  /// Functions in this group create new widgets and add them to the scene.
  ///
  /// Each of these functions takes a single required parameter (widget name) and returns a proxy
  /// "properties" object that can be used to manipulate the created widget's properties. These
  /// objects have a fluent interface, allowing to configure widgets using a chain of method calls.
  ///
  /// If a widget with a given name already exists and has a matching type, then the functions
  /// return a no-op "properties" proxy that do not do anything. This behavior allows the user to
  /// write visualization setup code that can be called multiple times. On the first execution the
  /// widget adding functions will return working proxies that will update widgets' properties. On
  /// the subsequent executions the adding functions will return no-op proxies and the widgets will
  /// stay in the same state.
  ///
  /// Example usage:
  ///
  /// \code
  /// viewer.addPointCloud("points")  // create a point cloud widget named "points"
  ///       .pointSize(4)             // set point size to 4 units
  ///       .colors("x")              // colorize using "x" data field
  ///       .data(pointcloud);        // set object pointcloud as data source
  /// \endcode

  ///@{

  /// Create a correspondences widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to set correspondences data and update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a
  /// correspondences widget
  CorrespondencesWidgetProperties addCorrespondences(const std::string& name);

  /// Create a normal cloud widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to set normal cloud data and update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a normal
  /// cloud widget
  NormalCloudWidgetProperties addNormalCloud(const std::string& name);

  /// Create a point cloud widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to set point cloud data and update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a point
  /// cloud widget
  PointCloudWidgetProperties addPointCloud(const std::string& name);

  /// Create a point selector widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a point
  /// selector widget
  PointSelectorWidgetProperties addPointSelector(const std::string& name);

  /// Create a poly data widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to set poly data and update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a poly
  /// data widget
  PolyDataWidgetProperties addPolyData(const std::string& name);

  /// Create a polygons widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to set polygons and update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a
  /// polygons widget
  PolygonsWidgetProperties addPolygons(const std::string& name);

  /// Create a primitive objects widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to set point cloud data and update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a
  /// primitive objects widget
  PrimitiveObjectsWidgetProperties addPrimitiveObjects(const std::string& name);

  /// Create a spinner widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a spinner
  /// widget
  SpinnerWidgetProperties addSpinner(const std::string& name);

  /// Create a 2d text widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to set poly data and update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a 2d text
  /// widget
  Text2DWidgetProperties addText2D(const std::string& name);

  /// Create a toggle widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a toggle
  /// widget
  ToggleWidgetProperties addToggle(const std::string& name);

  /// Create a trigger widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a trigger
  /// widget
  TriggerWidgetProperties addTrigger(const std::string& name);

  /// Create a variant widget.
  /// \param[in] name Name of the new widget, has to be unique.
  /// \return An object that can be used to update widget properties.
  /// \throw WidgetTypeMismatchException if a widget with the given name exists but is not a variant
  /// widget
  VariantWidgetProperties addVariant(const std::string& name);

  ///@}

  /// \name Widget retrieval
  ///
  /// Functions in this group retrieve widgets that are present in the scene.
  ///
  /// Each of these functions takes a single required parameter (widget name) and returns a proxy
  /// "properties" object that can be used to manipulate the widget's properties. These objects have
  /// a fluent interface, allowing to configure widgets using a chain of method calls.

  ///@{

  /// Get a handle to manipulate properties of a given correspondences widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a correspondences
  /// widget
  CorrespondencesWidgetProperties getCorrespondences(const std::string& name);

  /// Get a handle to manipulate properties of a given normal cloud widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a normal cloud
  /// widget
  NormalCloudWidgetProperties getNormalCloud(const std::string& name);

  /// Get a handle to manipulate properties of a given point cloud widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a point cloud widget
  PointCloudWidgetProperties getPointCloud(const std::string& name);

  /// Get a handle to manipulate properties of a given point selector widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a point selector
  /// widget
  PointSelectorWidgetProperties getPointSelector(const std::string& name);

  /// Get a handle to manipulate properties of a given poly data widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a poly data widget
  PolyDataWidgetProperties getPolyData(const std::string& name);

  /// Get a handle to manipulate properties of a given polygons widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a polygons widget
  PolygonsWidgetProperties getPolygons(const std::string& name);

  /// Get a handle to manipulate properties of a given primitive objects widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a primitive objects
  /// widget
  PrimitiveObjectsWidgetProperties getPrimitiveObjects(const std::string& name);

  /// Get a handle to manipulate properties of a given spinner widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a spinner widget
  SpinnerWidgetProperties getSpinner(const std::string& name);

  /// Get a handle to manipulate properties of a given 2d text widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a 2d text widget
  Text2DWidgetProperties getText2D(const std::string& name);

  /// Get a handle to manipulate properties of a given toggle widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a toggle widget
  ToggleWidgetProperties getToggle(const std::string& name);

  /// Get a handle to manipulate properties of a given trigger widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a trigger widget
  TriggerWidgetProperties getTrigger(const std::string& name);

  /// Get a handle to manipulate properties of a given variant widget.
  /// \param[in] name name of the widget
  /// \return an object that can be used to update widget properties.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  /// \throw WidgetTypeMismatchException if a widget with the given name is not a variant widget
  VariantWidgetProperties getVariant(const std::string& name);

  ///@}

  /// \name Widget removal
  ///
  /// This group contains functions to remove widgets singly or in batch.

  ///@{

  /// Remove a widget with a given name from the viewer.
  /// \param[in] name name of the widget
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  void remove(const std::string& name);

  /// Remove all widgets from the viewer.
  void removeAll();

  ///@}

  /// \name Widget existence querying
  ///
  /// This group contains a single function to check if a widget exists.

  ///@{

  /// Check if a widget with a given name is contained in the viewer.
  /// \param[in] name name of the widget
  bool contains(const std::string& name) const;

  ///@}

  /// \name Enable/disable widget groups
  ///
  /// TViewer supports assigning widgets to named groups. A group of widgets can be either in the
  /// enabled or disabled state. The former means that the widgets from this group behave as normal:
  ///   * receive keyboard and state change events;
  ///   * show up in help;
  ///   * show up in visualizer (if their visibility is on).
  /// The latter means the opposite, i.e. the widgets of the disabled group effectively disappear.
  /// Note, however, that they are not removed and maintain their state. If the group is re-enabled,
  /// they will show up again with exactly same data and properties as before.
  ///
  /// Upon creation, all widgets are assigned to the "default" group, which is enabled. The groups
  /// do not need to be explicitly registered. Calling one of the functions below transparently
  /// creates a group if it does not exist yet.

  ///@{

  /// Enable a group with a given name.
  /// \param[in] group name of the group
  void enableGroup(const std::string& group);

  /// Disable a group with a given name.
  /// \param[in] group name of the group
  void disableGroup(const std::string& group);

  ///@}

  /// Set intrinsics of the virtual camera used for rendering.
  ///
  /// This method is a convenience wrapper around PCL Visualizer's setCameraParameters() function.
  ///
  /// If the visualization window does not exist yet, the camera intrinsics are stored internally
  /// and are applied once the window is created. At this point the stored intrinsics are cleared,
  /// implying that subsequent re-creations of the visualization window will again use the default
  /// camera intrinsics.
  void setCameraIntrinsics(const Eigen::Matrix3d& intrinsics);

  /// Set pose of the virtual camera used for rendering.
  ///
  /// This method is a convenience wrapper around PCL Visualizer's setCameraPosition() function. It
  /// automatically applies rotation to flip the direction of the up vector to match the convention
  /// used in rendering engines (i.e. +Y points up).
  ///
  /// If the visualization window does not exist yet, the camera pose is stored internally and is
  /// applied once the window is created. At this point the stored pose is cleared, implying that
  /// subsequent re-creations of the visualization window will again use the default camera pose.
  void setCameraPose(const Eigen::Isometry3d& pose);

  /// Set intrinsics and extrinsics of the virtual camera used for rendering.
  ///
  /// This method is a convenience wrapper around PCL Visualizer's setCameraParameters() function.
  /// It automatically applies rotation to flip the direction of the up vector to match the
  /// convention used in rendering engines (i.e. +Y points up).
  ///
  /// If the visualization window does not exist yet, the camera parameters are stored internally
  /// and are applied once the window is created. At this point the stored parameters are cleared,
  /// implying that subsequent re-creations of the visualization window will again use the default
  /// camera parameters.
  void setCameraParameters(const Eigen::Matrix3d& intrinsics, const Eigen::Isometry3d& extrinsics);

  /// Add a new viewport.
  void addViewport(double xmin, double ymin, double xmax, double ymax,
                   bool separate_camera = false);

  /// Fetch new data for a given widget and synchronize visualization.
  /// \throw WidgetNotFoundException if a widget with the given name does not exist
  void update(const std::string& name);

  enum class KeepWindowOpen {
    NO,
    YES,
  };

  /// Create visualization window and start user interaction loop.
  ///
  /// This function will block program execution until the user terminates visualization.
  ///
  /// \param[in] keep_open keep the visualization window open after the user terminated
  /// visualization
  void run(KeepWindowOpen keep_open = KeepWindowOpen::NO);

  /// Create visualization window and start user interaction loop.
  ///
  /// This function will block program execution for a given duration (or until the user terminates
  /// visualization).
  ///
  /// \param[in] duration how long to run the interaction loop (zero duration means forever)
  /// \param[in] keep_open keep the visualization window open after the specified time is elapsed or
  /// the user terminated visualization
  void run(std::chrono::duration<float> duration, KeepWindowOpen keep_open = KeepWindowOpen::NO);

  /// Create visualization window and start user interaction loop.
  ///
  /// This function will block program execution until the user terminates visualization.
  ///
  /// \param[in] keep_open keep the visualization window open after the user terminated
  /// visualization
  [[deprecated("use overload with KeepWindowOpen-typed parameter instead")]] void run(
      bool keep_open);

  /// Create visualization window and start user interaction loop.
  ///
  /// This function will block program execution for a given duration (or until the user terminates
  /// visualization).
  ///
  /// \param[in] duration how long to run the interaction loop (zero duration means forever)
  /// \param[in] keep_open keep the visualization window open after the specified time is elapsed or
  ///            the user terminated visualization
  [[deprecated("use overload with KeepWindowOpen-typed parameter instead")]] void run(
      std::chrono::duration<float> duration, bool keep_open);

  void stop();

  /// \name Point selection
  ///
  /// Functions in this group allow the user to select points in the visualizer.

  ///@{

  /// Wait for a single point to be selected and output its index.
  /// \return \c true if a point was selected, \c false if "Escape" was pressed
  bool waitPointSelected(size_t& point_index);

  /// Wait for set of points to be selected and output their indices.
  ///
  /// "Escape" keypress finishes the point selection process.
  ///
  /// The selected points are highlighted in the visualizer to provide feedback to the user. A click
  /// on an already selected point de-selects it.
  ///
  /// \param[out] indices vector of selected point indices
  ///
  /// \return number of points selected
  unsigned int waitPointsSelected(std::vector<int>& indices);

  ///@}

  /// Set title of the visualization window to a given string.
  void setWindowTitle(const std::string& title);

  /// Set title of the visualization window to the result of formatting provided arguments.
  ///
  /// This function is a convenience wrapper that uses format() to construct the title string.
  template <typename... Args>
  void setWindowTitle(Args&&... args) {
    const std::string title = detail::format(std::forward<Args>(args)...);
    setWindowTitle(title);
  }

  /// Set background color of the visualization window.
  void setBackgroundColor(Color color);

  /// Enable or disable display of FPS count in the bottom-left corner.
  void showFPS(bool show);

  void feedKey(const std::string& key);

  std::weak_ptr<pcl::visualization::PCLVisualizer> getVisualizer() const;

 private:
  std::unique_ptr<TViewerImpl> p;
};

/// Keep the visualization window open after the user terminated visualization run.
constexpr auto KeepWindowOpen = TViewer::KeepWindowOpen::YES;

}  // namespace tviewer
