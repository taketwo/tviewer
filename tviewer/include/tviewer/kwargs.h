/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include <boost/any.hpp>

#include <iostream>

namespace tviewer {

class Kwargs {
 public:
  void add(const std::string& key, const boost::any& value) {
    state_dict_.emplace(key, value);
  }

  /// Get the value associated with a given key, casting it to a given type.
  /// \throw KeyNotFoundException if non-existent key was requested
  template <typename T>
  T get(const std::string& key) const {
    return boost::any_cast<T>(get(key));
  }

  /// Get the value associated with a given key, casting it to a given type.
  /// If the requested key does not exist, the provided default value will be returned.
  template <typename T>
  T get(const std::string& key, const T& default_value) const {
    if (state_dict_.count(key))
      return boost::any_cast<T>(get(key));
    else
      return default_value;
  }

  std::vector<std::string> keys() const {
    std::vector<std::string> keys;
    for (const auto& v : state_dict_)
      keys.push_back(v.first);
    return keys;
  }

 private:
  /// Get the value associated with a given key.
  /// \throw KeyNotFoundException if non-existent key was requested
  boost::any get(const std::string& key) const;

  std::unordered_map<std::string, boost::any> state_dict_;
};

}  // namespace tviewer
