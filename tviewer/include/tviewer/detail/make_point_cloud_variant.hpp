/******************************************************************************
 * Copyright (c) 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <boost/mpl/push_front.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/variant/variant.hpp>

#include <pcl/point_cloud.h>

namespace tviewer {

namespace detail {

// A meta-function that creates a constant pointer to a point cloud of a given type.
struct MakePointCloud {
  template <class PointT>
  struct apply {
    using type = typename pcl::PointCloud<PointT>::ConstPtr;
  };
};

/// A meta-function that creates a variant containing constant pointers to points clouds for each
/// point type in a given vector, plus a null pointer.
template <typename Types>
struct MakePointCloudVariant {
  using PointClouds = typename boost::mpl::transform<Types, MakePointCloud>::type;
  using PointCloudsAndNullptr = typename boost::mpl::push_front<PointClouds, std::nullptr_t>::type;
  using type = typename boost::make_variant_over<PointCloudsAndNullptr>::type;
};

}  // namespace detail

}  // namespace tviewer
