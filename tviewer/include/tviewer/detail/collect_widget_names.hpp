/******************************************************************************
 * Copyright (c) 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <set>
#include <string>

namespace tviewer {

template <typename T>
class WidgetProperties;

namespace detail {

template <typename T>
std::enable_if_t<std::is_convertible<T, std::string>::value, std::string> getWidgetName(
    const T& obj) {
  return obj;
}

template <typename T>
std::enable_if_t<std::is_base_of<WidgetProperties<T>, T>::value, std::string> getWidgetName(
    const T& obj) {
  return obj.name();
}

template <typename = void>
void collectWidgetNamesImpl(std::set<std::string>& /* names */) {}

template <typename Arg, typename... Args>
void collectWidgetNamesImpl(std::set<std::string>& names, Arg arg, Args... args) {
  names.insert(getWidgetName(arg));
  collectWidgetNamesImpl(names, args...);
}

template <typename... Args>
std::set<std::string> collectWidgetNames(Args... args) {
  std::set<std::string> names;
  collectWidgetNamesImpl(names, args...);
  return names;
}

}  // namespace detail

}  // namespace tviewer
