/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <functional>
#include <vector>

#include <boost/variant.hpp>

#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include <Eigen/Geometry>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <tviewer/detail/make_point_cloud_variant.hpp>

namespace tviewer {

class Color;

using Colors = std::vector<Color>;

class Kwargs;

using Callback = std::function<void()>;

template <typename T>
using ValueCallback = std::function<void(T value)>;

using KwargsCallback = std::function<void(const Kwargs&)>;

template <typename T>
using GetDataCallback = std::function<T()>;

template <typename T>
using GetDataKwargsCallback = std::function<T(const Kwargs&)>;

using PointTypes =
    boost::mpl::vector<pcl::PointXYZ, pcl::PointXYZI, pcl::PointXYZL, pcl::PointNormal,
                       pcl::PointXYZRGB, pcl::PointXYZRGBA, pcl::PointXYZRGBL, pcl::PointXYZINormal,
                       pcl::PointXYZLNormal, pcl::PointXYZRGBNormal>;

/// A meta-function to check if a given type is a point.
template <typename T>
struct is_point : boost::mpl::contains<PointTypes, T> {};

/// Shared pointer to a const cloud of points.
using PointCloudConstPtr = detail::MakePointCloudVariant<PointTypes>::type;

using NormalTypes =
    boost::mpl::vector<pcl::PointNormal, pcl::PointXYZRGBNormal, pcl::PointXYZINormal,
                       pcl::PointXYZLNormal, pcl::PointSurfel>;

/// Shared pointer to a const cloud of normals (with points).
using NormalCloudConstPtr = detail::MakePointCloudVariant<NormalTypes>::type;

/// Shared pointer to poly data.
using PolyDataPtr = vtkSmartPointer<vtkPolyData>;

struct Correspondences;

struct Arrow;

struct Axes;

struct Box;

struct Circle;

struct Cone;

struct Cylinder;

struct LineSegment;

struct Plane;

struct Sphere;

struct Text;

/// A primitive object.
using PrimitiveObject =
    boost::variant<Arrow, Axes, Box, Circle, Cone, Cylinder, LineSegment, Plane, Sphere, Text>;

/// A vector of primitive objects.
using PrimitiveObjects = std::vector<PrimitiveObject>;

/// A polygon (nothing else but a pointcloud).
using Polygon = PointCloudConstPtr;

/// A vector of polygons.
using Polygons = std::vector<Polygon>;

class StateChangeEvent;

using Transform = Eigen::Isometry3f;

class TViewer;
class TViewerImpl;

}  // namespace tviewer
