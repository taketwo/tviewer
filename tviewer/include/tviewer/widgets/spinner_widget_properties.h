/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>

#include <tviewer/widgets/callback_widget_properties.h>

namespace tviewer {

class SpinnerWidget;

/// Spinner widget properties.
///
/// Spinner widget incapsulates a ``double`` variable, providing an interface to increment and
/// decrement it.
class SpinnerWidgetProperties : public CallbackWidgetProperties<SpinnerWidgetProperties, double> {
 public:
  double value() const;

  /// Set key used to increment the spinner value.
  /// The decrement is automatically assigned to Ctrl+key. In order to use a different decrement
  /// key, use another overload.
  SpinnerWidgetProperties& key(const std::string& key_increment);

  /// Set keys used to increment and decrement the spinner value.
  SpinnerWidgetProperties& key(const std::string& key_increment, const std::string& key_decrement);

  SpinnerWidgetProperties& value(double value);

  SpinnerWidgetProperties& step(double step);

  /// Set spinner range.
  /// The range boundaries are inclusive and the order of passing them does not matter; the smaller
  /// value will be used as the lower boundary and the larger value will be used as the upper
  /// boundary. After the range is set, the spinner value will be coerced to the new range.
  SpinnerWidgetProperties& range(double min, double max);

  /// Set spinner range such that it can be used as an index into a container of the given length.
  /// The range will be set to [0, length - 1] and the spinner value will be coerced to the new
  /// range. If the length is 0, the range and the value will be set to NaNs, effectively disabling
  /// the spinner.
  SpinnerWidgetProperties& range(size_t length);

  SpinnerWidgetProperties& wrapAround(bool wrap_around = true);

 private:
  SpinnerWidgetProperties(const std::shared_ptr<SpinnerWidget>& object);

  std::weak_ptr<SpinnerWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class CallbackWidgetProperties;
};

}  // namespace tviewer
