/******************************************************************************
 * Copyright (c) 2014, 2018, 2024 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <string>

#include <tviewer/fwd.h>

#include <tviewer/widgets/widget_properties.h>

namespace tviewer {

template <typename SelfT>
class VisualizationWidgetProperties : public WidgetProperties<SelfT> {
 public:
  /// Set toggle key
  SelfT& key(const std::string& key);

  /// Set viewport
  SelfT& viewport(unsigned int viewport);

  /// Get visibility of the widget.
  bool visibility() const;

  /// Set visibility of the widget.
  SelfT& visibility(bool visibility);

  /// Show the widget.
  /// This is equivalent to calling `visibility(true)`.
  SelfT& show() {
    return visibility(true);
  }

  /// Hide the widget.
  /// This is equivalent to calling `visibility(false)`.
  SelfT& hide() {
    return visibility(false);
  }

  SelfT& transform(const Transform& transform);

  SelfT& transform(const Eigen::Isometry3d& transform);
};

}  // namespace tviewer
