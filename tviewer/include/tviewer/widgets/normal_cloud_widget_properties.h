/******************************************************************************
 * Copyright (c) 2014, 2018, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>

#include <tviewer/fwd.h>

#include <tviewer/widgets/data_visualization_widget_properties.h>

namespace tviewer {

class NormalCloudWidget;

class NormalCloudWidgetProperties
: public DataVisualizationWidgetProperties<NormalCloudWidgetProperties, NormalCloudConstPtr> {
 public:
  /// Set normal cloud density.
  ///
  /// Each \a density -th normal will be displayed. In order to display all normals, set this
  /// property to 1. The default value is 10.
  ///
  /// This corresponds to the `level` property of `PCLVisualizer::addPointCloudNormals()`.
  NormalCloudWidgetProperties& density(unsigned int density);

  /// Set normal length.
  ///
  /// This controls the length of the arrows used to display normals. Length should be given in same
  /// units as point coordinates. The default value is 0.01.
  ///
  /// This corresponds to the `scale` property of `PCLVisualizer::addPointCloudNormals()`.
  NormalCloudWidgetProperties& length(float length);

 private:
  NormalCloudWidgetProperties(const std::shared_ptr<NormalCloudWidget>& object);

  std::weak_ptr<NormalCloudWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class VisualizationWidgetProperties;
  friend class DataVisualizationWidgetProperties;
};

}  // namespace tviewer
