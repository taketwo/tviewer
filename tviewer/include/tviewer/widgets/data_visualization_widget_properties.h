/******************************************************************************
 * Copyright (c) 2014, 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <set>

#include <tviewer/fwd.h>
#include <tviewer/widgets/visualization_widget_properties.h>
#include <tviewer/detail/collect_widget_names.hpp>

namespace tviewer {

template <typename SelfT, typename DataT>
class DataVisualizationWidgetProperties : public VisualizationWidgetProperties<SelfT> {
 public:
  /// Set data
  SelfT& data(const DataT& data);

  /// Set get data callback
  SelfT& dataCallback(const GetDataCallback<DataT>& callback);

  /// Set get data callback
  SelfT& dataCallback(const GetDataKwargsCallback<DataT>& callback);

  SelfT& dependencies(const std::set<std::string>& dependencies);

  // Each argument should be either a widget, or a widget name.
  template <typename... Args>
  SelfT& dependencies(Args... args) {
    return dependencies(detail::collectWidgetNames(args...));
  }
};

}  // namespace tviewer
