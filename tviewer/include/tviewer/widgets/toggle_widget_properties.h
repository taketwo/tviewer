/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>

#include <tviewer/widgets/callback_widget_properties.h>

namespace tviewer {

class ToggleWidget;

/// Toggle widget properties.
///
/// Toggle widget encapsulates a boolean flag. The user can toggle the state of this flag either
/// using a keyboard shortcut or programmatically. If set, a callback will be executed every time
/// the state of the flag changes.
class ToggleWidgetProperties : public CallbackWidgetProperties<ToggleWidgetProperties, bool> {
 public:
  /// Get the value of the flag.
  bool value() const;

  /// Bool cast (effectively get the value of the flag).
  operator bool() const {
    return value();
  }

  /// Set the value of the flag.
  ToggleWidgetProperties& value(bool value);

  /// Set the key used to toggle the flag.
  ToggleWidgetProperties& key(const std::string& key);

 private:
  ToggleWidgetProperties(const std::shared_ptr<ToggleWidget>& object);

  std::weak_ptr<ToggleWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class CallbackWidgetProperties;
};

}  // namespace tviewer
