/******************************************************************************
 * Copyright (c) 2020 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>

#include <tviewer/fwd.h>

#include <tviewer/widgets/data_visualization_widget_properties.h>

namespace tviewer {

class PolygonsWidget;

class PolygonsWidgetProperties
: public DataVisualizationWidgetProperties<PolygonsWidgetProperties, Polygons> {
 public:
  /// Set point size used to visualize polygon vertices.
  PolygonsWidgetProperties& pointSize(unsigned int point_size);

  /// Set colors for polygon edges and vertices.
  ///
  /// All edges and vertices of a single polygon are visualized with the same color. The color that
  /// a polygon will have is decided by cycling through the provided vector of colors. If a single
  /// color is provided, then all polygons will have the same color. In the case when no colors are
  /// provided (i.e. the vector is empty), the colors are assigned automatically from Glasbey LUT.
  /// This is the default upon widget creation.
  PolygonsWidgetProperties& colors(const std::vector<Color>& colors);

  /// Set data (single polygon).
  PolygonsWidgetProperties& data(const Polygon& polygon);

  // Prevent base-class data() methods from being hidden.
  using DataVisualizationWidgetProperties<PolygonsWidgetProperties, Polygons>::data;

 private:
  PolygonsWidgetProperties(const std::shared_ptr<PolygonsWidget>& object);

  std::weak_ptr<PolygonsWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class VisualizationWidgetProperties;
  friend class DataVisualizationWidgetProperties;
};

}  // namespace tviewer
