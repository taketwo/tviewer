/******************************************************************************
 * Copyright (c) 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>
#include <vector>

#include <tviewer/widgets/callback_widget_properties.h>

namespace tviewer {

class VariantWidget;

/// Variant widget properties.
///
/// Variant widget incapsulates a string variable that can take on a set of predefined values
/// ("variants"), providing an interface to change the value.
class VariantWidgetProperties
: public CallbackWidgetProperties<VariantWidgetProperties, std::string> {
 public:
  std::string value() const;

  /// Set key used to advance through the variant values.
  ///
  /// This sets a key only for forward direction, the backward direction will have no key.
  VariantWidgetProperties& key(const std::string& key_next);

  /// Set keys used to alternate through the variant values.
  VariantWidgetProperties& key(const std::string& key_next, const std::string& key_previous);

  /// Set current value, has to be one of the variants.
  VariantWidgetProperties& value(const std::string& value);

  /// Set current value by providing an index into variants list.
  VariantWidgetProperties& valueIndex(size_t value_index);

  VariantWidgetProperties& variants(const std::vector<std::string>& variants);

 private:
  VariantWidgetProperties(const std::shared_ptr<VariantWidget>& object);

  std::weak_ptr<VariantWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class CallbackWidgetProperties;
};

}  // namespace tviewer
