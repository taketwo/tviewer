/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <string>

#include <tviewer/fwd.h>
#include <tviewer/widgets/widget_properties.h>

namespace tviewer {

template <typename SelfT, typename ValueType>
class CallbackWidgetProperties : public WidgetProperties<SelfT> {
 public:
  /// Set a callback to be executed when the internal state of the widget has changed.
  /// This sets a parameterless callback.
  SelfT& callback(const Callback& callback);

  // Set a callback to be executed when the internal state of the widget has changed.
  // The callback is specified as a pointer to an object and its member function (parameterless).
  // This sets a parameterless callback.
  // Note that this is not formatted as a Doxygen comment because Breathe/Sphinx can not handle
  // this.
  template <typename R>
  [[deprecated("use callback(const Callback&) with capturing lambda instear")]] SelfT& callback(
      R* object, void (R::*method)()) {
    callback(static_cast<Callback>(std::bind(method, object)));
  }

  /// Set a callback to be executed when the internal state of the widget has changed.
  /// This sets a callback with a single parameter--widget state.
  SelfT& callback(const ValueCallback<ValueType>& callback);

  // Set a callback to be executed when the internal state of the widget has changed.
  // The callback is specified as a pointer to an object and its member function (with a single
  // parameter--widget state).
  // Note that this is not formatted as a Doxygen comment because Breathe/Sphinx can not handle
  // this.
  template <typename R>
  [[deprecated(
      "use callback(const ValueCallback<ValueType>&) with capturing lambda instead")]] SelfT&
  callback(R* object, void (R::*method)(const ValueType&)) {
    callback(
        static_cast<ValueCallback<ValueType>>(std::bind(method, object, std::placeholders::_1)));
  }

  /// Set a callback to be executed when the internal state of the widget has changed.
  /// This sets a kwargs callback that receives complete state.
  SelfT& callback(const KwargsCallback& callback);

  // Set a callback to be executed when the internal state of the widget has changed.
  // The callback is specified as a pointer to an object and its member function (with a single
  // Kwargs argument). This sets a kwargs callback that receives complete state. Note that this is
  // not formatted as a Doxygen comment because Breathe/Sphinx can not handle this.
  template <typename R>
  [[deprecated("use callback(const KwargsCallback&) with capturing lambda instead")]] SelfT&
  callback(R* object, void (R::*method)(const Kwargs&)) {
    callback(static_cast<KwargsCallback>(std::bind(method, object, std::placeholders::_1)));
  }
};

}  // namespace tviewer
