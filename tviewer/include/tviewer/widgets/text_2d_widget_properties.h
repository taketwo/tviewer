/******************************************************************************
 * Copyright (c) 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>

#include <tviewer/fwd.h>

#include <tviewer/widgets/data_visualization_widget_properties.h>
#include <tviewer/detail/format.hpp>

namespace tviewer {

class Text2DWidget;

class Text2DWidgetProperties
: public DataVisualizationWidgetProperties<Text2DWidgetProperties, std::string> {
 public:
  /// Text position in the window.
  using Position = std::pair<unsigned int, unsigned int>;

  /// Set text position.
  ///
  /// Position is the distance in pixels along x and y axes from the bottom-left corner of the
  /// window to the bottom-left corner of the text.
  Text2DWidgetProperties& position(Position position);

  /// Set text position.
  ///
  /// Position is the distance in pixels along x and y axes from the bottom-left corner of the
  /// window to the bottom-left corner of the text.
  Text2DWidgetProperties& position(unsigned int position_x, unsigned int position_y) {
    return position({position_x, position_y});
  }

  /// Set text color.
  Text2DWidgetProperties& color(Color color);

  /// Set font size.
  Text2DWidgetProperties& fontSize(unsigned int font_size);

  /// Set text to the result of formatting provided arguments.
  template <typename... Args>
  Text2DWidgetProperties& text(Args&&... args) {
    const std::string text = detail::format(std::forward<Args>(args)...);
    return data(text);
  }

 private:
  Text2DWidgetProperties(const std::shared_ptr<Text2DWidget>& object);

  std::weak_ptr<Text2DWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class VisualizationWidgetProperties;
  friend class DataVisualizationWidgetProperties;
};

}  // namespace tviewer
