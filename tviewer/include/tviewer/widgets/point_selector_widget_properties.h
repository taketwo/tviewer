/******************************************************************************
 * Copyright (c) 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>
#include <vector>

#include <tviewer/widgets/callback_widget_properties.h>

namespace tviewer {

class PointSelectorWidget;

/// Point selector widget properties
///
/// Point selector widget observes point picking events and allows to select single or multiple
/// points. Once selection is complete, the selected point indices are passed to a user-provided
/// callback.
///
/// When a selection is considered complete depends on how the widget is configured.
class PointSelectorWidgetProperties
: public CallbackWidgetProperties<PointSelectorWidgetProperties, std::vector<int>> {
 public:
  using PointIndices = std::vector<int>;

  /// Get currently selected point indices.
  PointIndices indices() const;

  /// Get currently selected points;
  pcl::PointCloud<pcl::PointXYZ>::ConstPtr points() const;

  /// Set how many points should be selected in order to complete the selection.
  /// If set to zero, the selection is unlimited in size and is considered complete only when the
  /// user presses a trigger.
  PointSelectorWidgetProperties& numPoints(size_t num_points);

  /// Enable single point selection mode.
  /// This is equivalent to calling ``numPoints(1)``.
  PointSelectorWidgetProperties& single() {
    return numPoints(1);
  }

  /// Enable unlimited multiple point selection mode.
  /// This is equivalent to calling ``numPoints(0)``.
  PointSelectorWidgetProperties& multiple() {
    return numPoints(0);
  }

  /// Set key used to end point selection.
  PointSelectorWidgetProperties& key(const std::string& key);

  /// Enable or disable display of selected points.
  PointSelectorWidgetProperties& showSelected(bool show_selected = true);

  /// Enable or disable display of selected points.
  PointSelectorWidgetProperties& hideSelectedAfterComplete(bool hide_selected = true);

  /// Enable or disable deselection of already selected points.
  PointSelectorWidgetProperties& enableDeselection(bool enable_deselection = true);

  /// Clear currently selected points.
  PointSelectorWidgetProperties& clear();

 private:
  PointSelectorWidgetProperties(const std::shared_ptr<PointSelectorWidget>& object);

  std::weak_ptr<PointSelectorWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class CallbackWidgetProperties;
};

}  // namespace tviewer
