/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>

#include <tviewer/fwd.h>

#include <tviewer/widgets/data_visualization_widget_properties.h>

namespace tviewer {

class PointCloudWidget;

class PointCloudWidgetProperties
: public DataVisualizationWidgetProperties<PointCloudWidgetProperties, PointCloudConstPtr> {
 public:
  /// Point cloud coloring method, either a fixed Color or a point data field.
  ///
  /// See colors() setter method for more information.
  using Colors = boost::variant<Color, std::string>;

  /// Set point size.
  PointCloudWidgetProperties& pointSize(unsigned int point_size);

  /// Set point coloring method.
  ///
  /// There are two distinct ways to colorize points: with uniform and with data-dependent colors.
  /// The first method visualizes all points using the same fixed color; pass an instance of \ref
  /// Color class to use it. In order to use the second coloring method, pass the name of the point
  /// field that is to be used as a source of data. Supported fields:
  ///  - `rgb`
  ///  - `rgba`
  ///  - `label`
  ///  - any other floating-point field (e.g. `x`, `intensity`, `normal_z`)
  ///
  /// If the point cloud does not have the specified field, the widget will fall back to using
  /// uniform white color.
  ///
  /// By default (i.e. if colors() has never been called) the widget will attempt automatic
  /// coloring method selection. Each of the following fields will be tested (in the given order):
  ///  - `rgb`
  ///  - `rgba`
  ///  - `label`
  ///  - `intensity`
  ///  - `curvature`
  ///
  /// The first field that is present will be used. If none of the supported fields are available,
  /// the points will be colored white.
  PointCloudWidgetProperties& colors(Colors colors);

  /// Set LUT.
  //
  /// Supported values: "viridis" (default), "gray", "jet".
  PointCloudWidgetProperties& lut(const std::string& lut);

  PointCloudWidgetProperties& lutRange(double min, double max);

  /// Set data (single point).
  template <typename PointT, std::enable_if_t<is_point<PointT>::value>* = nullptr>
  PointCloudWidgetProperties& data(const PointT& point) {
    typename pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>(1, 1));
    cloud->at(0) = point;
    data(cloud);
    return *this;
  }

  // Prevent base-class data() methods from being hidden.
  using DataVisualizationWidgetProperties<PointCloudWidgetProperties, PointCloudConstPtr>::data;

 private:
  PointCloudWidgetProperties(const std::shared_ptr<PointCloudWidget>& object);

  std::weak_ptr<PointCloudWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class VisualizationWidgetProperties;
  friend class DataVisualizationWidgetProperties;
};

}  // namespace tviewer
