/******************************************************************************
 * Copyright (c) 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>

#include <tviewer/fwd.h>

#include <tviewer/widgets/data_visualization_widget_properties.h>

namespace tviewer {

class PrimitiveObjectsWidget;

class PrimitiveObjectsWidgetProperties
: public DataVisualizationWidgetProperties<PrimitiveObjectsWidgetProperties, PrimitiveObjects> {
 public:
  /// Switch between Phong (default) and simple flat shading of the primitives.
  ///
  /// This applies to all primitive objects visualized by this widget, however has no effect on
  /// line-based primitives that are rendered without any shading (e.g. Arrow, LineSegment).
  PrimitiveObjectsWidgetProperties& flatShading(bool flat_shading = true);

  /// Switch between sufrace (default) and wireframe representation of the primitives.
  ///
  /// This applies to all primitive objects visualized by this widget, however has no effect on
  /// line-based primitives that have no surface (e.g. Arrow, LineSegment).
  PrimitiveObjectsWidgetProperties& wireframe(bool wireframe = true);

  /// Set line width.
  ///
  /// This applies to all primitive objects visualized by this widget, however only has effect on
  /// LineSegment.
  PrimitiveObjectsWidgetProperties& lineWidth(float line_width = 1.0f);

  /// Set data (single primitive object).
  PrimitiveObjectsWidgetProperties& data(const PrimitiveObject& object);

  // Prevent base-class data() methods from being hidden.
  using DataVisualizationWidgetProperties<PrimitiveObjectsWidgetProperties, PrimitiveObjects>::data;

 private:
  PrimitiveObjectsWidgetProperties(const std::shared_ptr<PrimitiveObjectsWidget>& object);

  std::weak_ptr<PrimitiveObjectsWidget> obj;
  friend class TViewerImpl;
  friend class WidgetProperties;
  friend class VisualizationWidgetProperties;
  friend class DataVisualizationWidgetProperties;
};

}  // namespace tviewer
