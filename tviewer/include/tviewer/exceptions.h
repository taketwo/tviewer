/******************************************************************************
 * Copyright (c) 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <stdexcept>

namespace tviewer {

/// An exception indicating a mistake in configuration.
class ConfigurationError : public std::logic_error {
 public:
  ConfigurationError(const std::string& what) : std::logic_error(what) {}
};

class DuplicateWidgetNameException : public std::runtime_error {
 public:
  DuplicateWidgetNameException(const std::string& name)
  : std::runtime_error("Widget name is already in use: " + name) {}
};

/// This exception is raised if an operation on a widget is requested, but a widget with such
/// name does not exist.
class WidgetNotFoundException : public std::runtime_error {
 public:
  WidgetNotFoundException(const std::string& name)
  : std::runtime_error("Widget with name not found: " + name) {}
};

class WidgetTypeMismatchException : public std::runtime_error {
 public:
  WidgetTypeMismatchException(const std::string& name, const std::string& expected_type)
  : std::runtime_error("Widget with name has mismatched type: " + name + ", " + expected_type) {}
};

/// This exception is raised if an non-existent key is queried from Kwargs.
class KeyNotFoundException : public std::runtime_error {
 public:
  KeyNotFoundException(const std::string& name) : std::runtime_error("Key not found: " + name) {}
};

}  // namespace tviewer
