/******************************************************************************
 * Copyright (c) 2018, 2019, 2020, 2021, 2022, 2024 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <tviewer/color.h>
#include <tviewer/fwd.h>

#include <pcl/point_types.h>

namespace tviewer {

/// Structure that represents a colored directed arrow.
struct Arrow {
  Eigen::Vector3f source = Eigen::Vector3f::Zero();  ///< Source point of the arrow.
  Eigen::Vector3f target = Eigen::Vector3f::Ones();  ///< Target point (tip) of the arrow.
  Color color = Color::White;                        ///< Color of the arrow.

  /// Default constructor, white arrow from origin to (1, 1, 1).
  Arrow() = default;

  /// Construct an arrow between two points, of a given color.
  static Arrow fromVectors(const Eigen::Ref<const Eigen::Vector3f>& source,
                           const Eigen::Ref<const Eigen::Vector3f>& target,
                           Color color = Color::White);

  /// Construct an arrow between two points, of a given color.
  static Arrow fromVectors(const Eigen::Ref<const Eigen::Vector3d>& source,
                           const Eigen::Ref<const Eigen::Vector3d>& target,
                           Color color = Color::White);

  /// Construct an arrow between two PCL points, of a given color.
  template <typename PointT1 = pcl::PointXYZ, typename PointT2 = PointT1>
  static Arrow fromPoints(const PointT1& source, const PointT2& target,
                          Color color = Color::White) {
    return Arrow::fromVectors(source.getVector3fMap(), target.getVector3fMap(), color);
  }

  /// Construct an arrow of a given length from an oriented PCL point, of a given color.
  /// The point position defines the arrow source, the point normal defines the arrow direction.
  template <typename PointT = pcl::PointNormal>
  static Arrow fromOrientedPoint(const PointT& point_with_normal, float length = 1.0f,
                                 Color color = Color::White) {
    static_assert(pcl::traits::has_xyz<PointT>::value, "point type should have xyz field");
    static_assert(pcl::traits::has_normal<PointT>::value, "point type should have normal field");
    Arrow arrow;
    arrow.source = point_with_normal.getVector3fMap();
    arrow.target = arrow.source + point_with_normal.getNormalVector3fMap() * length;
    arrow.color = color;
    return arrow;
  }
};

/// Structure that represents a coordinate system axes.
struct Axes {
  Eigen::Vector3f position = Eigen::Vector3f::Zero();               ///< Position of the axes.
  Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();  ///< Orientation of the axes.
  float size = 1.0f;                                                ///< Size of the axes.

  /// Default constructor, axes will be the same as world coordinate system axes.
  Axes() = default;

  /// Construct axes of a given size that are same as world coordinate system axes.
  explicit Axes(float size);

  /// Construct axes at a given position of a given size, aligned with world coordinate system axes.
  static Axes fromVector(const Eigen::Ref<const Eigen::Vector3f>& position, float size = 1.0f);

  /// Construct axes at a given position of a given size, aligned with world coordinate system axes.
  static Axes fromVector(const Eigen::Ref<const Eigen::Vector3d>& position, float size = 1.0f);

  /// Construct axes at origin with given orientation and size.
  template <typename Scalar>
  static Axes fromOrientation(const Eigen::Quaternion<Scalar>& orientation, float size = 1.0f) {
    Axes axes;
    axes.position = Eigen::Vector3f::Zero();
    axes.orientation = orientation.template cast<float>();
    axes.size = size;
    return axes;
  }

  /// Construct axes at origin with given orientation and size.
  static Axes fromOrientation(const Eigen::Ref<const Eigen::Matrix3f>& orientation,
                              float size = 1.0f);

  /// Construct axes at origin with given orientation and size.
  static Axes fromOrientation(const Eigen::Ref<const Eigen::Matrix3d>& orientation,
                              float size = 1.0f);

  /// Construct axes at a given position and orientation, with a given size.
  template <typename Scalar1, typename Scalar2 = Scalar1>
  static Axes fromVectorAndOrientation(const Eigen::Matrix<Scalar1, 3, 1>& position,
                                       const Eigen::Quaternion<Scalar2>& orientation,
                                       float size = 1.0f) {
    Axes axes;
    axes.position = position.template cast<float>();
    axes.orientation = orientation.template cast<float>();
    axes.size = size;
    return axes;
  }

  /// Construct axes at a position given by a PCL point and orientation, with a given size.
  template <typename PointT = pcl::PointXYZ, typename Scalar = float>
  static Axes fromPointAndOrientation(
      const PointT& point,
      const Eigen::Quaternion<Scalar>& orientation = Eigen::Quaternion<Scalar>::Identity(),
      float size = 1.0f) {
    Axes axes;
    axes.position = point.getVector3fMap();
    axes.orientation = orientation;
    axes.size = size;
    return axes;
  }

  /// Construct axes from a transform, with a given size.
  /// Position is initialized with the translational component of the transform and orientation is
  /// initialized with the rotational component of the transform.
  template <typename Scalar, int Mode>
  static Axes fromTransform(const Eigen::Transform<Scalar, 3, Mode>& transform, float size = 1.0f) {
    Axes axes;
    axes.position = transform.translation().template cast<float>();
    axes.orientation = transform.linear().template cast<float>();
    axes.size = size;
    return axes;
  }
};

/// Structure that represents a box.
struct Box {
  Eigen::Vector3f center = Eigen::Vector3f::Zero();                 ///< Center of the box.
  Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();  ///< Orientation of the box.
  Eigen::Vector3f dimensions = Eigen::Vector3f::Ones();  ///< Dimensions (W,H,D) of the box.
  Color color = Color::White;                            ///< Color of the box.

  /// Default constructor, box will be centered at origin with sides of one meter and have white
  /// color.
  Box() = default;

  /// Construct box at a given pose, with a given size and of a given color.
  /// Position is initialized with the translational component of the transform and orientation is
  /// initialized with the rotational component of the transform.
  template <typename Scalar, int Mode>
  static Box fromTransform(const Eigen::Transform<Scalar, 3, Mode>& transform,
                           const Eigen::Matrix<Scalar, 3, 1>& dimensions,
                           Color color = Color::White) {
    Box box;
    box.center = transform.translation().template cast<float>();
    box.orientation = transform.linear().template cast<float>();
    box.dimensions = dimensions.template cast<float>();
    box.color = color;
    return box;
  }
};

/// Structure that represents a circle.
struct Circle {
  Eigen::Vector2f center;  ///< Center of the circle.
  float radius;            ///< Radius of the circle.
};

/// Structure that represents a cone.
struct Cone {
  Eigen::Vector3f apex;  ///< Apex of the cone.
  Eigen::Vector3f axis;  ///< Axis of the cone. The norm of the axis defines the height of the cone.
  float angle;           ///< Opening angle of the cone (degrees).
};

/// Structure that represents a cylinder.
struct Cylinder {
  Eigen::Vector3f center;      ///< Center of the cylinder.
  Eigen::Vector3f axis;        ///< Axis of the cylinder. The norm of the axis defines the height of
                               ///  the cylinder.
  float radius;                ///< Radius of the cylinder.
  Color color = Color::White;  ///< Color of the cylinder.

  /// Construct a cylinder from the model coefficients output by PCL sample consensus fitting.
  static Cylinder fromModelCoefficients(const Eigen::VectorXf& coefficients,
                                        Color color = Color::White);

  /// Construct a cylinder whose axis spans line segment between two points, of a given radius and
  /// of a given color.
  static Cylinder fromVectors(const Eigen::Ref<const Eigen::Vector3f>& start,
                              const Eigen::Ref<const Eigen::Vector3f>& end, float radius,
                              Color color = Color::White);

  /// Construct a cylinder whose axis spans line segment between two points, of a given radius and
  /// of a given color.
  static Cylinder fromVectors(const Eigen::Ref<const Eigen::Vector3d>& start,
                              const Eigen::Ref<const Eigen::Vector3d>& end, float radius,
                              Color color = Color::White);
};

/// Structure that represents a colored line segment.
struct LineSegment {
  Eigen::Vector3f start = {0, 0, 0};  ///< Start point on the line.
  Eigen::Vector3f end = {1, 1, 1};    ///< End point on the line.
  Color color = Color::White;         ///< Color of the line.

  /// Default constructor, a white line segment from origin to (1, 1, 1).
  LineSegment() = default;

  /// Construct a line segment between two points, of a given color.
  static LineSegment fromVectors(const Eigen::Ref<const Eigen::Vector3f>& start,
                                 const Eigen::Ref<const Eigen::Vector3f>& end,
                                 Color color = Color::White);

  /// Construct a line segment between two points, of a given color.
  static LineSegment fromVectors(const Eigen::Ref<const Eigen::Vector3d>& start,
                                 const Eigen::Ref<const Eigen::Vector3d>& end,
                                 Color color = Color::White);

  /// Construct a line segment between two PCL points, of a given color.
  template <typename PointT1 = pcl::PointXYZ, typename PointT2 = PointT1>
  static LineSegment fromPoints(const PointT1& start, const PointT2& end,
                                Color color = Color::White) {
    return LineSegment::fromVectors(start.getVector3fMap(), end.getVector3fMap(), color);
  }
};

/// Structure that represents a colored plane.
struct Plane {
  Eigen::Vector3f normal = {0, 0, 1};    ///< Plane normal.
  Eigen::Vector3f position = {0, 0, 0};  ///< Position of the plane center.
  Eigen::Vector2f extents = {1, 1};      ///< Extents of the plane.
  Color color = Color::White;            ///< Color of the plane.

  /// Construct a plane through a given PCL point with normal.
  template <typename PointT = pcl::PointNormal>
  static Plane fromOrientedPoint(const PointT& point_with_normal, const Eigen::Vector2f& extents,
                                 Color color = Color::White) {
    static_assert(pcl::traits::has_xyz<PointT>::value, "point type should have xyz field");
    static_assert(pcl::traits::has_normal<PointT>::value, "point type should have normal field");
    return Plane::fromVectorAndNormal(point_with_normal.getVector3fMap(),
                                      point_with_normal.getNormalVector3fMap(), extents, color);
  }

  /// Construct a plane through a given position with a given normal.
  static Plane fromVectorAndNormal(const Eigen::Ref<const Eigen::Vector3f>& position,
                                   const Eigen::Ref<const Eigen::Vector3f>& normal,
                                   const Eigen::Vector2f& extents, Color color = Color::White);

  /// Construct a plane through a given position with a given normal.
  static Plane fromVectorAndNormal(const Eigen::Ref<const Eigen::Vector3d>& position,
                                   const Eigen::Ref<const Eigen::Vector3d>& normal,
                                   const Eigen::Vector2f& extents, Color color = Color::White);

  /// Construct a plane through origin with a given normal.
  static Plane fromNormal(const Eigen::Ref<const Eigen::Vector3f>& normal,
                          const Eigen::Vector2f& extents, Color color = Color::White);

  /// Construct a plane through origin with a given normal.
  static Plane fromNormal(const Eigen::Ref<const Eigen::Vector3d>& normal,
                          const Eigen::Vector2f& extents, Color color = Color::White);

  /// Construct a plane from the model coefficients output by PCL sample consensus fitting.
  /// The plane will be centered at the point closest to origin.
  static Plane fromModelCoefficients(const Eigen::VectorXf& coefficients,
                                     const Eigen::Vector2f& extents, Color color = Color::White);
};

/// Structure that represents a colored sphere.
struct Sphere {
  Eigen::Vector3f center = Eigen::Vector3f::Zero();  ///< Center of the sphere.
  float radius = 0.01f;                              ///< Radius of the sphere.
  Color color = Color::White;                        ///< Color of the sphere.

  /// Default constructor, a white sphere with radius 1 at origin.
  Sphere() = default;

  /// Construct a sphere at a given position, of a given radius and color.
  static Sphere fromVector(const Eigen::Ref<const Eigen::Vector3f>& center, float radius = 0.01f,
                           Color color = Color::White);

  /// Construct a sphere at a given position, of a given radius and color.
  static Sphere fromVector(const Eigen::Ref<const Eigen::Vector3d>& center, float radius = 0.01f,
                           Color color = Color::White);

  /// Construct a sphere at a position given by a PCL point, of a given radius and color.
  template <typename PointT = pcl::PointXYZ>
  static Sphere fromPoint(const PointT& center, float radius = 0.01f, Color color = Color::White) {
    return Sphere::fromVector(center.getVector3fMap(), radius, color);
  }
};

/// Structure that represents 3D text.
struct Text {
  std::string text = "";                               ///< Actual text.
  Eigen::Vector3f position = Eigen::Vector3f::Zero();  ///< Postion of the text.
  float scale = 0.01;                                  ///< Scale of the text.
  Color color = Color::White;                          ///< Color of the text.

  /// Default constructor, empty white text of 1 cm scale at origin.
  Text() = default;

  /// Given white text of 1 cm scale at origin.
  Text(const std::string& t) : text(t){};

  /// Construct a text at a given position, of a given scale and color.
  static Text fromVector(const std::string& text, const Eigen::Ref<const Eigen::Vector3f>& position,
                         float scale = 0.01f, Color color = Color::White);

  /// Construct a text at a given position, of a given scale and color.
  static Text fromVector(const std::string& text, const Eigen::Ref<const Eigen::Vector3d>& position,
                         float scale = 0.01f, Color color = Color::White);
};

template <typename T>
T transform(const T& primitive_object, const Transform& transform);

}  // namespace tviewer
