/******************************************************************************
 * Copyright (c) 2014, 2018, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <cstdint>
#include <iostream>
#include <tuple>

#include <pcl/common/colors.h>

namespace tviewer {

/// A 24-bit RGB color.
class Color {
 public:
  /// A selection of color names from https://en.wikipedia.org/wiki/List_of_colors_(compact).
  enum ColorName {
    Black,
    Blue,
    Brown,
    Citron,
    Corn,
    Crimson,
    Cyan,
    DarkGray,
    Denim,
    Emerald,
    Gray,
    Green,
    Indigo,
    LightGray,
    Magenta,
    Mango,
    MaximumBlue,
    MaximumGreen,
    MaximumRed,
    Orange,
    Pear,
    Purple,
    Red,
    Rose,
    Ruby,
    Teal,
    White,
    Yellow
  };

  Color() = default;

  /// Construct a color from red, green, and blue values (unsigned chars in `0..255` range).
  Color(uint8_t r, uint8_t g, uint8_t b);

  /// Construct a color from red, green, and blue values (floats in `0..1` range).
  /// Values that are outside of the [0..1] range are clamped.
  Color(float r, float g, float b);

  /// Construct a color from red, green, and blue values.
  /// This will forward to either `uint8_t` or `float` constructor depending on whether type T is
  /// integral or not. Integrals outside `0..255` range are clamped.
  template <typename T>
  Color(T r, T g, T b);

  /// Construct a color from 24-bit RGB.
  Color(uint32_t rgb);

  /// Construct a color from name.
  Color(ColorName color_name);

  template <typename T>
  Color(const std::tuple<T, T, T>& rgb)
  : Color(std::get<0>(rgb), std::get<1>(rgb), std::get<2>(rgb)) {}

  /** Get a color from a string representation.
   *
   * Supported formats:
   *
   * * Sequence of digits representing an unsigned integer
   *
   *   Uses `std::stoul()` internally, therefore supports numbers given in
   *   octal or hexadecimal base.
   *
   *   Examples:
   *
   *     * <tt>"255"</tt> : blue
   *     * <tt>"0177400"</tt> : green
   *     * <tt>"0xFF0000"</tt> : red
   *
   * For invalid string representations black color will be returned. */
  explicit Color(const std::string& color);

  /** Get R, G, and B values (floats) from a color. */
  std::tuple<float, float, float> getRGB() const;

  /** Get R, G, and B values (floats) as array from a color. */
  void getRGB(float* rgb) const;

  /** Get R, G, and B values (chars) as array from a color. */
  void getRGB(uint8_t* rgb) const;

  /** Get R, G, and B values (chars) as array from a color. */
  void getRGB(uint8_t& r, uint8_t& g, uint8_t& b) const;

  /** Generate a random color. */
  static Color random();

  bool operator==(const tviewer::Color& rhs) const;

  bool operator!=(const tviewer::Color& rhs) const;

  operator uint32_t() const {
    return rgba_;
  }

 private:
  uint32_t rgba_ = 0;
};

/// Color maps that associate a color to every float from [0..1].
/// This mimics some of the standard MATLAB® color maps, see
/// http://www.mathworks.de/de/help/matlab/ref/colormap.html.
enum class ColorMap {
  /// Ranges from blue to red, and passes through the colors cyan, yellow,
  /// and orange
  JET,
  /// Consists of colors that are shades of green and yellow
  SUMMER,
  /// Varies smoothly from red, through orange, to yellow
  AUTUMN,
  /// Consists of colors that are shades of cyan and magenta
  COOL,
  /// Viridis color map from matplotlib
  VIRIDIS
};

/** Get the color for a given number in [0..1] using a given colormap.
 *
 * Input values that are outside of the [0..1] region are clamped
 * automatically.
 *
 * \see ColorMap */
Color getColor(float value, ColorMap map = ColorMap::VIRIDIS);

/** Get the color for a given id using a given PCL colormap.
 *
 * Input values outside of the [0..max-id] range are wrapped around automatically.
 */
template <pcl::ColorLUTName Name = pcl::LUT_GLASBEY>
Color getColor(size_t color_id) {
  using LUT = pcl::ColorLUT<Name>;
  return Color(LUT::at(color_id % LUT::size()).rgba);
}

std::ostream& operator<<(std::ostream& os, const Color& obj);

}  // namespace tviewer
