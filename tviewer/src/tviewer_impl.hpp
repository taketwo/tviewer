/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <memory>
#include <set>
#include <sstream>
#include <unordered_map>

#include <boost/format.hpp>

#include <pcl/common/utils.h>
#include <pcl/pcl_config.h>

#include <tviewer/tviewer.h>

#include "key.h"
#include "traits.h"
#include "widgets/point_cloud_widget.hpp"
#include "widgets/widget.hpp"

const char* PRIVATE_WIDGET_PREFIX = "__";

namespace tviewer {

struct Viewport {
  Viewport(double xmin, double ymin, double xmax, double ymax, bool separate_cam = true)
  : dimensions({xmin, ymin, xmax, ymax}), separate_camera(separate_cam) {}

  std::array<double, 4> dimensions;
  bool separate_camera;
  int id = 0;  ///< index assigned by createViewPort
  boost::optional<pcl::visualization::Camera>
      camera;  ///< will be preserved when visualizer is destroyed
};

/// Helper class to format widget names.
/// If the name is a valid template with a single integer index placeholder, it will replaced with
/// the number of occurrences if this specific template. If the name is not a valid template, it
/// will be returned without changes.
class NameFormatter {
 public:
  std::string format(const std::string& name) {
    if (!isValidTemplate(name))
      return name;
    if (templates_.count(name) == 0)
      templates_[name] = 0;
    return boost::str(boost::format(name) % (++templates_[name]));
  }

 private:
  static bool isValidTemplate(const std::string& str) {
    try {
      boost::str(boost::format(str) % 1);
    } catch (boost::io::too_many_args&) {
      return false;
    } catch (boost::io::too_few_args&) {
      WARN("(tviewer) {{NameFormatter}} too many placeholders in name template: {}", str);
      return false;
    } catch (boost::io::bad_format_string&) {
      WARN("(tviewer) {{NameFormatter}} invalid placeholders in name template: {}", str);
      return false;
    }
    return true;
  }

  std::unordered_map<std::string, unsigned int> templates_;
};

class TViewerImpl {
 public:
  TViewerImpl() {
    vtkObject::GlobalWarningDisplayOff();
    viewports_.emplace_back(0.0, 0.0, 1.0, 1.0);  // default viewport, occupies entire window
    addPrivateWidget<Text2DWidget>("legend").key("C-h");
  }

  ~TViewerImpl() {
    if (visualizer_)
      destroyVisualizer();
  }

 private:
  [[maybe_unused]] Logger logger_;
  std::string window_title_ = "TViewer";
  boost::optional<Color> bg_color_;
  boost::optional<Eigen::Isometry3d> camera_pose_;
  boost::optional<Eigen::Matrix3d> camera_intrinsics_;
  boost::optional<pcl::visualization::KeyboardEvent> last_keyboard_event_;
  boost::optional<pcl::visualization::PointPickingEvent> last_point_picking_event_;
  std::shared_ptr<pcl::visualization::PCLVisualizer> visualizer_;
  std::vector<Viewport> viewports_;
  std::string camera_persistence_;
  NameFormatter name_formatter_;
  bool show_fps_ = true;
  bool stop_requested_ = false;

  std::unordered_map<std::string, Widget::Ptr> widgets_;
  std::list<std::string> widget_names_in_order_of_creation_;
  std::set<std::string> disabled_groups_;

  bool mode_waiting_user_input_ = false;

  friend class TViewer;

  void stateChangeEventCallback(const std::string& name) {
    DEBUG("(tviewer) {{stateChangeEventCallback}} triggered by: {}", name);
    if (!isWidgetGroupEnabled(name)) {
      DEBUG(
          "(tviewer) {stateChangeEventCallback} ignoring because widget belongs to a disabled "
          "group");
      return;
    }
    StateChangeEvent event(getWidgetsState(), {name});
    TRACE("(tviewer) {stateChangeEventCallback} dispatching event to source widget");
    widgets_.at(name)->handleStateChangeEvent(event);
    TRACE("(tviewer) {stateChangeEventCallback} dispatching event to other widgets");
    for (const auto& widget : widgets_)
      if (widget.first != name && isWidgetGroupEnabled(widget.second))
        widget.second->handleStateChangeEvent(event);
    updateLegend();
  }

  void update(const std::string& name) {
    DEBUG("(tviewer) {{update}} name: {}", name);
    auto widget = widgets_.find(name);
    if (widget == widgets_.end()) {
      ERROR("(tviewer) {{update}} widget with name {} does not exist", name);
      throw WidgetNotFoundException(name);
    }
    if (isWidgetGroupEnabled(widget->second)) {
      auto state = getWidgetsState();
      TRACE("(tviewer) {update} sending state change event to the target widget");
      widget->second->handleStateChangeEvent(StateChangeEvent(state));
    } else {
      DEBUG("(tviewer) {update} not sending state change event because widget's group is disabled");
    }
  }

  Kwargs getWidgetsState() {
    TRACE("(tviewer) {getWidgetsState} querying state of widgets");
    Kwargs state;
    for (const auto& widget : widgets_)
      if (isWidgetGroupEnabled(widget.second))
        state.add(widget.first, widget.second->getState().getValue());
    return state;
  }

  void keyboardEventCallback(const pcl::visualization::KeyboardEvent& event) {
    if (event.getKeyCode() == 'e' || event.getKeyCode() == 'q') {
      // Fast-track return if the 'e' or 'q' key was pressed. Otherwise we may crash somewhere in
      // STL code.
      DEBUG("(tviewer) {{keyboardEventCallback}} ignoring {} keypress", event.getKeyCode());
      return;
    }
    Key key(event);
    DEBUG("(tviewer) {{keyboardEventCallback}} triggered by {}: {}",
          (event.keyUp() ? "key up" : "key down"), key);
    last_keyboard_event_ = event;
    if (event.keyUp()) {
      // // save camera parameters
      // // if (matchKeys (event, "z"))
      // // saveCameraParameters ("viewpoint.cam");
      // // // load camera parameters
      // // if (matchKeys (event, "Z"))
      // // loadCameraParameters ("viewpoint.cam");
      // // print (H)elp
      if (key == Key("h"))
        printHelp();
      if (key == Key("escape")) {
        mode_waiting_user_input_ = false;
        INFO("(tviewer) {keyboardEventCallback} stop waiting for point selection");
      }
      for (const auto& widget : widgets_)
        if (isWidgetGroupEnabled(widget.second))
          widget.second->handleKeyboardEvent(key);
    }
  }

  void pointPickingEventCallback(const pcl::visualization::PointPickingEvent& event) {
    DEBUG("(tviewer) {pointPickingEventCallback}");
    last_point_picking_event_ = event;
    for (const auto& widget : widgets_)
      if (isWidgetGroupEnabled(widget.second))
        widget.second->handlePointPickingEvent(event);
  }

  void printHelp() const {
    if (widgets_.empty())
      return;

    boost::format fmt_header(
        "\n%=64s"
        "\n─────┬╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┬──────\n");
    boost::format fmt_footer(
        "─────┴╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┴──────\n");
    boost::format fmt_info("%=5s│ %-58s │ ");
    boost::format fmt_extra("     │ %-58s │\n");

    std::stringstream ss;
    ss << fmt_header % "Widgets";
    for (const auto& name : widget_names_in_order_of_creation_) {
      if (name.rfind(PRIVATE_WIDGET_PREFIX, 0) == 0)
        continue;  // do not display private widgets
      if (!isWidgetGroupEnabled(name))
        continue;  // do not display widgets whose group is disabled
      std::string icon;
      std::string description;
      std::string extra;
      std::vector<Key> keys;
      widgets_.at(name)->getHelpInfo(icon, description, keys, extra);
      ss << fmt_info % icon % description;
      for (const auto& key : keys)
        ss << key << " ";
      ss << "\n";
      if (!extra.empty())
        ss << fmt_extra % extra;
    }
    ss << fmt_footer;

    std::cout << ss.str();
  }

  void updateLegend() {
    // TODO: outsource generating legend text to widgets.
    std::stringstream ss;
    for (const auto& widget : widgets_) {
      if (!isWidgetGroupEnabled(widget.second))
        continue;
      if (auto ptr = std::dynamic_pointer_cast<SpinnerWidget>(widget.second))
        ss << widget.first << " : " << ptr->getState().getValue<double>() << "\n";
      else if (auto ptr = std::dynamic_pointer_cast<VariantWidget>(widget.second))
        ss << widget.first << " : " << ptr->getState().getValue<std::string>() << "\n";
      else if (auto ptr = std::dynamic_pointer_cast<ToggleWidget>(widget.second))
        ss << widget.first << " : " << (ptr->getState().getValue<bool>() ? "on" : "off") << "\n";
    }
    getPrivateWidget<Text2DWidget>("legend").data(ss.str());
  }

  template <typename WidgetT>
  typename WidgetT::Properties addWidget(const std::string& name) {
    auto name_formatted = name_formatter_.format(name);
    INFO("(tviewer) {{addWidget}} name: {}, type: {}", name_formatted,
         widget_traits<WidgetT>::name);
    const auto widget = widgets_.find(name_formatted);
    if (widget != widgets_.end()) {
      if (std::dynamic_pointer_cast<WidgetT>(widget->second)) {
        DEBUG("(tviewer) {{addWidget}} widget of matching type exists, returning no-op properties");
        return typename WidgetT::Properties{nullptr};
      }
      ERROR("(tviewer) {getWidget} widget of non-matching type exists");
      throw WidgetTypeMismatchException(name, widget_traits<WidgetT>::name);
    }
    typename WidgetT::Ptr obj(new WidgetT(name_formatted, [name_formatted, this] {
      this->stateChangeEventCallback(name_formatted);
    }));
    widgets_.emplace(name_formatted, obj);
    widget_names_in_order_of_creation_.push_back(name_formatted);
    if (visualizer_)
      obj->injectVisualizer(visualizer_);
    for (const auto& aux_widget : obj->getAuxiliaryWidgets()) {
      const auto aux_name = makePrivateWidgetName(aux_widget->getName());
      DEBUG("(tviewer) {{addWidget}} register auxiliary widget: {}", aux_name);
      widgets_.emplace(aux_name, aux_widget);
      widget_names_in_order_of_creation_.push_back(aux_name);
      if (visualizer_)
        aux_widget->injectVisualizer(visualizer_);
    }
    return obj;
  }

  template <typename WidgetT>
  typename WidgetT::Properties addPrivateWidget(const std::string& name) {
    return addWidget<WidgetT>(makePrivateWidgetName(name));
  }

  template <typename WidgetT>
  typename WidgetT::Properties getWidget(const std::string& name) {
    DEBUG("(tviewer) {{getWidget}} name: {}", name);
    auto widget = widgets_.find(name);
    if (widget == widgets_.end()) {
      ERROR("(tviewer) {{getWidget}} widget with name {} does not exist", name);
      throw WidgetNotFoundException(name);
    }
    if (auto ptr = std::dynamic_pointer_cast<WidgetT>(widget->second))
      return ptr;
    ERROR("(tviewer) {{getWidget}} type mismatch: expected {}", widget_traits<WidgetT>::name);
    throw WidgetTypeMismatchException(name, widget_traits<WidgetT>::name);
  }

  template <typename WidgetT>
  typename WidgetT::Properties getPrivateWidget(const std::string& name) {
    return getWidget<WidgetT>(makePrivateWidgetName(name));
  }

  template <typename WidgetT>
  typename WidgetT::Properties addOrGetWidget(const std::string& name) {
    auto name_formatted = name_formatter_.format(name);
    INFO("(tviewer) {{addOrGetWidget}} name: {}, type: {}", name_formatted,
         widget_traits<WidgetT>::name);
    if (widgets_.count(name_formatted)) {
      DEBUG("(tviewer) {{addOrGetWidget}} widget {} exists, dispatching to getWidget()");
      return getWidget<WidgetT>(name_formatted);
    }
    DEBUG("(tviewer) {{addOrGetWidget}} widget {} does not exist, dispatching to addWidget()");
    return addWidget<WidgetT>(name_formatted);
  }

  void removeWidget(const std::string& name) {
    INFO("(tviewer) {{removeWidget}} name: {}", name);
    auto widget = widgets_.find(name);
    if (widget == widgets_.end()) {
      ERROR("(tviewer) {{removeWidget}} widget with name {} does not exist", name);
      throw WidgetNotFoundException(name);
    }
    for (const auto& aux_widget : widget->second->getAuxiliaryWidgets()) {
      auto aux_name = makePrivateWidgetName(aux_widget->getName());
      DEBUG("(tviewer) {{removeWidget}} deregister auxiliary widget: {}", aux_name);
      removeWidget(aux_name);
    }
    widgets_.erase(widget);
    widget_names_in_order_of_creation_.remove(name);
  }

  void removeAllWidgets() {
    const auto widgets_to_remove = widget_names_in_order_of_creation_;
    INFO("(tviewer) {removeAllWidgets}");
    for (const auto& name : widgets_to_remove)
      if (name.rfind(PRIVATE_WIDGET_PREFIX, 0) != 0)  // skip private widgets
        removeWidget(name);
  }

  bool containsWidget(const std::string& name) {
    const auto result = widgets_.count(name);
    DEBUG("(tviewer) {{containsWidget}} name: {}, result: {}", name, (result ? "yes" : "no"));
    return result != 0u;
  }

  void enableGroup(const std::string& group) {
    DEBUG("(tviewer) {{enableGroup}} group: {}", group);
    if (disabled_groups_.count(group) == 1) {
      disabled_groups_.erase(group);
      for (const auto& widget : widgets_)
        if (widget.second->getGroup() == group)
          widget.second->injectVisualizer(visualizer_);
    }
  }

  void disableGroup(const std::string& group) {
    DEBUG("(tviewer) {{disableGroup}} group: {}", group);
    if (disabled_groups_.count(group) == 0) {
      disabled_groups_.insert(group);
      for (const auto& widget : widgets_)
        if (widget.second->getGroup() == group)
          widget.second->ejectVisualizer();
    }
  }

  bool isWidgetGroupEnabled(const Widget::Ptr& widget) const {
    if (widget)
      return disabled_groups_.count(widget->getGroup()) == 0;
    return false;
  }

  bool isWidgetGroupEnabled(const std::string& name) const {
    auto widget = widgets_.find(name);
    if (widget != widgets_.end())
      return isWidgetGroupEnabled(widget->second);
    return false;
  }

  static std::string makePrivateWidgetName(const std::string& name) {
    return std::string(PRIVATE_WIDGET_PREFIX) + name;
  }

  void setCameraIntrinsics(const Eigen::Matrix3d& intrinsics) {
    TRACE("(tviewer) {setCameraIntrinsics}");
    camera_intrinsics_ = intrinsics;
    applyCameraParameters();
  }

  void setCameraPose(const Eigen::Isometry3d& pose) {
    TRACE("(tviewer) {setCameraPose}");
    camera_pose_ = pose;
    applyCameraParameters();
  }

  void setCameraParameters(const Eigen::Matrix3d& intrinsics, const Eigen::Isometry3d& extrinsics) {
    TRACE("(tviewer) {setCameraParameters}");
    camera_intrinsics_ = intrinsics;
    camera_pose_ = extrinsics;
    applyCameraParameters();
  }

  void applyCameraParameters() {
    if (!camera_pose_ && !camera_intrinsics_) {
      TRACE("(tviewer) {applyCameraParameters} no custom camera pose or intrinsics, skipping");
    } else if (!visualizer_) {
      TRACE("(tviewer) {applyCameraParameters} no visualization window, skipping");
    } else {
      if (camera_pose_ && camera_intrinsics_) {
        TRACE(
            "(tviewer) {applyCameraParameters} setting camera pose and intrinsics in visualization "
            "window");
        // Rotation to ensure the image is not upside down
        const Eigen::Isometry3d rotation(Eigen::AngleAxisd(M_PI, Eigen::Vector3d::UnitZ()));
        const Eigen::Matrix4d extrinsics = (*camera_pose_ * rotation).matrix();
        visualizer_->setCameraParameters(camera_intrinsics_->cast<float>(),
                                         extrinsics.cast<float>());
      } else if (camera_pose_ && !camera_intrinsics_) {
        TRACE("(tviewer) {applyCameraParameters} setting camera pose in visualization window");
        Eigen::Vector3d position = camera_pose_->translation();
        Eigen::Vector3d viewpoint = position + camera_pose_->linear() * Eigen::Vector3d::UnitZ();
        Eigen::Vector3d up = -camera_pose_->linear() * Eigen::Vector3d::UnitY();
        visualizer_->setCameraPosition(position[0], position[1], position[2], viewpoint[0],
                                       viewpoint[1], viewpoint[2], up[0], up[1], up[2]);
      } else {
        TRACE(
            "(tviewer) {applyCameraParameters} setting camera intrinsics in visualization window");
        const auto extrinsics = visualizer_->getViewerPose();
        visualizer_->setCameraParameters(camera_intrinsics_->cast<float>(), extrinsics.matrix());
      }
      camera_pose_.reset();
      camera_intrinsics_.reset();
    }
  }

  void addViewport(double xmin, double ymin, double xmax, double ymax,
                   bool separate_camera = false) {
    auto id = viewports_.size();
    INFO("(tviewer) {{addViewport}} id: {}, separate camera: {}", id,
         (separate_camera ? "yes" : "no"));
    if (xmin >= xmax || ymin > ymax)
      WARN("(tviewer) {addViewport} invalid dimensions");
    viewports_.emplace_back(xmin, ymin, xmax, ymax, separate_camera);
    auto id_string = std::to_string(id);
    addPrivateWidget<Text2DWidget>("viewport_" + id_string)
        .key("C-v")
        .text("Viewport " + id_string)
        .viewport(id)
        .hide();
  }

  void createVisualizer() {
    TRACE("(tviewer) {createVisualizer} creating visualization window");

    // Create visualizer
    visualizer_ = std::make_shared<pcl::visualization::PCLVisualizer>(window_title_);

    // Load viewport cameras from disk, if enabled
    if (!camera_persistence_.empty()) {
      DEBUG("(tviewer) {{createVisualizer}} camera persistence enabled, reading from: {}",
            camera_persistence_);
      loadCameras();
    }

    // Create viewports
    for (size_t i = 0; i < viewports_.size(); ++i) {
      auto& vp = viewports_[i];
      // The very first viewport is the default one and is created by the visualizer automatically.
      if (i > 0) {
        visualizer_->createViewPort(vp.dimensions[0], vp.dimensions[1], vp.dimensions[2],
                                    vp.dimensions[3], vp.id);
        DEBUG("(tviewer) {{createVisualizer}} created viewport with id: {}", vp.id);
      }
      if (vp.separate_camera) {
        visualizer_->createViewPortCamera(vp.id);
        DEBUG("(tviewer) {{createVisualizer}} created camera (viewport: {})", vp.id);
        if (vp.camera) {
          visualizer_->setCameraParameters(*vp.camera, vp.id);
          DEBUG("(tviewer) {{createVisualizer}} restored camera settings (viewport: {})", vp.id);
        }
      }
    }

    visualizer_->registerKeyboardCallback(
        boost::bind(&TViewerImpl::keyboardEventCallback, this, _1));
    visualizer_->registerPointPickingCallback(
        boost::bind(&TViewerImpl::pointPickingEventCallback, this, _1));

    for (const auto& widget : widgets_)
      if (isWidgetGroupEnabled(widget.second))
        widget.second->injectVisualizer(visualizer_);
  }

  void configureVisualizer() {
    TRACE("(tviewer) {configureVisualizer} configuring visualization window");

    visualizer_->setShowFPS(show_fps_);
    visualizer_->setWindowName(window_title_);
    applyBackgroundColor();
    applyCameraParameters();
  }

  void destroyVisualizer() {
    // Query viewport cameras
    for (auto& vp : viewports_)
      if (vp.separate_camera) {
#if PCL_VERSION_COMPARE(>=, 1, 10, 0)
        vp.camera = pcl::visualization::Camera();
        visualizer_->getCameraParameters(*vp.camera, vp.id);
#else
        // Old versions do not support retrieving camera parameters for individual viewports, so
        // need to get all of them at once.
        std::vector<pcl::visualization::Camera> cameras;
        visualizer_->getCameras(cameras);
        vp.camera = cameras[vp.id];
#endif
        DEBUG("(tviewer) {{destroyVisualizer}} saved camera settings (viewport: {})", vp.id);
      }

    // Write viewport cameras to disk, if needed
    if (!camera_persistence_.empty()) {
      DEBUG("(tviewer) {{destroyVisualizer}} camera persistence enabled, writing to: {}",
            camera_persistence_);
      saveCameras();
    }

    TRACE("(tviewer) {destroyVisualizer} calling visualizer destructor");
    visualizer_.reset();
  }

  void run(std::chrono::duration<float> duration, TViewer::KeepWindowOpen keep_open) {
    if (duration.count() != 0.0f)
      TRACE("(tviewer) {{run}} duration: {} ms",
            std::chrono::duration_cast<std::chrono::milliseconds>(duration).count());
    else
      TRACE("(tviewer) {run} duration: unlimited");

    if (!visualizer_)
      createVisualizer();

    configureVisualizer();

    // Interaction loop
    auto start_time = std::chrono::steady_clock::now();
    while (!visualizer_->wasStopped()) {
      visualizer_->spinOnce(1);
      if ((duration.count() != 0.0f) &&
          ((std::chrono::steady_clock::now() - start_time) > duration)) {
        TRACE("(tviewer) {run} stopping interaction loop because requested duration time elapsed");
        break;
      }
      if (stop_requested_) {
        TRACE("(tviewer) {run} stopping interaction loop because of stop request");
        stop_requested_ = false;
        break;
      }
    }

    if (keep_open == TViewer::KeepWindowOpen::YES) {
      // Prevent visualizer from terminating
      visualizer_->resetStoppedFlag();
    } else
      destroyVisualizer();
  }

  void stop() {
    TRACE("(tviewer) {stop}");

    if (visualizer_) {
      stop_requested_ = true;
    }
  }

  void setBackgroundColor(Color color) {
    DEBUG("(tviewer) {{setBackgroundColor}} color: 0x{0:08X}", static_cast<uint32_t>(color));
    bg_color_ = color;
    applyBackgroundColor();
  }

  void applyBackgroundColor() {
    if (!bg_color_) {
      TRACE("(tviewer) {applyBackgroundColor} no custom background color, skipping");
    } else if (visualizer_) {
      TRACE("(tviewer) {applyBackgroundColor} setting background color in visualization window");
      auto [r, g, b] = bg_color_->getRGB();
      for (int i = 0; i < static_cast<int>(viewports_.size()); ++i)
        visualizer_->setBackgroundColor(r, g, b, i);
    } else {
      TRACE("(tviewer) {applyBackgroundColor} no visualization window, skipping");
    }
  }

  void feedKey(const std::string& key) {
    Key k(key);
    for (const auto& widget : widgets_)
      widget.second->handleKeyboardEvent(k);
  }

  bool waitPointSelected(size_t& point_index) {
    TRACE("(tviewer) {waitPointSelected}");

    if (!visualizer_)
      createVisualizer();

    configureVisualizer();

    DEBUG("(tviewer) {waitPointSelected} start waiting for point selection");
    mode_waiting_user_input_ = true;
    while (!visualizer_->wasStopped() && mode_waiting_user_input_) {
      if (last_point_picking_event_) {
        point_index = last_point_picking_event_->getPointIndex();
        last_point_picking_event_ = boost::none;
        INFO("(tviewer) {{waitPointSelected}} point with index {} selected", point_index);
        return true;
      }
      visualizer_->spinOnce(10);
    }
    return false;
  }

  unsigned int waitPointsSelected(std::vector<int>& indices) {
    TRACE("(tviewer) {waitPointsSelected}");

    // Set up point cloud display
    pcl::PointCloud<pcl::PointXYZL>::Ptr points(new pcl::PointCloud<pcl::PointXYZL>);
    const std::string id = "selected points";

    addWidget<PointCloudWidget>(id).key("u").data(points).pointSize(8);

    // pcl::console::print_info("Please select points:\n");

    if (!visualizer_)
      createVisualizer();

    configureVisualizer();

    DEBUG("(tviewer) {waitPointsSelected} start waiting for points selection");

    indices.clear();
    mode_waiting_user_input_ = true;
    while (!visualizer_->wasStopped() && mode_waiting_user_input_) {
      if (last_point_picking_event_) {
        int index = last_point_picking_event_->getPointIndex();
        pcl::PointXYZL pt;
        pt.label = 0;
        last_point_picking_event_->getPoint(pt.x, pt.y, pt.z);
        last_point_picking_event_ = boost::none;
        bool removed = false;
        using namespace pcl::utils;
        for (size_t i = 0; i < points->size(); ++i) {
          const auto& p = points->at(i);
          if (equal(p.x, pt.x) && equal(p.y, pt.y) && equal(p.z, pt.z)) {
            DEBUG("(tviewer) {{waitPointsSelected}} point with index {} removed from selection",
                  indices[i]);
            points->erase(points->begin() + i);
            indices.erase(indices.begin() + i);
            removed = true;
            break;
          }
        }
        if (!removed) {
          DEBUG("(tviewer) {{waitPointsSelected}} point with index {} added to selection", index);
          indices.push_back(index);
          points->push_back(pt);
        }
        update(id);
      }
      visualizer_->spinOnce(10);
    }
    removeWidget(id);
    return indices.size();
  }

  void saveCameras() {
    std::ofstream file(camera_persistence_);
    for (auto& vp : viewports_)
      if (vp.separate_camera)
        file << vp.camera->clip[0] << "," << vp.camera->clip[1] << "/" << vp.camera->focal[0] << ","
             << vp.camera->focal[1] << "," << vp.camera->focal[2] << "/" << vp.camera->pos[0] << ","
             << vp.camera->pos[1] << "," << vp.camera->pos[2] << "/" << vp.camera->view[0] << ","
             << vp.camera->view[1] << "," << vp.camera->view[2] << "/" << vp.camera->fovy << "/"
             << vp.camera->window_size[0] << "," << vp.camera->window_size[1] << "/"
             << vp.camera->window_pos[0] << "," << vp.camera->window_pos[1] << "\n";
  }

  void loadCameras() {
    std::vector<pcl::visualization::Camera> cameras;
    std::ifstream file(camera_persistence_);
    if (!file.is_open()) {
      DEBUG("(tviewer) {loadCameras} failed to open persistence file for reading");
      return;
    }
    std::string line;
    while (std::getline(file, line)) {
      std::vector<std::string> tokens;
      boost::split(tokens, line, boost::is_any_of("/"), boost::token_compress_on);
      // look for '/' as a separator
      if (tokens.size() != 7) {
        WARN("(tviewer) {{loadCameras}} invalid camera parameters string ({} tokens instead of 7)",
             tokens.size());
        continue;
      }

      std::string clip_str = tokens.at(0);
      std::string focal_str = tokens.at(1);
      std::string pos_str = tokens.at(2);
      std::string view_str = tokens.at(3);
      std::string fovy_str = tokens.at(4);
      std::string win_size_str = tokens.at(5);
      std::string win_pos_str = tokens.at(6);

      pcl::visualization::Camera camera;

      // Get each camera setting separately and parse for ','
      std::vector<std::string> clip_st;
      boost::split(clip_st, clip_str, boost::is_any_of(","), boost::token_compress_on);
      if (clip_st.size() != 2) {
        WARN("(tviewer) {loadCameras} invalid camera clipping angle");
        continue;
      }
      camera.clip[0] = atof(clip_st.at(0).c_str());
      camera.clip[1] = atof(clip_st.at(1).c_str());

      std::vector<std::string> focal_st;
      boost::split(focal_st, focal_str, boost::is_any_of(","), boost::token_compress_on);
      if (focal_st.size() != 3) {
        WARN("(tviewer) {loadCameras} invalid camera focal point");
        continue;
      }
      camera.focal[0] = atof(focal_st.at(0).c_str());
      camera.focal[1] = atof(focal_st.at(1).c_str());
      camera.focal[2] = atof(focal_st.at(2).c_str());

      std::vector<std::string> pos_st;
      boost::split(pos_st, pos_str, boost::is_any_of(","), boost::token_compress_on);
      if (pos_st.size() != 3) {
        WARN("(tviewer) {loadCameras} invalid camera position");
        continue;
      }
      camera.pos[0] = atof(pos_st.at(0).c_str());
      camera.pos[1] = atof(pos_st.at(1).c_str());
      camera.pos[2] = atof(pos_st.at(2).c_str());

      std::vector<std::string> view_st;
      boost::split(view_st, view_str, boost::is_any_of(","), boost::token_compress_on);
      if (view_st.size() != 3) {
        WARN("(tviewer) {loadCameras} invalid camera view up");
        continue;
      }
      camera.view[0] = atof(view_st.at(0).c_str());
      camera.view[1] = atof(view_st.at(1).c_str());
      camera.view[2] = atof(view_st.at(2).c_str());

      std::vector<std::string> fovy_size_st;
      boost::split(fovy_size_st, fovy_str, boost::is_any_of(","), boost::token_compress_on);
      if (fovy_size_st.size() != 1) {
        WARN("(tviewer) {loadCameras} invalid camera field of view angle");
        continue;
      }
      camera.fovy = atof(fovy_size_st.at(0).c_str());

      std::vector<std::string> win_size_st;
      boost::split(win_size_st, win_size_str, boost::is_any_of(","), boost::token_compress_on);
      if (win_size_st.size() != 2) {
        WARN("(tviewer) {loadCameras} invalid camera window size");
        continue;
      }
      camera.window_size[0] = atof(win_size_st.at(0).c_str());
      camera.window_size[1] = atof(win_size_st.at(1).c_str());

      std::vector<std::string> win_pos_st;
      boost::split(win_pos_st, win_pos_str, boost::is_any_of(","), boost::token_compress_on);
      if (win_pos_st.size() != 2) {
        WARN("(tviewer) {loadCameras} invalid camera window position");
        continue;
      }
      camera.window_pos[0] = atof(win_pos_st.at(0).c_str());
      camera.window_pos[1] = atof(win_pos_st.at(1).c_str());

      cameras.push_back(camera);
    }
    // Count viewports with separate cameras
    size_t num_viewports = std::count_if(viewports_.begin(), viewports_.end(),
                                         [](const auto& vp) { return vp.separate_camera; });
    if (cameras.size() != num_viewports)
      WARN(
          "(tviewer) {loadCameras} number of loaded cameras does not match the number of viewports "
          "with separate cameras");
    else {
      INFO("(tviewer) {{loadCameras}} read {} cameras from: {}", cameras.size(),
           camera_persistence_);
      auto iter = cameras.begin();
      for (auto& viewport : viewports_)
        if (viewport.separate_camera)
          viewport.camera = *(iter++);
    }
  }
};

}  // namespace tviewer
