/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <functional>

#include <boost/any.hpp>

#include <tviewer/fwd.h>

#include "logging.hpp"

namespace tviewer {

struct State : boost::any {};

template <typename T>
class Property {
 public:
  using ChangedCallback = std::function<void()>;

  Property(T value) : value_(value) {}

  Property(T value, const std::function<void()>& callback)
  : value_(value), changed_callback_(callback) {}

  /// Assign a value to the property.
  ///
  /// If the property is locked, this is a no-op. Otherwise if the old value is different from the
  /// new value, it is overwritten and the callback is fired.
  ///
  /// The definition of "different" depends on the type of the property:
  ///   - For floating point types, the values are considered different if their difference is
  ///     greater than 1e-6. Additionally, two NaNs are considered equal.
  ///   - For all other types, the values are compared using the `==` operator.
  Property& operator=(const T& rhs) {
    if (!locked_) {
      if (!areEqual(value_, rhs)) {
        DEBUG("property change: {} → {}", value_, rhs);
        value_ = rhs;
        if (changed_callback_)
          changed_callback_();
      }
    } else {
      TRACE("property change blocked: {} → {}", value_, rhs);
    }
    return *this;
  }

  template <typename U>
  Property& operator+=(const U& rhs) {
    return (*this = (value_ + rhs));
  }

  Property& operator-=(const T& rhs) {
    return (*this = (value_ - rhs));
  }

  Property& operator*=(const T& rhs) {
    return (*this = (value_ * rhs));
  }

  friend T operator*(const Property& lhs, const T& rhs) {
    return lhs.value_ * rhs;
  }

  T operator!() {
    return !value_;
  }

  operator T() const {
    return value_;
  }

 private:
  bool areEqual(const T& lhs, const T& rhs) const {
    if constexpr (std::is_floating_point_v<T>) {
      // Two NaNs are considered equal
      if (std::isnan(lhs) && std::isnan(rhs))
        return true;
      // Floating point values are compared with a tolerance
      constexpr T kTolerance = 1e-6;
      return std::abs(lhs - rhs) < kTolerance;
    } else {
      return lhs == rhs;
    }
  }

  T value_;
  std::function<void()> changed_callback_ = nullptr;
  bool locked_ = false;

  friend class PropertyLock;
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Property<T>& obj) {
  os << static_cast<T>(obj);
  return os;
}

/// This class locks a given property, preventing its value from being modified.
///
/// The lock is released when this object is destroyed.
///
/// Example usage:
///
/// \code
/// Property<int> property;
/// property = 42;                  // property value changed to 42
/// {
///   PropertyLock lock(property);  // property is locked
///   property = 1;                 // has no effect, the value is still 42
/// }                               // end of scope, property is unlocked
/// property += 1;                  // property value changed to 43
/// \endcode
class PropertyLock {
 public:
  template <typename T>
  explicit PropertyLock(Property<T>& property) : locked_(property.locked_) {
    locked_ = true;
  }

  ~PropertyLock() {
    locked_ = false;
  }

 private:
  bool& locked_;
};

}  // namespace tviewer
