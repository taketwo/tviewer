/******************************************************************************
 * Copyright (c) 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <iomanip>

#include "transform.h"

namespace Eigen {

std::ostream& operator<<(std::ostream& os, const Isometry3f& obj) {
  const Eigen::Translation3f t(obj.translation());
  const Eigen::Quaternionf q(obj.rotation());
  Eigen::Matrix<float, 1, 3> tv;
  tv << t.x(), t.y(), t.z();
  Eigen::Matrix<float, 1, 4> qv;
  qv << q.x(), q.y(), q.z(), q.w();
  constexpr unsigned int PRECISION = 3;
  static const Eigen::IOFormat EIGEN_FORMAT{PRECISION, Eigen::DontAlignCols, ", ", "\n", "", "", "",
                                            ""};
  auto flags = os.flags();
  os << std::fixed << tv.format(EIGEN_FORMAT) << "; " << qv.format(EIGEN_FORMAT);
  os.flags(flags);
  return os;
}

bool operator==(const Isometry3f& lhs, const Isometry3f& rhs) {
  return lhs.isApprox(rhs);
}

bool operator!=(const Isometry3f& lhs, const Isometry3f& rhs) {
  return !lhs.isApprox(rhs);
}

}  // namespace Eigen
