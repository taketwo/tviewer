/******************************************************************************
 * Copyright (c) 2014, 2018, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>

#include <tviewer/tviewer.h>

#include "logging.hpp"
#include "tviewer_impl.hpp"
#include "widgets/correspondences_widget.hpp"
#include "widgets/normal_cloud_widget.hpp"
#include "widgets/point_cloud_widget.hpp"
#include "widgets/point_selector_widget.hpp"
#include "widgets/poly_data_widget.hpp"
#include "widgets/polygons_widget.hpp"
#include "widgets/primitive_objects_widget.hpp"
#include "widgets/spinner_widget.hpp"
#include "widgets/text_2d_widget.hpp"
#include "widgets/toggle_widget.hpp"
#include "widgets/trigger_widget.hpp"
#include "widgets/variant_widget.hpp"
#include "widgets/widget.hpp"

#include <CLI/CLI.hpp>

namespace tviewer {

TViewer::TViewer() : p(new TViewerImpl) {
#ifdef ENABLE_LOGGING
  if (auto* const level_env = std::getenv("TV_LOG_LEVEL"))
    try {
      p->logger_.setLevel(boost::lexical_cast<unsigned int>(level_env));
    } catch (const boost::bad_lexical_cast&) {
    }
#endif
  if (auto* const camera_persistence_env = std::getenv("TV_CAMERA_PERSISTENCE"))
    p->camera_persistence_ = camera_persistence_env;
  if (auto* const bg_color_env = std::getenv("TV_BG_COLOR"))
    p->bg_color_ = Color(bg_color_env);
}

TViewer::TViewer(int argc, const char** argv) : p(new TViewerImpl) {
  CLI::App app;
  app.allow_extras();
  app.set_help_flag("--tv-help", "Print this help message and exit");
#ifdef ENABLE_LOGGING
  unsigned int level = 2;
  app.add_option("--tv-log-level", level, "Set log level.", true)->envname("TV_LOG_LEVEL");
#endif
  app.add_option("--tv-camera-persistence", p->camera_persistence_,
                 "Persist camera(s) into a given file")
      ->envname("TV_CAMERA_PERSISTENCE");
  std::string bg_color;
  app.add_option("--tv-bg-color", bg_color, "Set background color")->envname("TV_BG_COLOR");
  try {
    app.parse(argc, argv);
    if (!bg_color.empty())
      p->bg_color_ = Color(bg_color);
  } catch (const CLI::ParseError& e) {
    app.exit(e);
  }
#ifdef ENABLE_LOGGING
  p->logger_.setLevel(level);
#endif
}

TViewer::~TViewer() = default;

CorrespondencesWidgetProperties TViewer::addCorrespondences(const std::string& name) {
  return p->addWidget<CorrespondencesWidget>(name);
}

NormalCloudWidgetProperties TViewer::addNormalCloud(const std::string& name) {
  return p->addWidget<NormalCloudWidget>(name);
}

PointCloudWidgetProperties TViewer::addPointCloud(const std::string& name) {
  return p->addWidget<PointCloudWidget>(name);
}

PointSelectorWidgetProperties TViewer::addPointSelector(const std::string& name) {
  return p->addWidget<PointSelectorWidget>(name);
}

PolyDataWidgetProperties TViewer::addPolyData(const std::string& name) {
  return p->addWidget<PolyDataWidget>(name);
}

PolygonsWidgetProperties TViewer::addPolygons(const std::string& name) {
  return p->addWidget<PolygonsWidget>(name);
}

PrimitiveObjectsWidgetProperties TViewer::addPrimitiveObjects(const std::string& name) {
  return p->addWidget<PrimitiveObjectsWidget>(name);
}

SpinnerWidgetProperties TViewer::addSpinner(const std::string& name) {
  return p->addWidget<SpinnerWidget>(name);
}

Text2DWidgetProperties TViewer::addText2D(const std::string& name) {
  return p->addWidget<Text2DWidget>(name);
}

ToggleWidgetProperties TViewer::addToggle(const std::string& name) {
  return p->addWidget<ToggleWidget>(name);
}

TriggerWidgetProperties TViewer::addTrigger(const std::string& name) {
  return p->addWidget<TriggerWidget>(name);
}

VariantWidgetProperties TViewer::addVariant(const std::string& name) {
  return p->addWidget<VariantWidget>(name);
}

CorrespondencesWidgetProperties TViewer::getCorrespondences(const std::string& name) {
  return p->getWidget<CorrespondencesWidget>(name);
}

NormalCloudWidgetProperties TViewer::getNormalCloud(const std::string& name) {
  return p->getWidget<NormalCloudWidget>(name);
}

PointCloudWidgetProperties TViewer::getPointCloud(const std::string& name) {
  return p->getWidget<PointCloudWidget>(name);
}

PointSelectorWidgetProperties TViewer::getPointSelector(const std::string& name) {
  return p->getWidget<PointSelectorWidget>(name);
}

PolyDataWidgetProperties TViewer::getPolyData(const std::string& name) {
  return p->getWidget<PolyDataWidget>(name);
}

PolygonsWidgetProperties TViewer::getPolygons(const std::string& name) {
  return p->getWidget<PolygonsWidget>(name);
}

PrimitiveObjectsWidgetProperties TViewer::getPrimitiveObjects(const std::string& name) {
  return p->getWidget<PrimitiveObjectsWidget>(name);
}

SpinnerWidgetProperties TViewer::getSpinner(const std::string& name) {
  return p->getWidget<SpinnerWidget>(name);
}

Text2DWidgetProperties TViewer::getText2D(const std::string& name) {
  return p->getWidget<Text2DWidget>(name);
}

ToggleWidgetProperties TViewer::getToggle(const std::string& name) {
  return p->getWidget<ToggleWidget>(name);
}

TriggerWidgetProperties TViewer::getTrigger(const std::string& name) {
  return p->getWidget<TriggerWidget>(name);
}

VariantWidgetProperties TViewer::getVariant(const std::string& name) {
  return p->getWidget<VariantWidget>(name);
}

void TViewer::remove(const std::string& name) {
  p->removeWidget(name);
}

void TViewer::removeAll() {
  p->removeAllWidgets();
}

bool TViewer::contains(const std::string& name) const {
  return p->containsWidget(name);
}

void TViewer::enableGroup(const std::string& group) {
  return p->enableGroup(group);
}

void TViewer::disableGroup(const std::string& group) {
  return p->disableGroup(group);
}

void TViewer::setCameraIntrinsics(const Eigen::Matrix3d& intrinsics) {
  p->setCameraIntrinsics(intrinsics);
}

void TViewer::setCameraPose(const Eigen::Isometry3d& pose) {
  p->setCameraPose(pose);
}

void TViewer::setCameraParameters(const Eigen::Matrix3d& intrinsics,
                                  const Eigen::Isometry3d& extrinsics) {
  p->setCameraParameters(intrinsics, extrinsics);
}

void TViewer::addViewport(double xmin, double ymin, double xmax, double ymax,
                          bool separate_camera) {
  p->addViewport(xmin, ymin, xmax, ymax, separate_camera);
}

void TViewer::update(const std::string& name) {
  p->update(name);
}

void TViewer::run(KeepWindowOpen keep_open) {
  p->run(std::chrono::milliseconds(0), keep_open);
}

void TViewer::run(std::chrono::duration<float> duration, KeepWindowOpen keep_open) {
  p->run(duration, keep_open);
}

void TViewer::run(bool keep_open) {
  p->run(std::chrono::milliseconds(0), keep_open ? KeepWindowOpen::YES : KeepWindowOpen::NO);
}

void TViewer::run(std::chrono::duration<float> duration, bool keep_open) {
  p->run(duration, keep_open ? KeepWindowOpen::YES : KeepWindowOpen::NO);
}

void TViewer::stop() {
  p->stop();
}

bool TViewer::waitPointSelected(size_t& point_index) {
  return p->waitPointSelected(point_index);
}

unsigned int TViewer::waitPointsSelected(std::vector<int>& indices) {
  return p->waitPointsSelected(indices);
}

// void TViewer::update(const std::string& name) {
// auto vo = p->vo_.find(name);
// if (vo == p->vo_.end())
// throw ObjectNotFoundException(name); // TODO extract method
// vo->second->onUpdate({}); // TODO args
// }

void TViewer::setWindowTitle(const std::string& title) {
  p->window_title_ = title;
}

void TViewer::setBackgroundColor(Color color) {
  p->setBackgroundColor(color);
}

void TViewer::showFPS(bool show) {
  p->show_fps_ = show;
}

void TViewer::feedKey(const std::string& key) {
  p->feedKey(key);
}

std::weak_ptr<pcl::visualization::PCLVisualizer> TViewer::getVisualizer() const {
  return p->visualizer_;
}

}  // namespace tviewer
