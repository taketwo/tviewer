/******************************************************************************
 * Copyright (c) 2014, 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <set>
#include <string>

#include <tviewer/kwargs.h>

namespace tviewer {

class StateChangeEvent {
 public:
  StateChangeEvent(const Kwargs& state) : state_(state) {}

  StateChangeEvent(const Kwargs& state, const std::set<std::string>& changed)
  : state_(state), changed_(changed) {}

  bool changed(const std::string& key) const {
    return changed_.empty() || changed_.count(key);
  }

  bool changed(const std::set<std::string>& keys) const {
    auto result = changed_.empty();
    for (const auto& key : keys)
      result |= changed_.count(key);
    return result;
  }

  const Kwargs& state() const {
    return state_;
  }

 private:
  const Kwargs state_;
  /// Empty means everything changed.
  const std::set<std::string> changed_;
};

}  // namespace tviewer
