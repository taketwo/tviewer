/******************************************************************************
 * Copyright (c) 2019, 2024 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <tviewer/primitive_objects.h>

namespace tviewer {

Arrow Arrow::fromVectors(const Eigen::Ref<const Eigen::Vector3f>& source,
                         const Eigen::Ref<const Eigen::Vector3f>& target, Color color) {
  Arrow arrow;
  arrow.source = source;
  arrow.target = target;
  arrow.color = color;
  return arrow;
}

Arrow Arrow::fromVectors(const Eigen::Ref<const Eigen::Vector3d>& source,
                         const Eigen::Ref<const Eigen::Vector3d>& target, Color color) {
  return Arrow::fromVectors(source.cast<float>(), target.cast<float>(), color);
}

Axes::Axes(float _size) : size(_size) {}

Axes Axes::fromVector(const Eigen::Ref<const Eigen::Vector3f>& position, float size) {
  Axes axes;
  axes.position = position;
  axes.size = size;
  return axes;
}

Axes Axes::fromVector(const Eigen::Ref<const Eigen::Vector3d>& position, float size) {
  return Axes::fromVector(position.cast<float>(), size);
}

Axes Axes::fromOrientation(const Eigen::Ref<const Eigen::Matrix3f>& orientation, float size) {
  Axes axes;
  axes.position = Eigen::Vector3f::Zero();
  axes.orientation = Eigen::Quaternionf(orientation);
  axes.size = size;
  return axes;
}

Axes Axes::fromOrientation(const Eigen::Ref<const Eigen::Matrix3d>& orientation, float size) {
  return Axes::fromOrientation(orientation.cast<float>(), size);
}

LineSegment LineSegment::fromVectors(const Eigen::Ref<const Eigen::Vector3f>& start,
                                     const Eigen::Ref<const Eigen::Vector3f>& end, Color color) {
  LineSegment line_segment;
  line_segment.start = start;
  line_segment.end = end;
  line_segment.color = color;
  return line_segment;
}

LineSegment LineSegment::fromVectors(const Eigen::Ref<const Eigen::Vector3d>& start,
                                     const Eigen::Ref<const Eigen::Vector3d>& end, Color color) {
  return LineSegment::fromVectors(start.cast<float>(), end.cast<float>(), color);
}

Plane Plane::fromVectorAndNormal(const Eigen::Ref<const Eigen::Vector3f>& position,
                                 const Eigen::Ref<const Eigen::Vector3f>& normal,
                                 const Eigen::Vector2f& extents, Color color) {
  Plane plane;
  plane.position = position;
  plane.normal = normal;
  plane.extents = extents;
  plane.color = color;
  return plane;
}

Plane Plane::fromVectorAndNormal(const Eigen::Ref<const Eigen::Vector3d>& position,
                                 const Eigen::Ref<const Eigen::Vector3d>& normal,
                                 const Eigen::Vector2f& extents, Color color) {
  return Plane::fromVectorAndNormal(position.cast<float>(), normal.cast<float>(), extents, color);
}

Plane Plane::fromNormal(const Eigen::Ref<const Eigen::Vector3f>& normal,
                        const Eigen::Vector2f& extents, Color color) {
  return Plane::fromVectorAndNormal(Eigen::Vector3f(0, 0, 0), normal, extents, color);
}

Plane Plane::fromNormal(const Eigen::Ref<const Eigen::Vector3d>& normal,
                        const Eigen::Vector2f& extents, Color color) {
  return Plane::fromNormal(normal.cast<float>(), extents, color);
}

Plane Plane::fromModelCoefficients(const Eigen::VectorXf& coefficients,
                                   const Eigen::Vector2f& extents, Color color) {
  Eigen::Vector3f normal = coefficients.head<3>();
  Eigen::Vector3f point = -normal * coefficients[3];
  return Plane::fromVectorAndNormal(point, normal, extents, color);
}

Sphere Sphere::fromVector(const Eigen::Ref<const Eigen::Vector3f>& center, float radius,
                          Color color) {
  Sphere sphere;
  sphere.center = center;
  sphere.radius = radius;
  sphere.color = color;
  return sphere;
}

Sphere Sphere::fromVector(const Eigen::Ref<const Eigen::Vector3d>& center, float radius,
                          Color color) {
  return Sphere::fromVector(center.cast<float>(), radius, color);
}

Text Text::fromVector(const std::string& t, const Eigen::Ref<const Eigen::Vector3f>& position,
                      float scale, Color color) {
  Text text;
  text.text = t;
  text.position = position;
  text.scale = scale;
  text.color = color;
  return text;
}

Text Text::fromVector(const std::string& text, const Eigen::Ref<const Eigen::Vector3d>& position,
                      float scale, Color color) {
  return Text::fromVector(text, position.cast<float>(), scale, color);
}

Cylinder Cylinder::fromModelCoefficients(const Eigen::VectorXf& coefficients, Color color) {
  Cylinder cylinder;
  cylinder.axis = coefficients.middleRows(3, 3);
  cylinder.center = coefficients.head<3>();
  cylinder.radius = coefficients(6);
  cylinder.color = color;
  return cylinder;
}

Cylinder Cylinder::fromVectors(const Eigen::Ref<const Eigen::Vector3f>& start,
                               const Eigen::Ref<const Eigen::Vector3f>& end, float radius,
                               Color color) {
  Cylinder cylinder;
  cylinder.axis = start - end;
  cylinder.center = (start + end) / 2.0f;
  cylinder.radius = radius;
  cylinder.color = color;
  return cylinder;
}

Cylinder Cylinder::fromVectors(const Eigen::Ref<const Eigen::Vector3d>& start,
                               const Eigen::Ref<const Eigen::Vector3d>& end, float radius,
                               Color color) {
  return Cylinder::fromVectors(start.cast<float>(), end.cast<float>(), radius, color);
}

template <>
Arrow transform(const Arrow& arrow, const Transform& transform) {
  Arrow transformed;
  transformed.source = transform * arrow.source;
  transformed.target = transform * arrow.target;
  transformed.color = arrow.color;
  return transformed;
}

template <>
Axes transform(const Axes& axes, const Transform& transform) {
  Axes transformed;
  transformed.position = transform * axes.position;
  transformed.orientation = transform.linear() * axes.orientation.matrix();
  transformed.size = axes.size;
  return transformed;
}

template <>
Box transform(const Box& box, const Transform& transform) {
  Box transformed;
  transformed.center = transform * box.center;
  transformed.orientation = transform.linear() * box.orientation.matrix();
  transformed.dimensions = box.dimensions;
  return transformed;
}

template <>
Circle transform(const Circle& circle, const Transform& transform) {
  Circle transformed;
  transformed.center =
      (transform * Eigen::Vector3f(circle.center.x(), circle.center.y(), 0)).head<2>();
  transformed.radius = circle.radius;
  return transformed;
}

template <>
Cone transform(const Cone& cone, const Transform& transform) {
  Cone transformed;
  transformed.apex = transform * cone.apex;
  transformed.axis = transform * cone.axis;
  transformed.angle = cone.angle;
  return transformed;
}

template <>
Cylinder transform(const Cylinder& cylinder, const Transform& transform) {
  Cylinder transformed;
  transformed.center = transform * cylinder.center;
  transformed.axis = transform.linear() * cylinder.axis;
  transformed.radius = cylinder.radius;
  transformed.color = cylinder.color;
  return transformed;
}

template <>
LineSegment transform(const LineSegment& line_segment, const Transform& transform) {
  LineSegment transformed;
  transformed.start = transform * line_segment.start;
  transformed.end = transform * line_segment.end;
  transformed.color = line_segment.color;
  return transformed;
}

template <>
Plane transform(const Plane& plane, const Transform& transform) {
  Plane transformed;
  transformed.normal = transform.linear() * plane.normal;
  transformed.position = transform * plane.position;
  transformed.extents = plane.extents;
  transformed.color = plane.color;
  return transformed;
}

template <>
Sphere transform(const Sphere& sphere, const Transform& transform) {
  Sphere transformed;
  transformed.center = transform * sphere.center;
  transformed.radius = sphere.radius;
  transformed.color = sphere.color;
  return transformed;
}

template <>
Text transform(const Text& text, const Transform& transform) {
  Text transformed;
  transformed.text = text.text;
  transformed.position = transform * text.position;
  transformed.scale = text.scale;
  transformed.color = text.color;
  return transformed;
}

}  // namespace tviewer
