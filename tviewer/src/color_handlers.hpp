/******************************************************************************
 * Copyright (c) 2014, 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <pcl/visualization/point_cloud_color_handlers.h>

#include <tviewer/color.h>
#include <tviewer/widgets/point_cloud_widget_properties.h>

namespace tviewer {

// Several convenience usings to make some things below less verbose.
using WidgetColors = ::tviewer::PointCloudWidgetProperties::Colors;
template <typename PointT>
using ColorHandler = pcl::visualization::PointCloudColorHandler<PointT>;
template <typename PointT>
using ColorHandlerGenericField = pcl::visualization::PointCloudColorHandlerGenericField<PointT>;
template <typename PointT>
using ColorHandlerCustomColor = pcl::visualization::PointCloudColorHandlerCustom<PointT>;

// A convenience macro that makes a SFINAE-based color handler creator function.
// If a point type has a corresponding field, then a handler is created, otherwise null is returned.
#define MAKE_HANDLER_CREATOR(Name, Trait)                                                  \
  template <typename PointT>                                                               \
  typename std::enable_if<!pcl::traits::Trait<PointT>::value, ColorHandler<PointT>*>::type \
      create##Name##Handler() {                                                            \
    return nullptr;                                                                        \
  }                                                                                        \
  template <typename PointT>                                                               \
  typename std::enable_if<pcl::traits::Trait<PointT>::value, ColorHandler<PointT>*>::type  \
      create##Name##Handler() {                                                            \
    return new pcl::visualization::PointCloudColorHandler##Name##Field<PointT>;            \
  }

// Use macro to produce creators for Label, RGB, and RGBA-based handlers
MAKE_HANDLER_CREATOR(Label, has_label)
MAKE_HANDLER_CREATOR(RGB, has_color)
MAKE_HANDLER_CREATOR(RGBA, has_color)

// A visitor for the WidgetColors variant.
// Creates a handler based on the variant value and the point cloud type.
template <typename PointT>
struct CreateColorHandlerVisitor : public boost::static_visitor<ColorHandler<PointT>*> {
  // If variant is a string, create field-based handler.
  ColorHandler<PointT>* operator()(const std::string& field) const {
    bool field_auto = field == "";
    if ((field_auto || field == "rgb") && pcl::traits::has_color<PointT>::value)
      return createRGBHandler<PointT>();
    if ((field_auto || field == "rgba") && pcl::traits::has_color<PointT>::value)
      return createRGBAHandler<PointT>();
    if ((field_auto || field == "label") && pcl::traits::has_label<PointT>::value)
      return createLabelHandler<PointT>();
    if ((field_auto || field == "intensity") && pcl::traits::has_intensity<PointT>::value)
      return new ColorHandlerGenericField<PointT>("intensity");
    if ((field_auto || field == "curvature") && pcl::traits::has_curvature<PointT>::value)
      return new ColorHandlerGenericField<PointT>("curvature");
    return new ColorHandlerGenericField<PointT>(field);
  }

  // If variant is a color, create fixed color handler.
  ColorHandler<PointT>* operator()(const Color& color) const {
    uint8_t r, g, b;
    color.getRGB(r, g, b);
    return new ColorHandlerCustomColor<PointT>(r, g, b);
  }
};

// A helper function to create a color handler for a point cloud based on the WidgetColors variant.
template <typename PointT>
typename ColorHandler<PointT>::Ptr createHandler(
    const typename pcl::PointCloud<PointT>::ConstPtr& cloud, const std::string& name,
    const WidgetColors& colors) {
  CreateColorHandlerVisitor<PointT> create_handler;
  typename ColorHandler<PointT>::Ptr handler(boost::apply_visitor(create_handler, colors));
  handler->setInputCloud(cloud);
  if (!handler->isCapable()) {
    auto field = boost::get<std::string>(colors);
    if (field != "")
      pcl::console::print_error(
          "Unable to colorize point cloud \"%s\" using \"%s\" field, falling back to uniform white "
          "color\n",
          name.c_str(), field.c_str());
    handler.reset(new ColorHandlerCustomColor<PointT>(255, 255, 255));
    handler->setInputCloud(cloud);
  }
  return handler;
}

}  // namespace tviewer
