/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2024 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <sstream>
#include <unordered_map>

#include <boost/algorithm/string.hpp>

#include "key.h"

namespace {

std::string toCanonicalSymbol(const std::string& symbol) {
  auto result = symbol;
  if (result.size() > 1 || !isalpha(result[0]))
    boost::algorithm::to_lower(result);
  std::unordered_map<std::string, std::string> map = {
      {"enter", "return"}, {"esc", "escape"},   {"at", "@"},          {"equal", "="},
      {"less", "<"},       {"greater", ">"},    {"bracketleft", "["}, {"bracketright", "]"},
      {"left", "←"},       {"right", "→"},      {"apostrophe", "'"},  {"comma", ","},
      {"down", "↓"},       {"up", "↑"},         {"prior", "page up"}, {"next", "page down"},
      {"braceleft", "{"},  {"braceright", "}"}, {"parenleft", "("},   {"parenright", ")"}};
  if (map.count(result))
    result = map[result];
  return result;
}

}  // anonymous namespace

namespace tviewer {

bool matchKeys(const pcl::visualization::KeyboardEvent& key_event, const std::string& key) {
  // Special case, keys with Control or Alt: "C-x" or "A-x"
  if (key.size() >= 3 && key[1] == '-') {
    const std::string k = key.substr(2);
    if ((key[0] == 'A' && key_event.isAltPressed()) || (key[0] == 'C' && key_event.isCtrlPressed()))
      return k == key_event.getKeySym();
    else
      return false;
  }
  // Regular case
  if (!key_event.isAltPressed() && !key_event.isCtrlPressed())
    return key_event.getKeySym() == key;
  else
    return false;
}

std::string toKey(const pcl::visualization::KeyboardEvent& key_event) {
  std::stringstream key;
  if (key_event.isAltPressed())
    key << "A-";
  if (key_event.isCtrlPressed())
    key << "C-";
  if (key_event.isShiftPressed())
    key << "S-";
  key << key_event.getKeySym();
  return key.str();
}

std::string flipCtrl(const std::string& key) {
  auto flipped = key;
  auto pc = key.find("C-");
  auto pa = key.find("A-");
  if (pc != std::string::npos)
    flipped.replace(pc, 2, "");
  else if (pa != std::string::npos)
    flipped.replace(pa + 2, 0, "C-");
  else
    flipped.replace(0, 0, "C-");
  return flipped;
}

Key::Key(const pcl::visualization::KeyboardEvent& key_event)
: alt_(key_event.isAltPressed()), ctrl_(key_event.isCtrlPressed()),
  symbol_(toCanonicalSymbol(key_event.getKeySym())) {}

Key::Key(const std::string& code) : symbol_(code) {
  if (boost::algorithm::starts_with(symbol_, "A-")) {
    alt_ = true;
    symbol_ = symbol_.substr(2);
  }
  if (boost::algorithm::starts_with(symbol_, "C-")) {
    ctrl_ = true;
    symbol_ = symbol_.substr(2);
  }
  if (symbol_.empty())
    throw std::invalid_argument("empty key code");
  symbol_ = toCanonicalSymbol(symbol_);
}

Key::operator std::string() const {
  std::stringstream key;
  if (alt_)
    key << "A-";
  if (ctrl_)
    key << "C-";
  key << symbol_;
  return key.str();
}

bool Key::operator==(const Key& other) const {
  return symbol_ == other.symbol_ && alt_ == other.alt_ && ctrl_ == other.ctrl_;
}

void Key::toggleAlt() {
  alt_ = !alt_;
}

void Key::toggleCtrl() {
  ctrl_ = !ctrl_;
}

std::ostream& operator<<(std::ostream& os, const Key& obj) {
  os << static_cast<std::string>(obj);
  return os;
}

}  // namespace tviewer
