/******************************************************************************
 * Copyright (c) 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include "widgets/correspondences_widget.hpp"
#include "widgets/normal_cloud_widget.hpp"
#include "widgets/point_cloud_widget.hpp"
#include "widgets/point_selector_widget.hpp"
#include "widgets/poly_data_widget.hpp"
#include "widgets/polygons_widget.hpp"
#include "widgets/primitive_objects_widget.hpp"
#include "widgets/spinner_widget.hpp"
#include "widgets/text_2d_widget.hpp"
#include "widgets/toggle_widget.hpp"
#include "widgets/trigger_widget.hpp"
#include "widgets/variant_widget.hpp"

namespace tviewer {

template <typename T>
struct widget_traits {};

template <>
struct widget_traits<CorrespondencesWidget> {
  static constexpr const char* name = "correspondences";
};

template <>
struct widget_traits<NormalCloudWidget> {
  static constexpr const char* name = "normal cloud";
};

template <>
struct widget_traits<PointCloudWidget> {
  static constexpr const char* name = "point cloud";
};

template <>
struct widget_traits<PointSelectorWidget> {
  static constexpr const char* name = "point selector";
};

template <>
struct widget_traits<PolyDataWidget> {
  static constexpr const char* name = "poly data";
};

template <>
struct widget_traits<PolygonsWidget> {
  static constexpr const char* name = "polygons";
};

template <>
struct widget_traits<PrimitiveObjectsWidget> {
  static constexpr const char* name = "primitive objects";
};

template <>
struct widget_traits<SpinnerWidget> {
  static constexpr const char* name = "spinner";
};

template <>
struct widget_traits<Text2DWidget> {
  static constexpr const char* name = "text 2d";
};

template <>
struct widget_traits<ToggleWidget> {
  constexpr static const char* const name = "toggle";
};

template <>
struct widget_traits<TriggerWidget> {
  constexpr static const char* const name = "trigger";
};

template <>
struct widget_traits<VariantWidget> {
  constexpr static const char* const name = "variant";
};

}  // namespace tviewer
