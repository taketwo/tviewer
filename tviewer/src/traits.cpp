/******************************************************************************
 * Copyright (c) 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include "traits.h"

namespace tviewer {

constexpr const char* const widget_traits<CorrespondencesWidget>::name;
constexpr const char* const widget_traits<NormalCloudWidget>::name;
constexpr const char* const widget_traits<PointCloudWidget>::name;
constexpr const char* const widget_traits<PointSelectorWidget>::name;
constexpr const char* const widget_traits<PolyDataWidget>::name;
constexpr const char* const widget_traits<PolygonsWidget>::name;
constexpr const char* const widget_traits<PrimitiveObjectsWidget>::name;
constexpr const char* const widget_traits<SpinnerWidget>::name;
constexpr const char* const widget_traits<Text2DWidget>::name;
constexpr const char* const widget_traits<ToggleWidget>::name;
constexpr const char* const widget_traits<TriggerWidget>::name;
constexpr const char* const widget_traits<VariantWidget>::name;
}  // namespace tviewer
