/******************************************************************************
 * Copyright (c) 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#define PROPERTIES_CTOR                                                                \
  PROPERTIES_CLASS::PROPERTIES_CLASS(const std::shared_ptr<PROPERTIES_WIDGET>& object) \
  : obj(object) {}

#define PROPERTIES_GETTER(type, name, Name, default) \
  type PROPERTIES_CLASS::name() const {              \
    if (auto p = obj.lock())                         \
      return p->get##Name();                         \
    return default;                                  \
  }

#define PROPERTIES_SETTER(type, name, Name)             \
  PROPERTIES_CLASS& PROPERTIES_CLASS::name(type name) { \
    if (auto p = obj.lock())                            \
      p->set##Name(name);                               \
    return *this;                                       \
  }

#define PROPERTIES_SETTER_2(type, name, Name, arg1, arg2)          \
  PROPERTIES_CLASS& PROPERTIES_CLASS::name(type arg1, type arg2) { \
    if (auto p = obj.lock())                                       \
      p->set##Name(arg1, arg2);                                    \
    return *this;                                                  \
  }
