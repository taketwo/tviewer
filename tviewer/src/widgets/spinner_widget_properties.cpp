/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <tviewer/widgets/spinner_widget_properties.h>

#include "widgets/callback_widget_properties.hpp"
#include "widgets/spinner_widget.hpp"
#include "widgets/widget_properties.hpp"

namespace tviewer {

#define PROPERTIES_WIDGET SpinnerWidget
#define PROPERTIES_CLASS SpinnerWidgetProperties

#include "properties_def.hpp"

PROPERTIES_CTOR
PROPERTIES_GETTER(double, value, State, 0.0)
PROPERTIES_SETTER(double, value, Value)
PROPERTIES_SETTER(double, step, Step)
PROPERTIES_SETTER(bool, wrapAround, WrapAround)
PROPERTIES_SETTER(const std::string&, key, Key)
PROPERTIES_SETTER_2(const std::string&, key, Key, key_increment, key_decrement)
PROPERTIES_SETTER_2(double, range, Range, min, max)
PROPERTIES_SETTER(size_t, range, Range)

#include "properties_undef.hpp"

template class WidgetProperties<SpinnerWidgetProperties>;
template class CallbackWidgetProperties<SpinnerWidgetProperties, double>;

}  // namespace tviewer
