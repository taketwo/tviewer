/******************************************************************************
 * Copyright (c) 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <boost/algorithm/string/join.hpp>

#include <tviewer/widgets/trigger_widget_properties.h>

#include "widgets/callback_widget.hpp"

namespace tviewer {

/// Trigger widget
/// \see TriggerWidgetProperties
class TriggerWidget : public CallbackWidget<std::string> {
 public:
  /// Shared pointer to a trigger widget.
  using Ptr = std::shared_ptr<TriggerWidget>;

  using Properties = TriggerWidgetProperties;

  TriggerWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : CallbackWidget(name), state_change_callback_(state_change_callback) {}

  WidgetState getState() const override {
    const auto empty = key_.empty();
    DEBUG("({}) {{getState}} {}", name_, empty ? "not triggered" : ("triggered key: " + key_));
    return WidgetState(key_);
  };

  void setKey(const std::string& key) {
    DEBUG("({}) {{setKey}} trigger key: {}", name_, key);
    clearKeyCallbacks();
    registerKeyCallback(key, std::bind(&TriggerWidget::trigger, this, key));
  }

  void setKeys(const std::vector<std::string>& keys) {
    DEBUG("({}) {{setKey}} trigger keys: {}", name_, boost::algorithm::join(keys, ", "));
    clearKeyCallbacks();
    for (const auto& key : keys)
      registerKeyCallback(key, std::bind(&TriggerWidget::trigger, this, key));
  }

  void trigger(const std::string& key) {
    DEBUG("({}) {{trigger}} triggered key: {}", name_, key);
    key_ = key;
    state_change_callback_();
    key_ = "";
  }

 private:
  const std::function<void()> state_change_callback_;
  std::string key_;
};

}  // namespace tviewer
