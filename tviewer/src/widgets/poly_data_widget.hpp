/******************************************************************************
 * Copyright (c) 2014, 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <tviewer/fwd.h>
#include <tviewer/widgets/poly_data_widget_properties.h>

#include "widgets/data_visualization_widget.hpp"

namespace tviewer {

/// PolyData widget
/// \see PolyDataWidgetProperties
class PolyDataWidget : public DataVisualizationWidget<PolyDataPtr> {
 public:
  /// Shared pointer to a poly data widget.
  using Ptr = std::shared_ptr<PolyDataWidget>;

  using Properties = PolyDataWidgetProperties;

  using DataVisualizationWidget::DataVisualizationWidget;

  ~PolyDataWidget() {
    TRACE("({}) {{destructor}}", name_);
    VisualizationWidget::removeFromVisualizer();
  }

  void setRepresentation(Properties::Representation representation) {
    DEBUG("({}) {{setRepresentation}} representation: {}", name_, static_cast<int>(representation));
    representation_ = representation;
    // Always synchronize, even if the stored representation was the same, because the user may have
    // altered representation using keyboard shortcuts directly.
    synchronizeVisualization();
  }

 protected:
  void addToVisualizer(pcl::visualization::PCLVisualizer& v) override {
    DEBUG("({}) {{addToVisualizer}} ", name_);
    if (data_) {
      v.addModelFromPolyData(data_, name_, viewport_);
      v.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
                                    static_cast<double>(representation_), name_, viewport_);
    }
  }

  void removeFromVisualizer(pcl::visualization::PCLVisualizer& v) override {
    DEBUG("({}) {{removeFromVisualizer}} ", name_);
    if (v.contains(name_))
      v.removeShape(name_);
  }

 private:
  Properties::Representation representation_ = Properties::Representation::WIREFRAME;
};

}  // namespace tviewer
