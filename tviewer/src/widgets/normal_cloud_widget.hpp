/******************************************************************************
 * Copyright (c) 2014, 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <pcl/common/transforms.h>

#include <tviewer/fwd.h>
#include <tviewer/widgets/normal_cloud_widget_properties.h>

#include "widgets/data_visualization_widget.hpp"

namespace tviewer {

// A visitor for the NormalCloudConstPtr variant.
// Adds the contained normal cloud to the given visualizer. Nullptr values are ignored.
struct AddNormalCloudVisitor : public boost::static_visitor<> {
  pcl::visualization::PCLVisualizer& v_;
  const std::string& name_;
  unsigned int viewport_;
  unsigned int density_;
  float scale_;
  boost::optional<Transform> transform_;

  AddNormalCloudVisitor(pcl::visualization::PCLVisualizer& v, const std::string& name,
                        unsigned int viewport, unsigned int density, float scale,
                        const boost::optional<Transform>& transform)
  : v_(v), name_(name), viewport_(viewport), density_(density), scale_(scale),
    transform_(transform) {}

  template <typename T>
  void operator()(const T& cloud) {
    if (!cloud)
      return;

    using PointType = typename T::element_type::PointType;
    T maybe_transformed_cloud;
    if (transform_) {
      typename pcl::PointCloud<PointType>::Ptr cloud_transformed(new pcl::PointCloud<PointType>);
      pcl::transformPointCloudWithNormals<PointType>(*cloud, *cloud_transformed,
                                                     transform_->matrix());
      maybe_transformed_cloud = cloud_transformed;
    } else {
      maybe_transformed_cloud = cloud;
    }
    v_.addPointCloudNormals<PointType>(maybe_transformed_cloud, density_, scale_, name_, viewport_);
  }

  void operator()(const std::nullptr_t& /* cloud */) {}
};

/// Normal cloud widget
/// \see NormalCloudWidgetProperties
class NormalCloudWidget : public DataVisualizationWidget<NormalCloudConstPtr> {
 public:
  /// Shared pointer to a normal cloud widget.
  using Ptr = std::shared_ptr<NormalCloudWidget>;

  using Properties = NormalCloudWidgetProperties;

  using DataVisualizationWidget::DataVisualizationWidget;

  ~NormalCloudWidget() {
    TRACE("({}) {{destructor}}", name_);
    VisualizationWidget::removeFromVisualizer();
  }

  void setDensity(unsigned int density) {
    DEBUG("({}) {{setDensity}} density: {}", name_, density);
    if (density_ != density) {
      density_ = density;
      synchronizeVisualization();
    }
  }

  void setLength(float length) {
    DEBUG("({}) {{setLength}} length: {}", name_, length);
    if (length_ != length) {
      length_ = length;
      synchronizeVisualization();
    }
  }

 protected:
  void addToVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{addToVisualizer}} ", name_);
    AddNormalCloudVisitor add(v, name_, viewport_, density_, length_, transform_);
    boost::apply_visitor(add, data_);
  }

  void removeFromVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{removeFromVisualizer}} ", name_);
    if (v.contains(name_))
      v.removePointCloud(name_);
  }

 private:
  unsigned int density_ = 10;
  float length_ = 0.01f;
};

}  // namespace tviewer
