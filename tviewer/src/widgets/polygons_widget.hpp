/******************************************************************************
 * Copyright (c) 2020 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkLine.h>
#include <vtkPolyData.h>
#include <vtkPolyLine.h>
#include <vtkSmartPointer.h>

#include <pcl/common/colors.h>
#include <pcl/common/io.h>

#include <tviewer/fwd.h>
#include <tviewer/widgets/polygons_widget_properties.h>

#include "widgets/point_cloud_widget.hpp"
#include "widgets/poly_data_widget.hpp"

namespace tviewer {

// A visitor for the Polygon (PointCloudConstPtr) variant.
// Builds a pointcloud by concatenating points contained in the visited objects. Nullptr values are
// ignored. Each appended pointcloud gets a unique label.
struct BuildPointCloudVisitor : public boost::static_visitor<> {
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointcloud_;
  const std::vector<Color>& palette_;
  boost::optional<Transform> transform_;
  int index_;

  BuildPointCloudVisitor(const std::vector<Color>& palette,
                         const boost::optional<Transform>& transform)
  : pointcloud_(new pcl::PointCloud<pcl::PointXYZRGBA>), palette_(palette), transform_(transform),
    index_(0) {}

  template <typename T>
  void operator()(const T& cloud) {
    if (!cloud)
      return;
    pcl::PointCloud<pcl::PointXYZRGBA> cloud_copy;
    pcl::copyPointCloud(*cloud, cloud_copy);
    if (transform_)
      pcl::transformPointCloud(cloud_copy, cloud_copy, *transform_);
    for (auto& point : cloud_copy)
      point.rgba = palette_.at(index_);
    *pointcloud_ += cloud_copy;
    index_ = (index_ + 1) % palette_.size();
  }

  void operator()(const std::nullptr_t& /* cloud */) {}

  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr getPointCloud() {
    return pointcloud_;
  }
};

// A visitor for the Polygon (PointCloudConstPtr) variant.
// Builds a polydata object by concatenating polylines with points contained in the visited objects.
// Nullptr values are ignored. Each appended polyline gets a unique color.
struct BuildPolyDataVisitor : public boost::static_visitor<> {
  vtkSmartPointer<vtkCellArray> cells_;
  vtkSmartPointer<vtkPoints> points_;
  vtkSmartPointer<vtkUnsignedCharArray> colors_;
  const std::vector<Color>& palette_;
  boost::optional<Transform> transform_;
  int index_;

  BuildPolyDataVisitor(const std::vector<Color>& palette,
                       const boost::optional<Transform>& transform)
  : palette_(palette), transform_(transform), index_(0) {
    cells_ = vtkSmartPointer<vtkCellArray>::New();
    points_ = vtkSmartPointer<vtkPoints>::New();
    colors_ = vtkSmartPointer<vtkUnsignedCharArray>::New();
    colors_->SetNumberOfComponents(3);
  }

  template <typename T>
  void operator()(const T& cloud) {
    if (!cloud)
      return;

    uint8_t rgb[] = {0, 0, 0};
    palette_.at(index_).getRGB(rgb);

    auto iter = cloud->begin();

    vtkIdType num_points_before = points_->GetNumberOfPoints();
    vtkIdType num_points = 0;
    while (iter != cloud->end()) {
      Eigen::Vector3f point = iter->getVector3fMap();
      if (transform_)
        point = *transform_ * point;
      points_->InsertNextPoint(point.data());
      ++num_points;
      ++iter;
    }

    auto insert_next_cell = [&](vtkCell* cell) {
      cells_->InsertNextCell(cell);
      colors_->InsertNextTypedTuple(rgb);
    };

    for (vtkIdType i = 0; i < num_points - 1; ++i) {
      vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
      line->GetPointIds()->SetId(0, num_points_before + i);
      line->GetPointIds()->SetId(1, num_points_before + i + 1);
      insert_next_cell(line);
    }
    vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
    line->GetPointIds()->SetId(0, num_points_before + num_points - 1);
    line->GetPointIds()->SetId(1, num_points_before);
    insert_next_cell(line);

    index_ = (index_ + 1) % palette_.size();
  }

  void operator()(const std::nullptr_t& /* cloud */) {}

  vtkSmartPointer<vtkPolyData> getPolyData() {
    vtkSmartPointer<vtkPolyData> data = vtkSmartPointer<vtkPolyData>::New();
    data->SetPoints(points_);
    data->SetLines(cells_);
    data->GetCellData()->SetScalars(colors_);
    return data;
  }
};

/// Polygons widget
/// \see PolygonsWidgetProperties
class PolygonsWidget : public DataVisualizationWidget<Polygons> {
 public:
  /// Shared pointer to a point cloud widget.
  using Ptr = std::shared_ptr<PolygonsWidget>;

  using Properties = PolygonsWidgetProperties;

  using DataVisualizationWidget::DataVisualizationWidget;

  PolygonsWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : DataVisualizationWidget(name, state_change_callback), colors_(GetGlasbeyColors()) {
    TRACE("({}) {{constructor}} creating auxiliary point cloud widget", name_);
    aux_point_cloud_.reset(new PointCloudWidget(name + "/vertices", [] {}));
    aux_point_cloud_->setVisibility(false);  // initially hidden
    TRACE("({}) {{constructor}} creating auxiliary poly data widget", name_);
    aux_poly_data_.reset(new PolyDataWidget(name + "/edges", [] {}));
    aux_poly_data_->setVisibility(false);  // initially hidden
  }

  ~PolygonsWidget() {
    TRACE("({}) {{destructor}}", name_);
  }

  void setPointSize(unsigned int point_size) {
    DEBUG("({}) {{setPointSize}} point size: {}", name_, point_size);
    aux_point_cloud_->setPointSize(point_size);
  }

  void setColors(const std::vector<Color>& colors) {
    if (colors.empty()) {
      DEBUG("({}) {{setColors}} automatic from Glasbey LUT", name_);
      colors_ = GetGlasbeyColors();
    } else {
      DEBUG("({}) {{setColors}} cycle through palette (size: {})", name_, colors.size());
      colors_ = colors;
    }
  }

  std::vector<Widget::Ptr> getAuxiliaryWidgets() const override {
    return {aux_point_cloud_, aux_poly_data_};
  }

 protected:
  void addToVisualizer(pcl::visualization::PCLVisualizer& /* v */) override {
    DEBUG("({}) {{addToVisualizer}} ", name_);
    BuildPointCloudVisitor build_pointcloud(colors_, transform_);
    BuildPolyDataVisitor build_polydata(colors_, transform_);
    for (const auto& polygon : data_) {
      boost::apply_visitor(build_pointcloud, polygon);
      boost::apply_visitor(build_polydata, polygon);
    }
    DEBUG("({}) {{addToVisualizer}} showing auxiliary point cloud widget", name_);
    aux_point_cloud_->setData(build_pointcloud.getPointCloud());
    aux_point_cloud_->setVisibility(true);
    DEBUG("({}) {{addToVisualizer}} showing auxiliary poly data widget", name_);
    aux_poly_data_->setData(build_polydata.getPolyData());
    aux_poly_data_->setVisibility(true);
  }

  void removeFromVisualizer(pcl::visualization::PCLVisualizer& /* v */) override {
    DEBUG("({}) {{removeFromVisualizer}} ", name_);
    DEBUG("({}) {{removeFromVisualizer}} hiding auxiliary point cloud widget", name_);
    aux_point_cloud_->setVisibility(false);
    DEBUG("({}) {{removeFromVisualizer}} hiding auxiliary poly data widget", name_);
    aux_poly_data_->setVisibility(false);
  }

 private:
  static std::vector<Color> GetGlasbeyColors() {
    std::vector<Color> colors;
    for (size_t i = 0; i < pcl::GlasbeyLUT::size(); ++i)
      colors.push_back(pcl::GlasbeyLUT::at(i).rgba);
    return colors;
  }

  PointCloudWidget::Ptr aux_point_cloud_;
  PolyDataWidget::Ptr aux_poly_data_;
  std::vector<Color> colors_;
};

}  // namespace tviewer
