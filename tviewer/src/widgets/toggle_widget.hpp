/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <tviewer/widgets/toggle_widget_properties.h>

#include "property.hpp"
#include "widgets/callback_widget.hpp"

namespace tviewer {

/// toggle widget
/// \see ToggleWidgetProperties
class ToggleWidget : public CallbackWidget<bool> {
 public:
  /// Shared pointer to a toggle widget.
  using Ptr = std::shared_ptr<ToggleWidget>;

  using Properties = ToggleWidgetProperties;

  ToggleWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : CallbackWidget(name), value_(false, state_change_callback) {}

  WidgetState virtual getState() const override {
    DEBUG("({}) {{getState}} value: {}", name_, value_);
    return value_;
  };

  void setKey(const std::string& key) {
    DEBUG("({}) {{setKey}} toggle key: {}", name_, key);
    clearKeyCallbacks();
    registerKeyCallback(key, &ToggleWidget::toggle, this);
  }

  void setValue(bool value) {
    value_ = value;
  }

  void toggle() {
    value_ = !value_;
  }

 private:
  Property<bool> value_;
};

}  // namespace tviewer
