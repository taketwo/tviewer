/******************************************************************************
 * Copyright (c) 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <boost/algorithm/string/join.hpp>

#include <tviewer/exceptions.h>
#include <tviewer/widgets/variant_widget_properties.h>

#include "property.hpp"
#include "widgets/callback_widget.hpp"

namespace tviewer {

/// Variant widget
/// \see VariantWidgetProperties
class VariantWidget : public CallbackWidget<std::string> {
 public:
  /// Shared pointer to a variant widget.
  using Ptr = std::shared_ptr<VariantWidget>;

  using Properties = VariantWidgetProperties;

  VariantWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : CallbackWidget(name), value_index_(0, state_change_callback) {}

  WidgetState virtual getState() const override {
    auto value = variants_.at(value_index_);
    DEBUG("({}) {{getState}} value: {}", name_, value);
    return value;
  };

  void getHelpInfo(std::string& icon, std::string& description, std::vector<Key>& keys,
                   std::string& extra) override {
    Widget::getHelpInfo(icon, description, keys, extra);
    icon = "  ❖  ";
    extra = std::string("Value: ") + variants_.at(value_index_);
  }

  void setKey(const Key& key_next) {
    DEBUG("({}) {{setKey}} next key: {}", name_, key_next);
    clearKeyCallbacks();
    registerKeyCallback(key_next, &VariantWidget::next, this);
  }

  void setKey(const Key& key_next, const Key& key_previous) {
    DEBUG("({}) {{setKey}} next/previous keys: {}/{}", name_, key_next, key_previous);
    clearKeyCallbacks();
    registerKeyCallback(key_next, &VariantWidget::next, this);
    registerKeyCallback(key_previous, &VariantWidget::previous, this);
  }

  /// Set value.
  /// Value should belong to the variants list. If not a warning is generated.
  void setValue(const std::string& value) {
    auto iter = std::find(variants_.begin(), variants_.end(), value);
    if (iter == variants_.end())
      WARN("({}) {{setValue}} invalid variant value: {}, ignoring", name_, value);
    else
      value_index_ = std::distance(variants_.begin(), iter);
  }

  /// Set value by providing an index into the vector of variants.
  /// Setting an index outside of the valid range has no effect and generates a warning.
  void setValueIndex(size_t value_index) {
    if (value_index >= variants_.size())
      WARN("({}) {{setValue}} invalid value index: {}, ignoring", name_, value_index);
    else
      value_index_ = value_index;
  }

  void setVariants(const std::vector<std::string>& variants) {
    if (variants.empty()) {
      ERROR("({}) {{setVariants}} empty list of variants is not allowed", name_);
      throw ConfigurationError("VariantWidget is not allowed to have empty list of variants");
    }
    DEBUG("({}) {{setVariants}} variants: {}", name_, boost::algorithm::join(variants, ", "));
    variants_ = variants;
    value_index_ = 0;
  }

  void next() {
    if (value_index_ == variants_.size() - 1)
      setValueIndex(0);
    else
      setValueIndex(value_index_ + 1);
  }

  void previous() {
    if (value_index_ == 0)
      setValueIndex(variants_.size() - 1);
    else
      setValueIndex(value_index_ - 1);
  }

 private:
  Property<size_t> value_index_;
  std::vector<std::string> variants_;
};

}  // namespace tviewer
