/******************************************************************************
 * Copyright (c) 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <pcl/common/utils.h>

#include <tviewer/widgets/point_cloud_widget_properties.h>
#include <tviewer/widgets/point_selector_widget_properties.h>

#include "widgets/callback_widget.hpp"
#include "widgets/point_cloud_widget.hpp"

namespace tviewer {

/// Point selector widget
/// \see PointSelectorWidgetProperties
class PointSelectorWidget : public CallbackWidget<std::vector<int>> {
 public:
  /// Shared pointer to a point selector widget.
  using Ptr = std::shared_ptr<PointSelectorWidget>;

  using Properties = PointSelectorWidgetProperties;

  PointSelectorWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : CallbackWidget(name), state_change_callback_(state_change_callback) {
    TRACE("({}) {{constructor}} creating auxiliary point cloud widget", name_);
    aux_point_cloud_.reset(new PointCloudWidget(name + "/selected points", [] {}));
    points_.reset(new pcl::PointCloud<pcl::PointXYZ>);
    aux_point_cloud_->setData(points_);
    aux_point_cloud_->setPointSize(10);
  }

  ~PointSelectorWidget() {
    TRACE("({}) {{destructor}}", name_);
  }

  bool handlePointPickingEvent(const pcl::visualization::PointPickingEvent& event) override {
    auto point_index = event.getPointIndex();
    DEBUG("({}) {{handlePointPickingEvent}} point with index {} picked", name_, point_index);

    // Selection was completed recently, so now we need to reset indices and visualization cloud
    if (selection_completed_) {
      selection_completed_ = false;
      point_indices_.clear();
      points_->clear();
    }

    pcl::PointXYZ point;
    event.getPoint(point.x, point.y, point.z);

    bool duplicate = false;
    using namespace pcl::utils;
    for (size_t i = 0; i < points_->size(); ++i) {
      const auto& p = points_->at(i);
      if (equal(p.x, point.x) && equal(p.y, point.y) && equal(p.z, point.z)) {
        duplicate = true;
        if (enable_deselection_) {
          DEBUG("({}) {{handlePointPickingEvent}} index {} removed from selection", name_,
                point_index);
          points_->erase(points_->begin() + i);
          point_indices_.erase(point_indices_.begin() + i);
        }
        break;
      }
    }
    if (!duplicate) {
      DEBUG("({}) {{handlePointPickingEvent}} index {} added to selection", name_, point_index);
      point_indices_.push_back(point_index);
      points_->push_back(point);
    }

    aux_point_cloud_->setData(points_);
    aux_point_cloud_->setVisibility(show_selected_);
    if (point_indices_.size() == num_points_)
      endSelection();
    return true;
  }

  WidgetState getState() const override {
    DEBUG("({}) {{getState}} number of points selected: {}", name_, point_indices_.size());
    return WidgetState(point_indices_);
  };

  std::vector<Widget::Ptr> getAuxiliaryWidgets() const override {
    return {aux_point_cloud_};
  }

  void setNumPoints(size_t num_points) {
    DEBUG("({}) {{setNumPoints}} number of points: {}", name_, num_points);
    num_points_ = num_points;
  }

  void setKey(const Key& key) {
    DEBUG("({}) {{setKey}} end selection key: {}", name_, key);
    clearKeyCallbacks();
    registerKeyCallback(key, &PointSelectorWidget::endSelection, this);
  }

  void setShowSelected(bool show) {
    DEBUG("({}) {{setShowSelected}} show selected: {}", name_, show);
    show_selected_ = show;
  }

  void setHideSelectedAfterComplete(bool hide) {
    DEBUG("({}) {{setHideSelectedAfterComplete}} hide selected after complete: {}", name_, hide);
    hide_selected_after_complete_ = hide;
  }

  void setEnableDeselection(bool enable) {
    DEBUG("({}) {{setEnableDeselection}} enable deselection: {}", name_, enable);
    enable_deselection_ = enable;
  }

  pcl::PointCloud<pcl::PointXYZ>::ConstPtr getPoints() const {
    DEBUG("({}) {{getSelectedPoints}} number of point selected: {}", name_, points_->size());
    return points_;
  }

  void clear() {
    DEBUG("({}) {{clear}}", name_);
    selection_completed_ = false;
    point_indices_.clear();
    points_->clear();
    aux_point_cloud_->setVisibility(false);
  }

 private:
  void endSelection() {
    TRACE("({}) {{endSelection}}", name_);
    selection_completed_ = true;
    state_change_callback_();
    if (hide_selected_after_complete_)
      aux_point_cloud_->setVisibility(false);
  }

  const std::function<void()> state_change_callback_;
  bool selection_completed_ = true;
  Properties::PointIndices point_indices_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr points_;
  size_t num_points_ = 1;
  bool show_selected_ = true;
  bool hide_selected_after_complete_ = false;
  bool enable_deselection_ = false;
  PointCloudWidget::Ptr aux_point_cloud_;
};

}  // namespace tviewer
