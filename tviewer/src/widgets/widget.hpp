/******************************************************************************
 * Copyright (c) 2014, 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

#include <boost/any.hpp>

#include <pcl/visualization/pcl_visualizer.h>

#include "key.h"
#include "logging.hpp"
#include "property.hpp"
#include "state_change_event.hpp"

namespace tviewer {

class WidgetState {
 public:
  WidgetState() = default;

  template <typename T>
  WidgetState(const T& value) : value_(value) {}

  template <typename T>
  WidgetState(const Property<T>& variable) : value_(static_cast<T>(variable)) {}

  const boost::any& getValue() const {
    return value_;
  }

  template <typename T>
  T getValue() const {
    return boost::any_cast<T>(value_);
  }

  template <typename T>
  operator T() const {
    return getValue<T>();
  }

 private:
  boost::any value_;
};

class TViewerImpl;

/// Abstract base class for various widgets.
///
/// A widget is an object that handles keyboard and point picking events, can represent itself in a
/// PCL Visualizer.
///
/// This serves two purposes:
///   * Acts as an interface through which TViewer instance manages objects;
///   * Hosts logic and member fields shared by all widgets.
class Widget {
 public:
  /// Shared pointer to a widget.
  using Ptr = std::shared_ptr<Widget>;

  /// Construct a widget with a given name.
  Widget(const std::string& name) : name_(name) {
    TRACE("({}) {{constructor}}", name_);
  }

  /// Virtual destructor.
  virtual ~Widget() {}

  /// A handler for keyboard events.
  ///
  /// This function is called by the managing TViewer instance whenever a keyboard event is
  /// received. It checks if there is a callback associated with the pressed key and executes it.
  ///
  /// Deriving classes that want to react to keyboard events should register callbacks using
  /// registerCallback().
  ///
  /// \returns a flag indicating whether the event was handled
  bool handleKeyboardEvent(const Key& key) {
    auto callback = keyboard_callbacks_.find(key);
    if (callback == keyboard_callbacks_.end()) {
      TRACE("({}) {{handleKeyboardEvent}} ignoring: {} keypress", name_, key);
      return false;
    }
    DEBUG("({}) {{handleKeyboardEvent}} executing callback for: {} keypress", name_, key);
    callback->second();
    return true;
  }

  /// A handler for point picking events.
  ///
  /// This function is called by the managing TViewer instance whenever a point picking event is
  /// received.
  ///
  /// Deriving classes that want to react to point picking events should implement this handler.
  ///
  /// \returns a flag indicating whether the event was handled
  virtual bool handlePointPickingEvent(const pcl::visualization::PointPickingEvent& /* event */) {
    TRACE("({}) {{handlePointPickingEvent}} ignoring", name_);
    return false;
  }

  /// A handler for state change events.
  ///
  /// This function is called by the managing TViewer instance every time one of the widgets
  /// existing in the system (including this widget itself) changes its state.
  ///
  /// Deriving classes that want to react to state changes should implement this handler.
  virtual void handleStateChangeEvent(const StateChangeEvent& /* event */) {
    TRACE("({}) {{handleStateChangeEvent}} ignoring", name_);
  }

  /// A function to supply an active PCL Visualizer object to the widget.
  ///
  /// This function is called by the managing TViewer instance wheneven a PCL Visualizer window
  /// is created.
  ///
  /// Deriving classes that perform visualization (and thus need to interact with the visualizer)
  /// should implement this method and store the passed instance.
  virtual void injectVisualizer(std::shared_ptr<pcl::visualization::PCLVisualizer> /* v */) {
    TRACE("({}) {{injectVisualizer}} ignoring", name_);
  }

  /// A function to remove Visualizer object from the widget (if any).
  ///
  /// This function is called by the managing TViewer instance wheneven it needs to remove the
  /// widget from an existing PCL Visualizer window.
  ///
  /// Deriving classes that perform visualization (and thus need to interact with the visualizer)
  /// should implement this method.
  virtual void ejectVisualizer() {
    TRACE("({}) {{ejectVisualizer}} ignoring", name_);
  }

  /// Get the state of the widget.
  ///
  /// This function is called by the managing TViewer instance to query the state of the widget.
  ///
  /// Deriving classes that have a state should override this. The default implementation outputs an
  /// empty state.
  virtual WidgetState getState() const {
    TRACE("({}) {{getState}} no state", name_);
    return WidgetState();
  };

  /// Get bits of information needed to display help about the widget.
  ///
  /// This function is called by the managing TViewer instance to query information needed to
  /// display a help table about the existing widgets. This includes:
  /// * \a icon: a unicode symbol that somehow conveys the state or function of the widget
  /// * \a description
  /// * \a keys: keyboard shortcuts that the widget reacts to
  /// * \a extra: additional string of any format
  ///
  /// The default implementation outputs an empty/crossed box (☐/☒) in case the widget has a boolean
  /// state, widget name as a description, automatically collects the keys, and no extra
  /// information.
  virtual void getHelpInfo(std::string& icon, std::string& description, std::vector<Key>& keys,
                           std::string& extra) {
    try {
      icon = boost::any_cast<bool>(getState().getValue()) ? "  ☒  " : "  ☐  ";
    } catch (boost::bad_any_cast&) {
      icon = "";
    }
    description = name_;
    keys.clear();
    std::transform(keyboard_callbacks_.cbegin(), keyboard_callbacks_.cend(),
                   std::back_inserter(keys), [](const auto& key) { return key.first; });
    extra = "";
  }

  /// Get auxiliary widgets for this widget.
  ///
  /// Deriving classes that need auxiliary widgets to operate should create such widgets in their
  /// constructors. They should also implement this method and return the list of these widgets so
  /// that the managing TViewer instance can register them.
  virtual std::vector<Widget::Ptr> getAuxiliaryWidgets() const {
    return {};
  }

  /// Get widget name.
  std::string getName() const {
    return name_;
  }

  /// Get the group to which the widget belongs.
  std::string getGroup() const {
    return group_;
  }

  /// Set group to which the widget belongs.
  void setGroup(const std::string& group) {
    DEBUG("({}) {{setGroup}} group: {}", name_, group);
    group_ = group;
  }

 protected:
  /// Associate a callback function with a given key.
  /// \see handleKeyboardEvent()
  void registerKeyCallback(const Key& key, const std::function<void()>& callback) {
    TRACE("({}) {{registerKeyCallback}} key: {}", name_, key);
    keyboard_callbacks_[key] = callback;
  }

  /// Associate a callback function (method of an object) with a given key.
  /// \see handleKeyboardEvent()
  template <typename F, typename T>
  void registerKeyCallback(const Key& key, F method, T* object) {
    TRACE("({}) {{registerKeyCallback}} key: {}", name_, key);
    keyboard_callbacks_[key] = std::bind(method, object);
  }

  /// Remove all installed callbacks.
  void clearKeyCallbacks() {
    TRACE("({}) {{clearKeyCallbacks}} number of registered callbacks: {}", name_,
          keyboard_callbacks_.size());
    keyboard_callbacks_.clear();
  }

  /// The name of the widget.
  const std::string name_;

  /// The group to which the widget belongs.
  std::string group_ = "default";

  /// A map associating keys to callbacks.
  std::unordered_map<Key, std::function<void()>> keyboard_callbacks_;
};

}  // namespace tviewer
