/******************************************************************************
 * Copyright (c) 2018, 2019, 2024 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <boost/algorithm/string/predicate.hpp>
#include <boost/format.hpp>

#include <vtkCubeSource.h>
#include <vtkPlaneSource.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkTubeFilter.h>

#include <pcl/common/angles.h>

#include <tviewer/color.h>
#include <tviewer/fwd.h>
#include <tviewer/primitive_objects.h>
#include <tviewer/widgets/primitive_objects_widget_properties.h>

// #include "color_handlers.hpp"
#include "widgets/data_visualization_widget.hpp"

namespace tviewer {

// A visitor for the PrimitiveObject variant.
// Adds the contained object to the given visualizer.
struct AddPrimitiveObjectVisitor : public boost::static_visitor<> {
  pcl::visualization::PCLVisualizer& v_;
  const std::string& name_;
  unsigned int viewport_;
  pcl::visualization::ShadingRepresentationProperties shading_;
  pcl::visualization::RenderingRepresentationProperties representation_;
  float line_width_;
  std::vector<std::string>& objects_;
  boost::format fmt_ = boost::format("%s-%i-%s");

  AddPrimitiveObjectVisitor(pcl::visualization::PCLVisualizer& v, const std::string& name,
                            unsigned int viewport, bool flat_shading, bool wireframe,
                            float line_width, std::vector<std::string>& objects)
  : v_(v), name_(name), viewport_(viewport),
    shading_(flat_shading ? pcl::visualization::PCL_VISUALIZER_SHADING_FLAT
                          : pcl::visualization::PCL_VISUALIZER_SHADING_PHONG),
    representation_(wireframe ? pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME
                              : pcl::visualization::PCL_VISUALIZER_REPRESENTATION_SURFACE),
    line_width_(line_width), objects_(objects) {}

  const std::string& makeId(const std::string& type) {
    objects_.push_back(boost::str(fmt_ % name_ % objects_.size() % type));
    return objects_.back();
  }

  void operator()(const Arrow& arrow) {
    pcl::PointXYZ p1, p2;
    p1.getVector3fMap() = arrow.source;
    p2.getVector3fMap() = arrow.target;
    float r, g, b;
    std::tie(r, g, b) = arrow.color.getRGB();
    // The arrow head is attached to the first point, so we pass the target first.
    v_.addArrow(p2, p1, r, g, b, false, makeId("arrow"), viewport_);
  }

  void operator()(const Axes& axes) {
    v_.addCoordinateSystem(axes.size, Eigen::Translation3f{axes.position} * axes.orientation,
                           makeId("axes"), viewport_);
  }

  void operator()(const Box& box) {
    const auto& id = makeId("box");

    vtkSmartPointer<vtkCubeSource> data_source = vtkSmartPointer<vtkCubeSource>::New();
    data_source->SetXLength(box.dimensions[0]);
    data_source->SetYLength(box.dimensions[1]);
    data_source->SetZLength(box.dimensions[2]);

    vtkSmartPointer<vtkTransform> tf = vtkSmartPointer<vtkTransform>::New();
    tf->Identity();
    tf->Translate(box.center.x(), box.center.y(), box.center.z());
    Eigen::AngleAxisf a(box.orientation);
    tf->RotateWXYZ(pcl::rad2deg(a.angle()), a.axis().x(), a.axis().y(), a.axis().z());

    vtkSmartPointer<vtkTransformPolyDataFilter> tf_filter =
        vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    tf_filter->SetTransform(tf);
    tf_filter->SetInputConnection(data_source->GetOutputPort());
    tf_filter->Update();

    v_.addModelFromPolyData(tf_filter->GetOutput(), id, viewport_);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, shading_, id);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
                                   representation_, id);
    auto actor = vtkLODActor::SafeDownCast(v_.getShapeActorMap()->at(id));
    float r, g, b;
    std::tie(r, g, b) = box.color.getRGB();
    actor->GetProperty()->SetColor(r, g, b);
  }

  void operator()(const Circle& circle) {
    pcl::ModelCoefficients coefficients;
    coefficients.values.resize(3);
    coefficients.values[0] = circle.center.x();
    coefficients.values[1] = circle.center.y();
    coefficients.values[2] = circle.radius;
    const auto& id = makeId("circle");
    v_.addCircle(coefficients, id, viewport_);
  }

  void operator()(const Cone& cone) {
    pcl::ModelCoefficients coefficients;
    coefficients.values.resize(7);
    std::memcpy(&coefficients.values[0], cone.apex.data(), 3 * sizeof(float));
    std::memcpy(&coefficients.values[3], cone.axis.data(), 3 * sizeof(float));
    coefficients.values[6] = cone.angle;
    const auto& id = makeId("cone");
    v_.addCone(coefficients, id, viewport_);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, shading_, id);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
                                   representation_, id);
  }

  void operator()(const Cylinder& cylinder) {
    const auto& id = makeId("cylinder");

    vtkSmartPointer<vtkLineSource> data_source = vtkSmartPointer<vtkLineSource>::New();
    Eigen::Vector3f begin = cylinder.center - (cylinder.axis / 2);
    data_source->SetPoint1(begin[0], begin[1], begin[2]);
    Eigen::Vector3f end = cylinder.center + (cylinder.axis / 2);
    data_source->SetPoint2(end[0], end[1], end[2]);

    vtkSmartPointer<vtkTubeFilter> tube_filter = vtkSmartPointer<vtkTubeFilter>::New();
    tube_filter->SetInputConnection(data_source->GetOutputPort());
    tube_filter->SetRadius(cylinder.radius);
    tube_filter->SetNumberOfSides(30);
    tube_filter->Update();

    v_.addModelFromPolyData(tube_filter->GetOutput(), id, viewport_);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, shading_, id);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
                                   representation_, id);

    auto actor = vtkLODActor::SafeDownCast(v_.getShapeActorMap()->at(id));
    float r, g, b;
    std::tie(r, g, b) = cylinder.color.getRGB();
    actor->GetProperty()->SetColor(r, g, b);
  }

  void operator()(const LineSegment& line_segment) {
    pcl::PointXYZ p1, p2;
    p1.getVector3fMap() = line_segment.start;
    p2.getVector3fMap() = line_segment.end;
    float r, g, b;
    std::tie(r, g, b) = line_segment.color.getRGB();
    const auto& id = makeId("line_segment");
    v_.addLine(p1, p2, r, g, b, id, viewport_);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, line_width_, id);
  }

  void operator()(const Plane& plane) {
    const auto& id = makeId("plane");
    vtkSmartPointer<vtkPlaneSource> data_source = vtkSmartPointer<vtkPlaneSource>::New();
    // Construct coordinate frame associated with the plane
    //  * origin is defined by plane position
    //  * x-axis is defined by plane normal
    //  * y-axis and z-axis form an orthonormal basis with x-axis
    Transform world_T_plane = Transform::Identity();
    world_T_plane.translation() = plane.position;
    world_T_plane.linear().col(0) = plane.normal.normalized();
    // Choose any vector not parallel to normal for temporary reference
    Eigen::Vector3f temp = (plane.normal.dot(Eigen::Vector3f::UnitX()) > 0.9f)
                               ? Eigen::Vector3f::UnitY()
                               : Eigen::Vector3f::UnitX();
    world_T_plane.linear().col(1) = temp.cross(plane.normal).normalized();
    world_T_plane.linear().col(2) = plane.normal.cross(world_T_plane.linear().col(1)).normalized();

    // VTK plane is defined by three corner points
    // Compute their coordinates in the world frame
    const auto e1 = plane.extents[0] / 2;
    const auto e2 = plane.extents[1] / 2;
    const Eigen::Vector3f origin = world_T_plane * Eigen::Vector3f(0.0, -e1, -e2);
    const Eigen::Vector3f point1 = world_T_plane * Eigen::Vector3f(0.0, e1, -e2);
    const Eigen::Vector3f point2 = world_T_plane * Eigen::Vector3f(0.0, -e1, e2);

    data_source->SetOrigin(origin[0], origin[1], origin[2]);
    data_source->SetPoint1(point1[0], point1[1], point1[2]);
    data_source->SetPoint2(point2[0], point2[1], point2[2]);
    data_source->Update();

    v_.addModelFromPolyData(data_source->GetOutput(), id, viewport_);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
                                   representation_, id);

    auto* actor = vtkLODActor::SafeDownCast(v_.getShapeActorMap()->at(id));
    float red, green, blue;
    std::tie(red, green, blue) = plane.color.getRGB();
    actor->GetProperty()->SetColor(red, green, blue);
  }

  void operator()(const Sphere& sphere) {
    pcl::PointXYZ p;
    p.getVector3fMap() = sphere.center;
    float r, g, b;
    std::tie(r, g, b) = sphere.color.getRGB();
    const auto& id = makeId("sphere");
    v_.addSphere(p, sphere.radius, r, g, b, id, viewport_);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, shading_, id);
    v_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
                                   representation_, id);
  }

  void operator()(const Text& text) {
    pcl::PointXYZ p;
    p.getVector3fMap() = text.position;
    float r, g, b;
    std::tie(r, g, b) = text.color.getRGB();
    const auto& id = makeId("text_3d");
    v_.addText3D(text.text, p, text.scale, r, g, b, id, viewport_);
  }
};

struct TransformPrimitiveObjectVisitor : public boost::static_visitor<> {
  boost::optional<Transform> transform_;

  TransformPrimitiveObjectVisitor(const boost::optional<Transform>& transform)
  : transform_(transform) {}

  template <typename T>
  void operator()(T& object) {
    if (transform_)
      object = transform(object, *transform_);
  }
};

/// Primitive objects widget
/// \see PrimitiveObjectsWidgetProperties
class PrimitiveObjectsWidget : public DataVisualizationWidget<PrimitiveObjects> {
 public:
  /// Shared pointer to a point cloud widget.
  using Ptr = std::shared_ptr<PrimitiveObjectsWidget>;

  using Properties = PrimitiveObjectsWidgetProperties;

  using DataVisualizationWidget::DataVisualizationWidget;

  ~PrimitiveObjectsWidget() {
    TRACE("({}) {{destructor}}", name_);
    VisualizationWidget::removeFromVisualizer();
  }

  void setFlatShading(bool flat_shading) {
    DEBUG("({}) {{setFlatShading}} flat shanding: {}", name_, flat_shading);
    flat_shading_ = flat_shading;
    // Always synchronize, even if the stored shading type was the same, because the user may have
    // altered it using keyboard shortcuts.
    synchronizeVisualization();
  }

  void setWireframe(bool wireframe) {
    DEBUG("({}) {{setWireframe}} wireframe: {}", name_, wireframe ? "yes" : "no");
    wireframe_ = wireframe;
    // Always synchronize, even if the stored rerpresentation property was the same, because the
    // user may have altered it using keyboard shortcuts.
    synchronizeVisualization();
  }

  void setLineWidth(float line_width) {
    DEBUG("({}) {{setLineWidth}} line width: {}", name_, line_width);
    if (line_width_ != line_width) {
      line_width_ = line_width;
      synchronizeVisualization();
    }
  }

 protected:
  void addToVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{addToVisualizer}} ", name_);
    AddPrimitiveObjectVisitor add(v, name_, viewport_, flat_shading_, wireframe_, line_width_,
                                  objects_in_visualizer_);
    TransformPrimitiveObjectVisitor transform(transform_);
    for (const auto& object : data_) {
      auto obj = object;
      boost::apply_visitor(transform, obj);
      boost::apply_visitor(add, obj);
    }
  }

  void removeFromVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{removeFromVisualizer}} ", name_);
    for (const auto& object : objects_in_visualizer_)
      if (boost::algorithm::ends_with(object, "axes"))
        v.removeCoordinateSystem(object);
      else
        v.removeShape(object);
  }

 private:
  bool flat_shading_ = false;
  bool wireframe_ = false;
  float line_width_ = 1.0f;
  std::vector<std::string> objects_in_visualizer_;
};

}  // namespace tviewer
