/******************************************************************************
 * Copyright (c) 2014, 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <boost/algorithm/string/join.hpp>

#include <tviewer/fwd.h>

#include "widgets/visualization_widget.hpp"

namespace tviewer {

template <typename T>
class DataVisualizationWidget : public VisualizationWidget {
 public:
  DataVisualizationWidget(const std::string& name,
                          const std::function<void()>& state_change_callback)
  : VisualizationWidget(name, state_change_callback) {}

  void handleStateChangeEvent(const StateChangeEvent& event) override {
    if (event.changed(dependencies_)) {
      DEBUG("({}) {{handleStateChangeEvent}} fetching data", name_);
      if (get_callback_)
        data_ = get_callback_();
      else if (get_kwargs_callback_)
        data_ = get_kwargs_callback_(event.state());
      synchronizeVisualization();
    } else {
      TRACE("({}) {{handleStateChangeEvent}} irrelevant", name_);
    }
  }

  void setDependencies(const std::set<std::string>& dependencies) {
    if (!dependencies.empty()) {
      auto joined = boost::algorithm::join(dependencies, ", ");
      DEBUG("({}) {{setDependencies}} dependencies: {}", name_, joined);
      dependencies_ = dependencies;
    } else {
      WARN("({}) {{setDependencies}} no dependencies specified", name_);
    }
  }

  /// Set data to be visualized.
  void setData(const T& data) {
    DEBUG("({}) {{setData}} data", name_);
    data_ = data;
    get_callback_ = 0;
    get_kwargs_callback_ = 0;
    synchronizeVisualization();
  }

  void setData(const GetDataCallback<T>& callback) {
    DEBUG("({}) {{setData}} callback", name_);
    get_callback_ = callback;
    get_kwargs_callback_ = 0;
  }

  void setData(const GetDataKwargsCallback<T>& callback) {
    DEBUG("({}) {{setData}} kwargs callback", name_);
    get_callback_ = 0;
    get_kwargs_callback_ = callback;
  }

 protected:
  T data_;  // make private + accessor?
  std::set<std::string> dependencies_;
  GetDataCallback<T> get_callback_;
  GetDataKwargsCallback<T> get_kwargs_callback_;
};

}  // namespace tviewer
