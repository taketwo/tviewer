/******************************************************************************
 * Copyright (c) 2014, 2018, 2024 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <tviewer/widgets/visualization_widget_properties.h>

namespace tviewer {

template <typename T>
T& VisualizationWidgetProperties<T>::key(const std::string& key) {
  auto self = static_cast<T*>(this);
  if (auto p = self->obj.lock())
    p->setKey(key);
  return *self;
}

template <typename T>
T& VisualizationWidgetProperties<T>::viewport(unsigned int viewport) {
  auto self = static_cast<T*>(this);
  if (auto p = self->obj.lock())
    p->setViewport(viewport);
  return *self;
}

template <typename T>
bool VisualizationWidgetProperties<T>::visibility() const {
  auto self = static_cast<const T*>(this);
  if (auto p = self->obj.lock())
    return p->getVisibility();
  return false;
}

template <typename T>
T& VisualizationWidgetProperties<T>::visibility(bool visibility) {
  auto self = static_cast<T*>(this);
  if (auto p = self->obj.lock())
    p->setVisibility(visibility);
  return *self;
}

template <typename T>
T& VisualizationWidgetProperties<T>::transform(const Transform& transform) {
  auto self = static_cast<T*>(this);
  if (auto p = self->obj.lock())
    p->setTransform(transform);
  return *self;
}

template <typename T>
T& VisualizationWidgetProperties<T>::transform(const Eigen::Isometry3d& transform) {
  auto self = static_cast<T*>(this);
  if (auto p = self->obj.lock())
    p->setTransform(transform.cast<float>());
  return *self;
}

}  // namespace tviewer
