/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include "widgets/widget.hpp"

namespace tviewer {

// Base for widgets that fire a callback when their own state changes
template <typename ValueType>
class CallbackWidget : public Widget {
 public:
  CallbackWidget(const std::string& name) : Widget(name) {}

  void handleStateChangeEvent(const StateChangeEvent& event) override {
    if (event.changed(name_)) {
      if (callback_) {
        DEBUG("({}) {{handleStateChangeEvent}} invoking callback", name_);
        callback_();
      } else if (value_callback_) {
        DEBUG("({}) {{handleStateChangeEvent}} invoking value callback", name_);
        value_callback_(event.state().get<ValueType>(name_));
      } else if (kwargs_callback_) {
        DEBUG("({}) {{handleStateChangeEvent}} invoking kwargs callback", name_);
        kwargs_callback_(event.state());
      }
    } else {
      TRACE("({}) {{handleStateChangeEvent}} irrelevant", name_);
    }
  }

  void setCallback(const Callback& callback) {
    DEBUG("({}) {{setCallback}} callback", name_);
    callback_ = callback;
    value_callback_ = 0;
    kwargs_callback_ = 0;
  }

  void setCallback(const ValueCallback<ValueType>& callback) {
    DEBUG("({}) {{setCallback}} value callback", name_);
    value_callback_ = callback;
    callback_ = 0;
    kwargs_callback_ = 0;
  }

  void setCallback(const KwargsCallback& callback) {
    DEBUG("({}) {{setCallback}} kwargs callback", name_);
    kwargs_callback_ = callback;
    callback_ = 0;
    value_callback_ = 0;
  }

 protected:
  Callback callback_;
  ValueCallback<ValueType> value_callback_;
  KwargsCallback kwargs_callback_;
};

}  // namespace tviewer
