/******************************************************************************
 * Copyright (c) 2014, 2018, 2024 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <boost/optional.hpp>

#include "property.hpp"
#include "transform.h"
#include "widgets/widget.hpp"

namespace tviewer {

class VisualizationWidget : public Widget {
 public:
  VisualizationWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : Widget(name), visibility_(true, state_change_callback), viewport_(0) {}

  // by default state is visibility, but can be changed further down
  WidgetState getState() const override {
    DEBUG("({}) {{getState}} visilibity: {}", name_, visibility_);
    return visibility_;
  };

  void injectVisualizer(std::shared_ptr<pcl::visualization::PCLVisualizer> v) override {
    TRACE("({}) {{injectVisualizer}} storing a weak pointer", name_);
    visualizer_ = v;
    synchronizeVisualization();
  }

  void ejectVisualizer() override {
    if (auto v = visualizer_.lock()) {
      TRACE("({}) {{ejectVisualizer}} have valid visualizer, removing self from it", name_);
      removeFromVisualizer(*v);
    } else {
      TRACE("({}) {{ejectVisualizer}} have no valid visualizer", name_);
    }
    visualizer_.reset();
  }

  void setKey(const std::string& key) {
    DEBUG("({}) {{setKey}} visibility toggle key: {}", name_, key);
    clearKeyCallbacks();
    registerKeyCallback(key, &VisualizationWidget::toggle, this);
  }

  bool getVisibility() const {
    return visibility_;
  }

  void setVisibility(bool visibility) {
    visibility_ = visibility;
    synchronizeVisualization();
  }

  void toggle() {
    setVisibility(!visibility_);
  }

  /// Set viewport in to which the widget should be added.
  /// Zero means add to all viewports.
  void setViewport(unsigned int viewport) {
    DEBUG("({}) {{setViewport}} viewport: {}", name_, viewport);
    if (viewport_ != viewport) {
      viewport_ = viewport;
      synchronizeVisualization();
    }
  }

  void setTransform(const Transform& transform) {
    DEBUG("({}) {{setTransform}} transform: {}", name_, transform);
    if (!transform_ || *transform_ != transform) {
      transform_ = transform;
      synchronizeVisualization();
    }
  }

 protected:
  void synchronizeVisualization() {
    auto v = visualizer_.lock();
    TRACE("({}) {{synchronizeVisualization}} have valid visualizer: {}", name_,
          static_cast<bool>(v));
    if (v) {
      removeFromVisualizer(*v);
      if (visibility_)
        addToVisualizer(*v);
    }
  }

  void removeFromVisualizer() {
    if (auto v = visualizer_.lock())
      removeFromVisualizer(*v);
  }

  virtual void addToVisualizer(pcl::visualization::PCLVisualizer& v) = 0;
  virtual void removeFromVisualizer(pcl::visualization::PCLVisualizer& v) = 0;

  Property<bool> visibility_;
  unsigned int viewport_;
  boost::optional<Transform> transform_;

 private:
  std::weak_ptr<pcl::visualization::PCLVisualizer> visualizer_;
};

}  // namespace tviewer
