/******************************************************************************
 * Copyright (c) 2014, 2018, 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <tviewer/widgets/point_cloud_widget_properties.h>

#include "widgets/data_visualization_widget_properties.hpp"
#include "widgets/point_cloud_widget.hpp"
#include "widgets/visualization_widget_properties.hpp"
#include "widgets/widget_properties.hpp"

namespace tviewer {

#define PROPERTIES_WIDGET PointCloudWidget
#define PROPERTIES_CLASS PointCloudWidgetProperties

#include "properties_def.hpp"

PROPERTIES_CTOR
PROPERTIES_SETTER(unsigned int, pointSize, PointSize)
PROPERTIES_SETTER(Colors, colors, Colors)
PROPERTIES_SETTER(const std::string&, lut, Lut)
PROPERTIES_SETTER_2(double, lutRange, LutRange, min, max)

#include "properties_undef.hpp"

template class WidgetProperties<PointCloudWidgetProperties>;
template class VisualizationWidgetProperties<PointCloudWidgetProperties>;
template class DataVisualizationWidgetProperties<PointCloudWidgetProperties, PointCloudConstPtr>;

}  // namespace tviewer
