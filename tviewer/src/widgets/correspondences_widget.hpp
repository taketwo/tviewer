/******************************************************************************
 * Copyright (c) 2020 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <tviewer/fwd.h>
#include <tviewer/widgets/correspondences_widget_properties.h>

#include "widgets/data_visualization_widget.hpp"

namespace tviewer {

// A visitor for the NormalCloudConstPtr variant.
// Adds the contained normal cloud to the given visualizer. Nullptr values are ignored.
struct GetPointCloudXYZVisitor : public boost::static_visitor<> {
  pcl::PointCloud<pcl::PointXYZ>::Ptr& pointcloud_;

  GetPointCloudXYZVisitor(pcl::PointCloud<pcl::PointXYZ>::Ptr& pointcloud)
  : pointcloud_(pointcloud) {}

  template <typename T>
  void operator()(const T& cloud) {
    pointcloud_.reset(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::copyPointCloud(*cloud, *pointcloud_);
  }

  void operator()(const std::nullptr_t& /* cloud */) {
    pointcloud_.reset();
  }
};

/// Correspondences widget
/// \see CorrespondencesWidgetProperties
class CorrespondencesWidget : public DataVisualizationWidget<Correspondences> {
 public:
  /// Shared pointer to a correspondences widget.
  using Ptr = std::shared_ptr<CorrespondencesWidget>;

  using Properties = CorrespondencesWidgetProperties;

  using DataVisualizationWidget::DataVisualizationWidget;

  ~CorrespondencesWidget() {
    TRACE("({}) {{destructor}}", name_);
    VisualizationWidget::removeFromVisualizer();
  }

 protected:
  void addToVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{addToVisualizer}} ", name_);
    if (!data_.correspondences)
      return;
    pcl::PointCloud<pcl::PointXYZ>::Ptr source_cloud, target_cloud;
    GetPointCloudXYZVisitor get_source_cloud(source_cloud), get_target_cloud(target_cloud);
    boost::apply_visitor(get_source_cloud, data_.source);
    boost::apply_visitor(get_target_cloud, data_.target);
    v.addCorrespondences<pcl::PointXYZ>(source_cloud, target_cloud, *data_.correspondences, name_,
                                        viewport_);
  }

  void removeFromVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{removeFromVisualizer}} ", name_);
    if (v.contains(name_))
      v.removeCorrespondences(name_);
  }
};

}  // namespace tviewer
