/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <tviewer/widgets/spinner_widget_properties.h>

#include "property.hpp"
#include "widgets/callback_widget.hpp"

#include <boost/format.hpp>

namespace tviewer {

/// Spinner widget
/// \see SpinnerWidgetProperties
class SpinnerWidget : public CallbackWidget<double> {
 public:
  /// Shared pointer to a spinner widget.
  using Ptr = std::shared_ptr<SpinnerWidget>;

  using Properties = SpinnerWidgetProperties;

  SpinnerWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : CallbackWidget(name), value_(0, state_change_callback) {}

  WidgetState virtual getState() const override {
    DEBUG("({}) {{getState}} value: {}", name_, value_);
    return value_;
  };

  void getHelpInfo(std::string& icon, std::string& description, std::vector<Key>& keys,
                   std::string& extra) override {
    Widget::getHelpInfo(icon, description, keys, extra);
    icon = "  ↑↓ ";
    boost::format fmt;
    auto is_integer = [](auto v) { return v == std::floor(v); };
    if (is_integer(value_) && is_integer(min_) && is_integer(max_) && is_integer(step_))
      fmt = boost::format("Value: %.0f, Range: [%.0f, %.0f], Step: %.0f");
    else
      fmt = boost::format("Value: %.4f, Range: [%.4f, %.4f], Step: %.4f");
    extra = boost::str(fmt % value_ % min_ % max_ % step_);
  }

  void setKey(const Key& key_increment) {
    auto key_decrement = key_increment;
    key_decrement.toggleCtrl();
    setKey(key_increment, key_decrement);
  }

  void setKey(const Key& key_increment, const Key& key_decrement) {
    DEBUG("({}) {{setKey}} increment/decrement keys: {}/{}", name_, key_increment, key_decrement);
    clearKeyCallbacks();
    registerKeyCallback(key_increment, &SpinnerWidget::increment, this);
    registerKeyCallback(key_decrement, &SpinnerWidget::decrement, this);
  }

  void setValue(double value) {
    double coerced_value = value;
    if (value > max_) {
      coerced_value = wrap_around_ ? min_ : max_;
    } else if (value < min_) {
      coerced_value = wrap_around_ ? max_ : min_;
    } else if (std::isnan(min_) || std::isnan(max_)) {
      coerced_value = std::numeric_limits<double>::quiet_NaN();
    } else if (!std::isnan(min_) && !std::isnan(max_) && std::isnan(value)) {
      coerced_value = min_;
    }
    DEBUG("({}) {{setValue}} value: {}, after range coercion: {}", name_, value, coerced_value);
    value_ = coerced_value;
  }

  void setStep(double step) {
    DEBUG("({}) {{setStep}} step: {}", name_, step);
    step_ = step;
  }

  void setRange(double min, double max) {
    min_ = std::min(min, max);
    max_ = std::max(min, max);
    DEBUG("({}) {{setRange}} range: [{}, {}]", name_, min_, max_);
    setValue(value_);
  }

  void setRange(size_t length) {
    DEBUG("({}) {{setRange}} for container of length: {}", name_, length);
    if (length) {
      setRange(0, length - 1);
    } else {
      setRange(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
    }
  }

  void setWrapAround(bool wrap_around) {
    DEBUG("({}) {{setWrapArounnd}} wrap around: {}", name_, wrap_around);
    wrap_around_ = wrap_around;
  }

  void increment() {
    setValue(value_ + step_);
  }

  void decrement() {
    setValue(value_ - step_);
  }

 private:
  Property<double> value_;
  double step_ = 1;
  bool wrap_around_ = false;
  double min_ = -std::numeric_limits<float>::infinity();
  double max_ = std::numeric_limits<float>::infinity();
};

}  // namespace tviewer
