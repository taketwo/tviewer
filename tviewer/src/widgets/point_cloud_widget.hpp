/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <boost/optional.hpp>

#include <pcl/common/transforms.h>

#include <tviewer/color.h>
#include <tviewer/fwd.h>
#include <tviewer/widgets/point_cloud_widget_properties.h>

#include "color_handlers.hpp"
#include "widgets/data_visualization_widget.hpp"

namespace tviewer {

class LutRange {
 public:
  LutRange(double min, double max) : min_(std::min(min, max)), max_(std::max(min, max)) {}

  double min() const {
    return min_;
  }

  double max() const {
    return max_;
  }

 private:
  double min_;
  double max_;
};

inline bool operator==(const LutRange& lhs, const LutRange& rhs) {
  const auto are_equal = [](double lhs, double rhs) {
    return (std::isnan(lhs) && std::isnan(rhs)) ||
           std::abs(lhs - rhs) < std::numeric_limits<double>::epsilon();
  };
  return are_equal(lhs.min(), rhs.min()) && are_equal(lhs.max(), rhs.max());
}

inline bool operator!=(const LutRange& lhs, const LutRange& rhs) {
  return !(lhs == rhs);
}

// A visitor for the PointCloudConstPtr variant.
// Adds the contained cloud to the given visualizer. Nullptr values are ignored.
struct AddPointCloudVisitor : public boost::static_visitor<> {
  pcl::visualization::PCLVisualizer& v_;
  const std::string& name_;
  unsigned int viewport_;
  WidgetColors colors_;
  int point_size_;
  float opacity_;
  pcl::visualization::LookUpTableRepresentationProperties lut_;
  boost::optional<LutRange> lut_range_;
  boost::optional<Transform> transform_;

  AddPointCloudVisitor(pcl::visualization::PCLVisualizer& v, const std::string& name,
                       unsigned int viewport, const WidgetColors& colors, int point_size,
                       float opacity, pcl::visualization::LookUpTableRepresentationProperties lut,
                       boost::optional<LutRange> lut_range,
                       const boost::optional<Transform>& transform)
  : v_(v), name_(name), viewport_(viewport), colors_(colors), point_size_(point_size),
    opacity_(opacity), lut_(lut), lut_range_(lut_range), transform_(transform) {}

  template <typename T>
  void operator()(const T& cloud) {
    if (!cloud)
      return;

    using PointType = typename T::element_type::PointType;
    T maybe_transformed_cloud;
    if (transform_) {
      typename pcl::PointCloud<PointType>::Ptr cloud_transformed(new pcl::PointCloud<PointType>);
      pcl::transformPointCloud<PointType>(*cloud, *cloud_transformed, transform_->matrix());
      maybe_transformed_cloud = cloud_transformed;
    } else {
      maybe_transformed_cloud = cloud;
    }
    v_.addPointCloud<PointType>(maybe_transformed_cloud,
                                *createHandler<PointType>(maybe_transformed_cloud, name_, colors_),
                                name_, viewport_);
    v_.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size_,
                                        name_);
    v_.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, opacity_,
                                        name_);
    v_.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LUT, lut_, name_,
                                        viewport_);
    if (lut_range_)
      v_.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LUT_RANGE,
                                          lut_range_->min(), lut_range_->max(), name_, viewport_);
    else
      v_.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LUT_RANGE,
                                          pcl::visualization::PCL_VISUALIZER_LUT_RANGE_AUTO, name_,
                                          viewport_);
  }

  void operator()(const std::nullptr_t& /* cloud */) {}
};

struct GetStringVisitor : public boost::static_visitor<std::string> {
  std::string operator()(const std::string& colors) const {
    return colors;
  }
  std::string operator()(const Color& color) const {
    std::stringstream ss;
    ss << color;
    return ss.str();
  }
};

/// Point cloud widget
/// \see PointCloudWidgetProperties
class PointCloudWidget : public DataVisualizationWidget<PointCloudConstPtr> {
 public:
  /// Shared pointer to a point cloud widget.
  using Ptr = std::shared_ptr<PointCloudWidget>;

  using Properties = PointCloudWidgetProperties;

  PointCloudWidget(const std::string& name, const std::function<void()>& state_change_callback)
  : DataVisualizationWidget(name, state_change_callback) {}

  ~PointCloudWidget() {
    TRACE("({}) {{destructor}}", name_);
    VisualizationWidget::removeFromVisualizer();
  }

  void setPointSize(unsigned int point_size) {
    DEBUG("({}) {{setPointSize}} point size: {}", name_, point_size);
    point_size_ = point_size;
    // Always synchronize, even if the stored point size was the same, because the user may have
    // altered point size using keyboard shortcuts +/- directly.
    // During synchronization the widget will be removed from the visualizer. At this point the
    // current state of the point size is queried and may overwrite the point size commanded by
    // the user. Therefore, we temporary lock the property to avoid undesired changes.
    PropertyLock lock(point_size_);
    synchronizeVisualization();
  }

  void setColors(WidgetColors colors) {
    DEBUG("({}) {{setColors}} colors: {}", name_, boost::apply_visitor(GetStringVisitor(), colors));
    if (colors_ != colors) {
      colors_ = colors;
      synchronizeVisualization();
    }
  }

  void setLut(const std::string& lut) {
    if (lut == "gray")
      lut_ = pcl::visualization::PCL_VISUALIZER_LUT_GREY;
    else if (lut == "jet")
      lut_ = pcl::visualization::PCL_VISUALIZER_LUT_JET;
#if PCL_VERSION_COMPARE(>, 1, 8, 1)
    else if (lut == "viridis")
      lut_ = pcl::visualization::PCL_VISUALIZER_LUT_VIRIDIS;
#endif
    else {
      WARN("({}) {{setLut}} invalid LUT value: {}, ignoring", name_, lut);
      return;
    }
    DEBUG("({}) {{setLut}} {}", name_, lut);
  }

  void setLutRange(double min, double max) {
    LutRange lut_range(min, max);
    DEBUG("({}) {{setLutRange}} range: [{}, {}]", name_, lut_range.min(), lut_range.max());
    if (lut_range_ != lut_range) {
      lut_range_ = lut_range;
      synchronizeVisualization();
    }
  }

 protected:
  void addToVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{addToVisualizer}} ", name_);
    AddPointCloudVisitor add(v, name_, viewport_, colors_, point_size_, 1.0, lut_, lut_range_,
                             transform_);
    boost::apply_visitor(add, data_);
  }

  void removeFromVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{removeFromVisualizer}} ", name_);
    if (v.contains(name_)) {
      double point_size;
      v.getPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size,
                                         name_);
      TRACE("({}) {{removeFromVisualizer}} queried actual point size: {}", name_, point_size);
      point_size_ = point_size;
      v.removePointCloud(name_);
    }
  }

 private:
  Property<unsigned int> point_size_ = 1;
  WidgetColors colors_ = "";
  pcl::visualization::LookUpTableRepresentationProperties lut_ =
#if PCL_VERSION_COMPARE(>, 1, 8, 1)
      pcl::visualization::PCL_VISUALIZER_LUT_VIRIDIS;
#else
      pcl::visualization::PCL_VISUALIZER_LUT_JET;
#endif
  boost::optional<LutRange> lut_range_;
};

}  // namespace tviewer
