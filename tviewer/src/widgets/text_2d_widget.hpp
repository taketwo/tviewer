/******************************************************************************
 * Copyright (c) 2019 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <tviewer/color.h>
#include <tviewer/fwd.h>
#include <tviewer/widgets/text_2d_widget_properties.h>

#include "widgets/data_visualization_widget.hpp"

namespace tviewer {

/// 2D text widget
/// \see Text2DWidgetProperties
class Text2DWidget : public DataVisualizationWidget<std::string> {
 public:
  /// Shared pointer to a 2D text widget.
  using Ptr = std::shared_ptr<Text2DWidget>;

  using Properties = Text2DWidgetProperties;

  using DataVisualizationWidget::DataVisualizationWidget;

  ~Text2DWidget() {
    TRACE("({}) {{destructor}}", name_);
    VisualizationWidget::removeFromVisualizer();
  }

  void setPosition(std::pair<unsigned int, unsigned int> position) {
    DEBUG("({}) {{setPosition}} position: ({}, {})", name_, position.first, position.second);
    if (position_ != position) {
      position_ = position;
      synchronizeVisualization();
    }
  }

  void setColor(Color color) {
    DEBUG("({}) {{setColor}} color: {}", name_, color);
    if (color_ != color) {
      color_ = color;
      synchronizeVisualization();
    }
  }

  void setFontSize(unsigned int font_size) {
    DEBUG("({}) {{setFontSize}} font size: {}", name_, font_size);
    if (font_size_ != font_size) {
      font_size_ = font_size;
      synchronizeVisualization();
    }
  }

 protected:
  void addToVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{addToVisualizer}} ", name_);
    float r, g, b;
    std::tie(r, g, b) = color_.getRGB();
    v.addText(data_, position_.first, position_.second, font_size_, r, g, b, name_, viewport_);
  }

  void removeFromVisualizer(pcl::visualization::PCLVisualizer& v) override {
    TRACE("({}) {{removeFromVisualizer}} ", name_);
    if (v.contains(name_))
      v.removeShape(name_);
  }

 private:
  std::pair<unsigned int, unsigned int> position_ = {14, 14};
  Color color_ = Color::White;
  unsigned int font_size_ = 10;
};

}  // namespace tviewer
