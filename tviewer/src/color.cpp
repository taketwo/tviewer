/******************************************************************************
 * Copyright (c) 2014 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <algorithm>
#include <cmath>
#include <random>
#include <stdexcept>
#include <string>

#include <tviewer/color.h>

namespace tviewer {

namespace {

template <typename T>
const T& clamp(const T& value, const T& low, const T& high) {
  return (value < low ? low : (value > high ? high : value));
}

template <typename T>
auto normalize(T v) -> std::enable_if_t<std::is_integral<T>::value, uint32_t> {
  return clamp(v, static_cast<T>(0), static_cast<T>(255));
}

template <typename T>
auto normalize(T v) -> std::enable_if_t<std::is_floating_point<T>::value, uint32_t> {
  return std::round(clamp(v, static_cast<T>(0), static_cast<T>(1)) * 255);
}

}  // anonymous namespace

uint32_t COLORS[] = {
    0x000000,  // Black
    0x0000FF,  // Blue
    0x993300,  // Brown
    0x9FA91F,  // Citron
    0xFBEC5D,  // Corn
    0xDC143C,  // Crimson
    0x00FFFF,  // Cyan
    0xA9A9A9,  // DarkGray
    0x1560BD,  // Denim
    0x50C878,  // Emerald
    0x808080,  // Gray
    0x00FF00,  // Green
    0x4B0082,  // Indigo
    0xD3D3D3,  // LightGray
    0xFF00FF,  // Magenta
    0xFDBE02,  // Mango
    0x47ABCC,  // MaximumBlue
    0x5E8C31,  // MaximumGreen
    0xD92121,  // MaximumRed
    0xFF6600,  // Orange
    0xD1E231,  // Pear
    0x6A0DAD,  // Purple
    0xFF0000,  // Red
    0xFF007F,  // Rose
    0xE0115F,  // Ruby
    0x008080,  // Teal
    0xFFFFFF,  // White
    0xFFFF00,  // Yellow
};

template <typename T>
Color::Color(T r, T g, T b) {
  rgba_ = 0xFF << 24 | normalize(r) << 16 | normalize(g) << 8 | normalize(b);
}

template Color::Color(int16_t r, int16_t g, int16_t b);
template Color::Color(uint16_t r, uint16_t g, uint16_t b);
template Color::Color(int32_t r, int32_t g, int32_t b);
template Color::Color(uint32_t r, uint32_t g, uint32_t b);
template Color::Color(int64_t r, int64_t g, int64_t b);
template Color::Color(uint64_t r, uint64_t g, uint64_t b);
template Color::Color(double r, double g, double b);

Color::Color(uint8_t r, uint8_t g, uint8_t b) {
  rgba_ = 0xFF << 24 | normalize(r) << 16 | normalize(g) << 8 | normalize(b);
}

Color::Color(float r, float g, float b) {
  rgba_ = 0xFF << 24 | normalize(r) << 16 | normalize(g) << 8 | normalize(b);
}

Color::Color(uint32_t rgb) : rgba_(0xFF000000 | rgb) {}

Color::Color(ColorName color_name) : rgba_(0xFF000000 | COLORS[color_name]) {}

Color::Color(const std::string& color) {
  try {
    rgba_ = 0xFF << 24 | std::stoul(color, nullptr, 0);
  } catch (std::invalid_argument& e) {
    rgba_ = 0;
  }
}

std::tuple<float, float, float> Color::getRGB() const {
  float r = static_cast<float>(((rgba_ >> 16) & 0xFF)) / 255.0;
  float g = static_cast<float>(((rgba_ >> 8) & 0xFF)) / 255.0;
  float b = static_cast<float>(((rgba_ >> 0) & 0xFF)) / 255.0;
  return std::make_tuple(r, g, b);
}

void Color::getRGB(float* rgb) const {
  rgb[0] = static_cast<float>(((rgba_ >> 16) & 0xFF)) / 255.0;
  rgb[1] = static_cast<float>(((rgba_ >> 8) & 0xFF)) / 255.0;
  rgb[2] = static_cast<float>(((rgba_ >> 0) & 0xFF)) / 255.0;
}

void Color::getRGB(uint8_t* rgb) const {
  rgb[0] = (rgba_ >> 16) & 0xFF;
  rgb[1] = (rgba_ >> 8) & 0xFF;
  rgb[2] = (rgba_ >> 0) & 0xFF;
}

void Color::getRGB(uint8_t& r, uint8_t& g, uint8_t& b) const {
  r = (rgba_ >> 16) & 0xFF;
  g = (rgba_ >> 8) & 0xFF;
  b = (rgba_ >> 0) & 0xFF;
}

Color Color::random() {
  static std::random_device rnd;
  static std::mt19937 gen(rnd());
  static std::uniform_int_distribution<> dist(0, 255);
  auto r = static_cast<uint8_t>((dist(gen) % 256));
  auto g = static_cast<uint8_t>((dist(gen) % 256));
  auto b = static_cast<uint8_t>((dist(gen) % 256));
  return {r, g, b};
}

Color getColor(float value, ColorMap colormap) {
  float v = clamp(value, 0.0f, 1.0f);
  switch (colormap) {
    case ColorMap::JET: {
      float four_value = v * 4;
      float r = std::min(four_value - 1.5, -four_value + 4.5);
      float g = std::min(four_value - 0.5, -four_value + 3.5);
      float b = std::min(four_value + 0.5, -four_value + 2.5);
      return {r, g, b};
    }
    case ColorMap::SUMMER: {
      return {v, (1.0f + v) * 0.5f, 0.4f};
    }
    case ColorMap::AUTUMN: {
      return {1.0f, v, 0.0f};
    }
    case ColorMap::COOL: {
      return {v, 1.0f - v, 1.0f};
    }
    case ColorMap::VIRIDIS: {
      using LUT = pcl::ColorLUT<pcl::LUT_VIRIDIS>;
      auto color_id = static_cast<size_t>(std::round(v * (LUT::size() - 1)));
      return Color(LUT::at(color_id).rgba);
    }
  }
  return Color::random();
}

bool Color::operator==(const tviewer::Color& rhs) const {
  return rgba_ == rhs.rgba_;
}

bool Color::operator!=(const tviewer::Color& rhs) const {
  return !(*this == rhs);
}

std::ostream& operator<<(std::ostream& os, const Color& obj) {
  uint8_t r, g, b;
  obj.getRGB(r, g, b);
  os << "(" << static_cast<int>(r) << ", " << static_cast<int>(g) << ", " << static_cast<int>(b)
     << ")";
  return os;
}

}  // namespace tviewer
