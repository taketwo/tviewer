/******************************************************************************
 * Copyright (c) 2014, 2018, 2019, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <ostream>
#include <string>

#include <pcl/visualization/keyboard_event.h>

namespace tviewer {

class Key {
 public:
  Key(const pcl::visualization::KeyboardEvent& key_event);

  Key(const std::string& code);

  operator std::string() const;

  bool operator==(const Key& other) const;

  void toggleAlt();

  void toggleCtrl();

 private:
  bool alt_ = false;
  bool ctrl_ = false;
  std::string symbol_ = "";
};

std::ostream& operator<<(std::ostream& os, const Key& obj);

}  // namespace tviewer

#include "logging.hpp"
CREATE_CUSTOM_FORMATTER(tviewer::Key)

namespace std {

template <>
struct hash<tviewer::Key> {
  std::size_t operator()(const tviewer::Key& k) const {
    return hash<string>()(std::string{k});
  }
};

}  // namespace std
