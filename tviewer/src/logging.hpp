/******************************************************************************
 * Copyright (c) 2018, 2023 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#ifdef ENABLE_LOGGING

// Logging enabled

#include <sstream>
#include <string>

#include <spdlog/fmt/bundled/format.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#define TRACE(...) spdlog::get("console")->trace(__VA_ARGS__)
#define DEBUG(...) spdlog::get("console")->debug(__VA_ARGS__)
#define INFO(...) spdlog::get("console")->info(__VA_ARGS__)
#define WARN(...) spdlog::get("console")->warn(__VA_ARGS__)
#define ERROR(...) spdlog::get("console")->error(__VA_ARGS__)

class Logger {
 public:
  Logger() {
    if (!spdlog::get("console"))
      spdlog::stdout_color_mt("console");
    setLevel(3);
  }
  ~Logger() {
    spdlog::drop("console");
  }
  void setLevel(unsigned int level) const {
    spdlog::get("console")->set_level(static_cast<spdlog::level::level_enum>(level));
  }
};

#define CREATE_CUSTOM_FORMATTER(Type)                        \
  namespace fmt {                                            \
  template <>                                                \
  struct formatter<Type> : formatter<std::string> {          \
    template <typename FormatContext>                        \
    auto format(Type t, FormatContext& ctx) {                \
      std::ostringstream oss;                                \
      oss << t;                                              \
      return formatter<std::string>::format(oss.str(), ctx); \
    }                                                        \
  };                                                         \
  }

#else

// Logging disabled

#define TRACE(...) (void)0
#define DEBUG(...) (void)0
#define INFO(...) (void)0
#define WARN(...) (void)0
#define ERROR(...) (void)0

class Logger {
  void setLevel(unsigned int /* level */) const {}
};

#define CREATE_CUSTOM_FORMATTER(Type)

#endif
