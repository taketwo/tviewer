import _tviewer

from .widgets import (
    SpinnerWidget,
    TriggerWidget,
    PointCloudWidget,
    PrimitiveObjectsWidget,
    ToggleWidget,
    PointSelectorWidget,
    VariantWidget,
    Text2DWidget,
)
from .primitive_objects import Arrow, Axes, Box, LineSegment, Plane, Sphere
from .point_cloud import PointCloud
from _tviewer import _Color as Color


class TViewer(_tviewer.TViewer):
    def __init__(self):
        super().__init__()
        # TODO: should this wrapper keep track of widgets, or rely on C++ getters?
        self._widgets = dict()

    def add_point_cloud(
        self,
        name,
        data=None,
        *,
        colors=None,
        lut_range=None,
        key=None,
        point_size=1,
        dependencies=None,
        viewport=None,
    ):
        """
        Create and add a point cloud widget to the scene.

        Parameters
        ----------
        name : str
            Unique name for this widget.
        data : :class:`.PointCloud` | callable
            Either a point cloud to be displayed or a callback that can retrieve such
            a point cloud.
        colors : str | tuple
            Method used to colorize the point cloud. A string should specify the point
            cloud field used as a source of color information (empty string means
            automatic). A tuple of three floats specifies a single fixed color user for
            all points.

        Returns
        -------
        widget : :class:`.PointCloudWidget`
            Created widget instance.
        """
        widget = PointCloudWidget(super().add_point_cloud(name))
        widget.data = data
        if dependencies is not None:
            widget.dependencies = dependencies
        if key is not None:
            widget.key = key
        if point_size is not None:
            widget.point_size = point_size
        if colors is not None:
            widget.colors = colors
        if lut_range is not None:
            widget.lut_range = lut_range
        if viewport is not None:
            widget.viewport = viewport
        self._widgets[name] = widget
        return widget

    def add_primitive_objects(
        self,
        name,
        data=None,
        *,
        key=None,
        wireframe=None,
        dependencies=None,
        viewport=None,
    ):
        widget = PrimitiveObjectsWidget(super().add_primitive_objects(name))
        widget.data = data
        if key is not None:
            widget.key = key
        if wireframe is not None:
            widget.wireframe = wireframe
        if dependencies is not None:
            widget.dependencies = dependencies
        if viewport is not None:
            widget.viewport = viewport
        self._widgets[name] = widget
        return widget

    def add_spinner(
        self,
        name,
        *,
        key=None,
        keys=None,
        wrap_around=False,
        value=0,
        step=1,
        range=(-1000, 1000),
        callback=None,
    ):
        """
        Internally, double precision numbers are used to represent the value.

        Returns
        -------
        widget : :class:`.SpinnerWidget`
            Created widget instance.
        """
        #  description = description or name
        #  callback = callback or (lambda v: None)
        widget = SpinnerWidget(super().add_spinner(name))
        widget.wrap_around = wrap_around
        if key is not None:
            widget.key = key
        if keys is not None:
            widget.keys = keys
        widget.value = value
        widget.step = step
        widget.range = range
        if callback is not None:
            widget.callback = callback
        self._widgets[name] = widget
        return widget

    def add_trigger(self, name, *, key=None, keys=None, callback=None):
        """
        Internally, double precision numbers are used to represent the value.
        """
        #  description = description or name
        #  callback = callback or (lambda v: None)
        widget = TriggerWidget(super().add_trigger(name))
        if key is not None:
            widget.key = key
        if keys is not None:
            widget.keys = keys
        if callback is not None:
            widget.callback = callback
        self._widgets[name] = widget
        return widget

    def add_variant(self, name, *, variants=None, keys=None, callback=None):
        #  description = description or name
        #  callback = callback or (lambda v: None)
        widget = VariantWidget(super().add_variant(name))
        if keys is not None:
            widget.keys = keys
        if callback is not None:
            widget.callback = callback
        if variants is not None:
            widget.variants = variants
        self._widgets[name] = widget
        return widget

    def add_toggle(self, name, *, key=None, callback=None, value=False):
        #  description = description or name
        #  callback = callback or (lambda v: None)
        widget = ToggleWidget(super().add_toggle(name))
        widget.value = value
        if key is not None:
            widget.key = key
        if callback is not None:
            widget.callback = callback
        self._widgets[name] = widget
        return widget

    def add_point_selector(self, name, *, key=None, callback=None, num_points=None):
        widget = PointSelectorWidget(super().add_point_selector(name))
        if num_points is not None:
            widget.num_points = num_points
        if key is not None:
            widget.key = key
        if callback is not None:
            widget.callback = callback
        self._widgets[name] = widget
        return widget

    def add_text_2d(
        self,
        name,
        text=None,
        *,
        color=None,
        position=None,
        key=None,
        viewport=None,
    ):
        """
        Create and add a 2D text widget to the scene.

        Parameters
        ----------
        name : str
            Unique name for this widget.

        Returns
        -------
        widget : :class:`.Text2DWidget`
            Created widget instance.
        """
        widget = Text2DWidget(super().add_text_2d(name))
        widget.text = text
        if color is not None:
            widget.color = color
        if key is not None:
            widget.key = key
        if position is not None:
            widget.position = position
        if viewport is not None:
            widget.viewport = viewport
        self._widgets[name] = widget
        return widget

    def add_viewport(self, xmin, ymin, xmax, ymax, separate_camera=False):
        super().add_viewport(xmin, ymin, xmax, ymax, separate_camera)

    def run(self, keep_open=False):
        super().run(keep_open)

    def update(self, name):
        super().update(name)

    def wait_point_selected(self):
        result = super().wait_point_selected()
        if result[1]:
            return result[0]
        return None

    def __getitem__(self, key):
        if key in self._widgets:
            return self._widgets[key]
        raise KeyError(f'Widget with name "{key}" does not exist')

    def __contains__(self, key):
        return key in self._widgets
        return NotImplementedError

    def widgets(self):
        yield from self._widgets.values()
