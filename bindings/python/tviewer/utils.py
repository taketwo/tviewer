import inspect
import numpy as np
import numbers


def to_numpy(data):
    if isinstance(data, np.ndarray):
        arr = data
    elif type(data).__name__ == 'Tensor' and data.__module__ == 'torch':
        arr = data.cpu().detach().numpy()
    elif isinstance(data, (tuple, list, numbers.Integral, numbers.Real)):
        arr = np.array(data)
    else:
        raise TypeError('data is not a Numpy array and can not be converted to it')
    return np.atleast_1d(arr)


def is_kwargs_function(obj):
    """
    Check if object is a function that takes _only_ kwargs parameter packs.
    """
    params = __import__("inspect").signature(callback).parameters
    if len(params) == 0:
        self._widget.callback = callback
    elif len(params) == 1:
        for v in params.values():
            if v.kind != 4:
                raise TypeError('callback should have 0 or 1 kwargs parameter')

