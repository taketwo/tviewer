import numpy as np
from .utils import to_numpy
import _tviewer


class PointCloud(_tviewer._PointCloud):
    """
    Holds point cloud data that can be visualized with TViewer.

    As *tviewer* serves only visualization purposes, PointCloud is not meant to be used
    as a point cloud data storage beyond visualization.

    This encapsulates a ``pcl::PointCloud``, however does not provide an interface to
    access or modify the underlying data. Its purpose is only to be a container for
    visualization.

    Every non-empty point cloud has mandatory ``xyz`` data, i.e. 3D point coordinates.
    Optionally, it may have a data field (to be used to colorize the point cloud in the
    visualizer).

    Several types are supported:
      * 'intensity' -> single float per point, used with colormap.
      * 'rgb' -> rgb triplet (either uint8 in 0..255 range or float in 0..1 range)
      * 'label' -> single integer (signed/unsigned, 16, 32, 64 bit) per point, mapped
                   to a set of distinctive colors

    Unlike ``pcl::PointCloud``\s which are templated on the point type, PointCloud can
    contain any data fields. Moreover, they are determined automatically on construction
    time.

    PointCloud does not need to be constructed explicitly. Every function that accepts
    point clouds will also accept arguments that can be used to construct a PointCloud
    and will do this for you.
    """

    def __init__(self, xyz_data=None, color_data=None):
        if xyz_data is None:
            if color_data is not None:
                raise ValueError("color data without xyz data does not make sense")
            super().__init__()
            return
        xyz_data = to_numpy(xyz_data)
        if xyz_data.ndim > 2 or xyz_data.shape[-1] != 3:
            raise ValueError(
                f"xyz data has {xyz_data.shape} shape, but (n, 3) is expected"
            )
        if xyz_data.dtype.kind != "f":
            raise TypeError(
                f"xyz data has {xyz_data.dtype.kind} kind, but floating point is "
                f"expected"
            )
        if color_data is None:
            super().__init__(xyz_data)
            return
        num_points = 1 if xyz_data.ndim == 1 else xyz_data.shape[0]
        color_data = to_numpy(color_data)
        num_color_channels = 1 if color_data.ndim == 1 else color_data.shape[1]
        if color_data.shape[0] != num_points:
            raise ValueError(
                f"xyz and color data have {xyz_data.shape} and {color_data.shape} "
                f"shapes, but are expected to have the same first dimension"
            )
        if color_data.ndim > 2 or num_color_channels not in (1, 3):
            raise ValueError(
                f"color data has {color_data.shape} shape, but (n, 1) or (n, 3) is "
                f"expected"
            )
        if color_data.dtype.kind == "f":
            if num_color_channels == 3:
                rgba_data = np.empty((len(color_data), 4), dtype=np.uint8)
                rgba_data[:, 3] = 255
                rgba_data[:, :3] = color_data * 255
                color_data = rgba_data
        elif color_data.dtype.kind in ("i", "u"):
            if num_color_channels == 3:
                rgba_data = np.empty((len(color_data), 4), dtype=np.uint8)
                rgba_data[:, 3] = 255
                rgba_data[:, :3] = color_data.astype(np.uint8)
                color_data = rgba_data

        # uint8 should be n,3
        # float should be n,1 or n,3
        # int32 or uint32 should be n,1
        super().__init__(xyz_data, color_data)
