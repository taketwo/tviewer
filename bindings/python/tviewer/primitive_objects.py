import _tviewer


class Arrow(_tviewer._Arrow):
    def __init__(self, source, target, color=None):
        super().__init__()
        self.source = source
        self.target = target
        self.color = _tviewer._Color(color) if color is not None else _tviewer._Color()


class Axes(_tviewer._Axes):
    def __init__(self, position=None, orientation=None, size=None):
        super().__init__()
        if position is not None:
            self.position = position
        if orientation is not None:
            self.orientation = orientation
        if size is not None:
            self.size = size


class Box(_tviewer._Box):
    def __init__(self, center, dimensions):
        super().__init__()
        self.center = center
        self.dimensions = dimensions


class LineSegment(_tviewer._LineSegment):
    def __init__(self, start, end, color=None):
        super().__init__()
        self.start = start
        self.end = end
        self.color = _tviewer._Color(color) if color is not None else _tviewer._Color()


class Plane(_tviewer._Plane):
    def __init__(self, normal=None, position=None, color=None):
        super().__init__()
        if normal is not None:
            self.normal = normal
        if position is not None:
            self.position = position
        self.color = _tviewer._Color(color) if color is not None else _tviewer._Color()


class Sphere(_tviewer._Sphere):
    def __init__(self, center, radius, color=None):
        super().__init__()
        self.center = center
        self.radius = radius
        self.color = _tviewer._Color(color) if color is not None else _tviewer._Color()
