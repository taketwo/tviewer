from collections.abc import Iterable

import _tviewer
import setters
from .point_cloud import PointCloud
from .primitive_objects import Arrow, Axes, Box, LineSegment, Plane, Sphere
import numpy as np


def setter(func, doc=None):
    return property(None, func, doc=doc)


class SpinnerWidget(object):

    """
    Spinner widget.
    """

    key = setters.key()
    keys = setters.keys()
    callback = setters.callback()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._SpinnerWidget
        ), "you are not supposed to use this"
        self._widget = widget

    def __float__(self):
        return self.value

    @property
    def value(self):
        return self._widget.get_value()

    @value.setter
    def value(self, value):
        self._widget.set_value(value)

    @setter
    def wrap_around(self, wrap_around):
        self._widget.set_wrap_around(wrap_around)

    @setter
    def step(self, step):
        self._widget.set_step(step)

    @setter
    def range(self, range):
        if isinstance(range, (tuple, list)) and len(range) == 2:
            self._widget.set_range(range)
        elif isinstance(range, int):
            self._widget.set_range(range)
        elif hasattr(range, '__len__'):
            self._widget.set_range(len(range))
        else:
            raise ValueError("range should either be a tuple or list with two elements, an int, or an object with length")


class VariantWidget(object):

    """
    Variant widget.
    """

    keys = setters.keys()
    callback = setters.callback()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._VariantWidget
        ), "you are not supposed to use this"
        self._widget = widget

    def __str__(self):
        return self.value

    @property
    def value(self):
        return self._widget.get_value()

    @value.setter
    def value(self, value):
        if isinstance(value, int):
            self._widget.set_value_index(value)
        elif isinstance(value, str):
            self._widget.set_value(value)
        else:
            raise TypeError("value should be an index into variants list or a string")

    @setter
    def variants(self, variants):
        self._widget.set_variants(variants)


class TriggerWidget(object):

    key = setters.key()
    keys = setters.keys()
    callback = setters.callback()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._TriggerWidget
        ), "you are not supposed to use this"
        self._widget = widget


class ToggleWidget(object):

    key = setters.key()
    callback = setters.callback()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._ToggleWidget
        ), "you are not supposed to use this"
        self._widget = widget

    def __repr__(self):
        return f"ToggleWidget(value={self.value})"

    def __bool__(self):
        return self.value

    @property
    def value(self):
        return self._widget.get_value()

    @value.setter
    def value(self, value):
        self._widget.set_value(value)


class PointCloudWidget(object):

    key = setters.key()
    dependencies = setters.dependencies()
    viewport = setters.viewport()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._PointCloudWidget
        ), "you are not supposed to use this"
        self._widget = widget

    @setter
    def point_size(self, point_size):
        if not isinstance(point_size, int):
            raise TypeError("point size should be an integer")
        if point_size <= 0:
            raise ValueError("point size should be positive")
        self._widget.set_point_size(point_size)

    @setter
    def lut_range(self, range):
        self._widget.set_lut_range(range)

    @setter
    def colors(self, colors):
        if isinstance(colors, str):
            self._widget.set_colors(colors)
        elif isinstance(colors, _tviewer._Color.ColorName):
            self._widget.set_colors(_tviewer._Color(colors))
        else:
            c = np.array(colors)
            if len(c) == 3:
                if c.dtype.kind == "i":
                    self._widget.set_colors(c)
                if c.dtype.kind == "f":
                    self._widget.set_colors(c)
        #  if not isinstance(point_size, int):
        #  raise TypeError("point size should be an integer")
        #  if point_size <= 0:
        #  raise ValueError("point size should be positive")
        #  self._widget.point_size = point_size

    @setter
    def data(self, data):
        if callable(data):

            def return_to_point_cloud(**kwargs):
                d = data(**kwargs)
                if isinstance(d, PointCloud):
                    return d
                if isinstance(d, tuple):
                    return PointCloud(*d)
                return PointCloud(d)

            params = __import__("inspect").signature(data).parameters
            if len(params) == 0:
                self._widget.set_data_callback(return_to_point_cloud)
            elif len(params) == 1:
                for v in params.values():
                    if v.kind != 4:
                        raise TypeError("callback should have 0 or 1 kwargs parameter")
                self._widget.set_data_kwargs_callback(return_to_point_cloud)
        elif isinstance(data, PointCloud):
            self._widget.set_data(data)
        elif isinstance(data, tuple):
            # TODO: we probably want to reset colors to "" here or in set_data()
            self._widget.set_data(PointCloud(*data))
        else:
            self._widget.set_data(PointCloud(data))

    def show(self):
        self._widget.show()

    def hide(self):
        self._widget.hide()


class PrimitiveObjectsWidget(object):

    key = setters.key()
    dependencies = setters.dependencies()
    viewport = setters.viewport()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._PrimitiveObjectsWidget
        ), "you are not supposed to use this"
        self._widget = widget

    @setter
    def data(self, data):
        primitives = list()
        # TODO: more type checking, support non-lists
        if data is not None:
            if not isinstance(data, Iterable):
                data = [data]
            for d in data:
                if isinstance(d, (Arrow, Axes, Box, LineSegment, Plane, Sphere)):
                    primitives.append(_tviewer._PrimitiveObject(d))
                else:
                    raise TypeError("not a primitive object")
        self._widget.set_data(primitives)

    @setter
    def wireframe(self, wireframe):
        self._widget.set_wireframe(wireframe)

    def show(self):
        self._widget.show()

    def hide(self):
        self._widget.hide()


class PointSelectorWidget(object):

    key = setters.key()
    callback = setters.callback()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._PointSelectorWidget
        ), "you are not supposed to use this"
        self._widget = widget

    @setter
    def num_points(self, num_points):
        if num_points < 0:
            raise ValueError("num points should be zero or positive")
        self._widget.set_num_points(num_points)


class Text2DWidget:

    viewport = setters.viewport()

    def __init__(self, widget):
        assert isinstance(
            widget, _tviewer._Text2DWidget
        ), "you are not supposed to use this"
        self._widget = widget

    @setter
    def position(self, position):
        self._widget.set_position(position)

    @setter
    def text(self, text):
        self._widget.set_text(text)

    @setter
    def color(self, color):
        self._widget.set_color(_tviewer._Color(color))

    def show(self):
        self._widget.show()

    def hide(self):
        self._widget.hide()
