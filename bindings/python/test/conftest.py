import pytest

from tviewer import TViewer


@pytest.fixture
def callback():
    class Callback:
        def __init__(self):
            self.call_count = 0

        def __call__(self):
            self.call_count += 1

    return Callback()


@pytest.fixture
def callback_kwargs():
    class Callback:
        def __init__(self):
            self.call_count = 0

        def __call__(self, **kwargs):
            self.kwargs = kwargs
            self.call_count += 1

    return Callback()


@pytest.fixture
def viewer():
    return TViewer()
