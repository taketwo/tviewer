from tviewer import PointCloud

import numpy as np
import pytest


def test_point_cloud_invalid_xyz():
    # Invalid type
    with pytest.raises(TypeError):
        PointCloud([("a", 1, True)])
    with pytest.raises(TypeError):
        PointCloud([0, 1, 2])
    with pytest.raises(TypeError):
        PointCloud(["a", "b", "c"])

    # Invalid shape
    with pytest.raises(ValueError):
        PointCloud([])
    with pytest.raises(ValueError):
        PointCloud((1.0, 2.0, 3.0, 4.0))
    with pytest.raises(ValueError):
        PointCloud([1.0, 2.0])
    with pytest.raises(ValueError):
        PointCloud([[1.0, 2.0], [1.0, 2.0, 3.0]])
    with pytest.raises(ValueError):
        PointCloud(np.random.rand(3, 2))

    # Invalid type and shape
    with pytest.raises((TypeError, ValueError)):
        PointCloud(1)
    with pytest.raises((TypeError, ValueError)):
        PointCloud(True)
    with pytest.raises((TypeError, ValueError)):
        PointCloud("string")


def test_point_cloud_valid_xyz():
    PointCloud((1.0, 2.0, 3.0))
    PointCloud([1.0, 2.0, 3.0])
    PointCloud([[1.0, 2.0, 3.0], [0.0, 0.0, 0.0]])


def test_point_cloud_invalid_color_data():
    # 2 points, so valid color data should also have two items
    xyz = np.random.rand(2, 3)

    # Invalid type
    with pytest.raises(TypeError):
        PointCloud(xyz, ("a", 1))
    with pytest.raises(TypeError):
        PointCloud(xyz, ("foo", "bar"))
    with pytest.raises(TypeError):
        PointCloud(xyz, np.array([True, True]))

    # Invalid shape
    with pytest.raises(ValueError):
        PointCloud(xyz, 0)
    with pytest.raises(ValueError):
        PointCloud(xyz, [1])
    with pytest.raises(ValueError):
        PointCloud(xyz, [1, 2, 3])
    with pytest.raises(ValueError):
        PointCloud(xyz, np.random.rand(3, 3))
    with pytest.raises(ValueError):
        PointCloud(xyz, [])

    # Invalid type and shape
    with pytest.raises((TypeError, ValueError)):
        PointCloud(xyz, "string")
    with pytest.raises((TypeError, ValueError)):
        PointCloud(xyz, [True, True, True])
    with pytest.raises((TypeError, ValueError)):
        PointCloud(xyz, (1.0, 2.0, 3.0, 4.0))

    # Can not be non-None when xyz is None
    with pytest.raises(ValueError):
        PointCloud(None, np.random.rand(3, 3))


def test_point_cloud_valid_color_data():
    # 2 points, so valid color data should also have two items
    xyz = np.random.rand(2, 3)

    # RGB with floats
    PointCloud(xyz, np.random.rand(2, 3).astype(np.float32))
    PointCloud(xyz, np.random.rand(2, 3).astype(np.float64))
    # RGB with integers
    rgb = [[0, 0, 0], [255, 255, 255]]
    PointCloud(xyz, rgb)
    PointCloud(xyz, np.array(rgb, dtype=np.uint8))
    PointCloud(xyz, np.array(rgb, dtype=np.int32))
    # Labels (integers of different type)
    PointCloud(xyz, [0, 1])
    PointCloud(xyz, np.array([0, 1]))
    for dt in [np.int16, np.uint16, np.int32, np.uint32, np.int64, np.uint64]:
        PointCloud(xyz, np.array([0, 1], dtype=dt))

    # Also try with single point
    PointCloud([1.0, 2.0, 3.0], 1)
    PointCloud([1.0, 2.0, 3.0], [1])
