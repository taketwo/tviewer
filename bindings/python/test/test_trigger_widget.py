import pytest
from tviewer import TriggerWidget


class TestTriggerWidget:
    @pytest.fixture
    def widget(self, viewer):
        return viewer.add_trigger("widget")

    def test_callback(self, viewer, widget, callback):
        widget.callback = callback
        widget.key = "a"
        viewer.feed_key("a")
        assert callback.call_count == 1
        viewer.feed_key("b")
        assert callback.call_count == 1
        viewer.feed_key("a")
        assert callback.call_count == 2

    def test_callback_kwargs(self, viewer, widget, callback_kwargs):
        widget.callback = callback_kwargs
        widget.key = "a"
        viewer.feed_key("a")
        assert callback_kwargs.call_count == 1
        assert callback_kwargs.kwargs == {"widget": "a"}
        viewer.feed_key("b")
        assert callback_kwargs.call_count == 1
        viewer.feed_key("a")
        assert callback_kwargs.call_count == 2
        assert callback_kwargs.kwargs == {"widget": "a"}

    def test_add_trigger_widget(self, viewer, callback):
        w = viewer.add_trigger("widget1")
        assert isinstance(w, TriggerWidget)
        w = viewer.add_trigger("widget2", key="a")
        assert isinstance(w, TriggerWidget)
        w = viewer.add_trigger("widget3", key="a", callback=callback)
        assert isinstance(w, TriggerWidget)
