import pytest
from tviewer import ToggleWidget


class TestToggleWidget:
    @pytest.fixture
    def widget(self, viewer):
        return viewer.add_toggle("widget")

    @pytest.mark.parametrize("value", [True, False])
    def test_value(self, widget, value):
        widget.value = value
        assert widget.value == value

    @pytest.mark.parametrize("value", [True, False])
    def test_bool_cast(self, widget, value):
        widget.value = value
        assert bool(widget) == value

    def test_key(self, viewer, widget):
        widget.key = "a"
        widget.value = False
        viewer.feed_key("a")
        assert widget.value
        viewer.feed_key("a")
        assert not widget.value
        viewer.feed_key("b")
        assert not widget.value
        widget.key = "b"
        viewer.feed_key("a")
        assert not widget.value
        viewer.feed_key("b")
        assert widget.value

    def test_callback(self, viewer, widget, callback):
        widget.callback = callback
        # Execution of callback through value assignment
        widget.value = True
        assert callback.call_count == 1
        widget.value = False
        assert callback.call_count == 2
        widget.value = False
        assert callback.call_count == 2
        # Execution of callback through keyboard shortcut
        widget.key = "a"
        viewer.feed_key("a")
        assert callback.call_count == 3
        viewer.feed_key("b")
        assert callback.call_count == 3

    def test_callback_kwargs(self, viewer, widget, callback_kwargs):
        widget.callback = callback_kwargs
        # Execution of callback through value assignment
        widget.value = True
        assert callback_kwargs.call_count == 1
        assert callback_kwargs.kwargs == {"widget": True}
        widget.value = False
        assert callback_kwargs.call_count == 2
        assert callback_kwargs.kwargs == {"widget": False}
        widget.value = False
        assert callback_kwargs.call_count == 2
        # Execution of callback through keyboard shortcut
        widget.key = "a"
        viewer.feed_key("a")
        assert callback_kwargs.call_count == 3
        assert callback_kwargs.kwargs == {"widget": True}
        viewer.feed_key("b")
        assert callback_kwargs.call_count == 3
        viewer.feed_key("a")
        assert callback_kwargs.call_count == 4
        assert callback_kwargs.kwargs == {"widget": False}

    def test_add_toggle_widget(self, viewer, callback):
        w = viewer.add_toggle("widget1")
        assert isinstance(w, ToggleWidget)
        w = viewer.add_toggle("widget2", key="a")
        assert isinstance(w, ToggleWidget)
        w = viewer.add_toggle("widget3", key="a", callback=callback)
        assert isinstance(w, ToggleWidget)
        w = viewer.add_toggle("widget4", key="a", callback=callback, value=True)
        assert isinstance(w, ToggleWidget)
