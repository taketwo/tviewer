def key():
    def setter(self, key):
        if isinstance(key, str):
            self._widget.set_key(key)
        else:
            raise TypeError('key should be a string')
    return property(None, setter)


def keys():
    def setter(self, keys):
        if isinstance(keys, tuple) and len(keys) == 2 and isinstance(keys[0], str):
            self._widget.set_keys(keys)
        else:
            raise TypeError('keys should be a tuple with two strings')
    return property(None, setter)


def callback():
    def setter(self, callback):
        if not callable(callback):
            raise TypeError('callback should be callable')
        params = __import__("inspect").signature(callback).parameters
        if len(params) == 0:
            self._widget.set_callback(callback)
        elif len(params) == 1:
            for v in params.values():
                if v.kind != 4:
                    raise TypeError('callback should have 0 or 1 kwargs parameter')
            self._widget.set_kwargs_callback(callback)
        else:
            raise TypeError('callback should have 0 or 1 parameter')
    return property(None, setter, doc="Callback to be executed on state change.")


def dependencies():
    def setter(self, dependencies):
        if isinstance(dependencies, str):
            self._widget.set_dependencies({dependencies})
    return property(None, setter)


def viewport():
    def setter(self, viewport):
        if isinstance(viewport, int):
            self._widget.set_viewport(viewport)
    return property(None, setter)
