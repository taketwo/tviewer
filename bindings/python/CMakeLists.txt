pybind11_add_module(_tviewer src/tviewer.cpp)
target_link_libraries(_tviewer PRIVATE tviewer)
set_target_properties(_tviewer PROPERTIES CXX_STANDARD 14)
