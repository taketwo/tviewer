#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>

#include <tviewer/tviewer.h>


namespace py = pybind11;

using namespace tviewer;

template <typename Scalar> using MatrixX1 = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
template <typename Scalar> using MatrixX3 = Eigen::Matrix<Scalar, Eigen::Dynamic, 3>;
template <typename Scalar> using MatrixX4 = Eigen::Matrix<Scalar, Eigen::Dynamic, 4>;
using MatrixX1f = MatrixX1<float>;
using MatrixX1d = MatrixX1<double>;
using MatrixX1s = MatrixX1<int16_t>;
using MatrixX1su = MatrixX1<uint16_t>;
using MatrixX1i = MatrixX1<int32_t>;
using MatrixX1u = MatrixX1<uint32_t>;
using MatrixX1l = MatrixX1<int64_t>;
using MatrixX1lu = MatrixX1<uint64_t>;
using MatrixX3f = MatrixX3<float>;
using MatrixX3d = MatrixX3<double>;
using MatrixX4b = MatrixX4<uint8_t>;
using Vector3b = Eigen::Matrix<uint8_t, 3, 1>;

/** Assign data stored in an Eigen matrix to (a subset of) point cloud fields.
  * The target point cloud is viewed as a matrix of type Scalar, where rows correspond to points and columns correspond
  * to data fields. Each row source data is assigned to a column in the target point cloud, starting at a given column
  * offset (in bytes). */
template<typename PointT, typename Scalar>
void assign(pcl::PointCloud<PointT>& cloud, const Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>& data_source, unsigned int offset = 0)
{
  if (cloud.size() != static_cast<size_t>(data_source.rows()))
    throw std::runtime_error("Unable to assign data to point cloud fields (point cloud length does not match data length)");
  using Map = Eigen::Map<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>, Eigen::Aligned, Eigen::OuterStride<>>;
  auto data_target = reinterpret_cast<Scalar*>(reinterpret_cast<uint8_t*>(cloud.points.data()) + offset);
  Map(data_target, data_source.cols(), data_source.rows(), Eigen::OuterStride<>(sizeof(PointT) / sizeof(Scalar))) = data_source.transpose();
}

/** Create a XYZ point cloud. */
template<typename Scalar>
PointCloudConstPtr createPointCloud(py::EigenDRef<MatrixX3<Scalar>> points)
{
  using PointType = pcl::PointXYZ;
  typename pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>(points.rows(), 1));
  assign<PointType, float>(*cloud, points.template cast<float>());
  return cloud;
}

/** Create a XYZ + Intensity/Label point cloud. */
template<typename Scalar1, typename Scalar2>
PointCloudConstPtr createPointCloud(py::EigenDRef<MatrixX3<Scalar1>> points, py::EigenDRef<MatrixX1<Scalar2>> colors)
{
  if (colors.rows() != static_cast<int>(points.rows()))
    throw std::runtime_error("Colors should have the same number of rows as points");
  if (std::is_floating_point<Scalar2>::value)
  {
    using PointType = pcl::PointXYZI;
    typename pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>(points.rows(), 1));
    assign<PointType, float>(*cloud, points.template cast<float>());
    assign<PointType, float>(*cloud, colors.template cast<float>(), 16);
    return cloud;
  }
  if (std::is_integral<Scalar2>::value)
  {
    using PointType = pcl::PointXYZL;
    typename pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>(points.rows(), 1));
    assign<PointType, float>(*cloud, points.template cast<float>());
    assign<PointType, int>(*cloud, colors.template cast<int>(), 16);
    return cloud;
  }
}

/** Create a XYZ + RGBA point cloud. */
template<typename Scalar>
PointCloudConstPtr createPointCloud(py::EigenDRef<MatrixX3<Scalar>> points, py::EigenDRef<MatrixX4b> colors)
{
  if (colors.rows() != static_cast<int>(points.rows()))
    throw std::runtime_error("Colors should have the same number of rows as points");
  using PointType = pcl::PointXYZRGBA;
  pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>(points.rows(), 1));
  assign<PointType, float>(*cloud, points.template cast<float>());
  assign<PointType, uint8_t>(*cloud, colors, 16);
  return cloud;
}

template<typename CallbackT>
auto wrap(const CallbackT& callback) {
  return  [callback](const Kwargs& kw){
    py::dict kwargs;
    for (auto& k : kw.keys()) {
      try {
        kwargs[k.c_str()] = kw.get<double>(k);
      }
      catch (boost::bad_any_cast) { }
      try {
        kwargs[k.c_str()] = kw.get<bool>(k);
      }
      catch (boost::bad_any_cast) { }
      try {
        kwargs[k.c_str()] = kw.get<std::string>(k);
      }
      catch (boost::bad_any_cast) { }
      try {
        kwargs[k.c_str()] = kw.get<std::vector<int>>(k);
      }
      catch (boost::bad_any_cast) { }
    }
    callback(**kwargs);
  };
}

template<typename ReturnT, typename CallbackT>
auto wrapReturn(const CallbackT& callback) {
  return  [callback](const Kwargs& kw){
    py::dict kwargs;
    for (auto& k : kw.keys()) {
      try {
        kwargs[k.c_str()] = kw.get<double>(k);
      }
      catch (boost::bad_any_cast) { }
      try {
        kwargs[k.c_str()] = kw.get<bool>(k);
      }
      catch (boost::bad_any_cast) { }
      try {
        kwargs[k.c_str()] = kw.get<std::string>(k);
      }
      catch (boost::bad_any_cast) { }
    }
    return callback(**kwargs).template cast<ReturnT>();
  };
}

PYBIND11_MODULE(_tviewer, m)
{
  using namespace pybind11::literals;

  m.doc() = "TViewer Python bindings";

  py::class_<tviewer::TViewer>(m, "TViewer")
    .def(py::init<>())
    .def("run", &TViewer::run, "run", "keep_open"_a)
    .def("update", &TViewer::update, "update", "name"_a)
    .def("add_spinner", &TViewer::addSpinner, "add spinner", "name"_a)
    .def("add_trigger", &TViewer::addTrigger, "add trigger", "name"_a)
    .def("add_variant", &TViewer::addVariant, "add variant", "name"_a)
    .def("add_toggle", &TViewer::addToggle, "add toggle", "name"_a)
    .def("add_point_cloud", &TViewer::addPointCloud, "add point cloud", "name"_a)
    .def("add_primitive_objects", &TViewer::addPrimitiveObjects, "add primitive objects", "name"_a)
    .def("add_point_selector", &TViewer::addPointSelector, "add point selector", "name"_a)
    .def("add_text_2d", &TViewer::addText2D, "add 2d text", "name"_a)
    .def("add_viewport", &TViewer::addViewport, "add viewports", "xmin"_a, "ymin"_a, "xmax"_a, "ymax"_a, "separate_camera"_a.noconvert() = false)
    .def("feed_key", &TViewer::feedKey, "feed key", "key"_a)
    .def("set_background_color", [](TViewer& obj, const std::tuple<int, int, int>& colors){ obj.setBackgroundColor(Color(255, 255, 255)); }, "set background color", "color"_a)
    .def("wait_point_selected", [](TViewer& obj){
        size_t point_index;
        bool result = obj.waitPointSelected(point_index);
        return std::pair<size_t, bool>{point_index, result};
      }, "wait point selected");

    // .def("set_point_picking_callback", &Visualizer::setPointPickingCallback, "set point picking callback",
         // "callback"_a)
    // .def("set_keyboard_callback", &Visualizer::setKeyboardCallback, "set keyboard callback",
         // "callback"_a)
    // .def("load_camera_parameters", &Visualizer::loadCameraParameters, "load camera parameters", "filename"_a)
    // .def("wait_key_pressed", py::overload_cast<const std::vector<std::string>&>(&TViewerImpl::waitKeyPressed), "wait until one of the keys is pressed");
    // .def("wait_key_pressed", [](TViewerImpl& self, const std::vector<std::string>& keys){
        // std::string key;
        // auto result = self.waitKeyPressed(key, keys);
        // return std::pair<bool, std::string>{result, key}; }, "wait until one of the keys is pressed");
    // .def("remove", &Visualizer::remove, "remove a point cloud", "name"_a);

  auto color = py::class_<Color>(m, "_Color")
    .def(py::init([]{ return Color::White; }))
    .def(py::init([](Color::ColorName color_name){ return Color(color_name); }))
    .def(py::init([](const Vector3b& rgb){ return Color(rgb[0], rgb[1], rgb[2]); }));

  py::enum_<Color::ColorName>(color, "ColorName")
    .value("Black", Color::ColorName::Black)
    .value("Blue", Color::ColorName::Blue)
    .value("Brown", Color::ColorName::Brown)
    .value("Citron", Color::ColorName::Citron)
    .value("Corn", Color::ColorName::Corn)
    .value("Crimson", Color::ColorName::Crimson)
    .value("Cyan", Color::ColorName::Cyan)
    .value("Denim", Color::ColorName::Denim)
    .value("Emerald", Color::ColorName::Emerald)
    .value("Green", Color::ColorName::Green)
    .value("Indigo", Color::ColorName::Indigo)
    .value("LightGray", Color::ColorName::LightGray)
    .value("Magenta", Color::ColorName::Magenta)
    .value("Mango", Color::ColorName::Mango)
    .value("MaximumBlue", Color::ColorName::MaximumBlue)
    .value("MaximumGreen", Color::ColorName::MaximumGreen)
    .value("MaximumRed", Color::ColorName::MaximumRed)
    .value("Orange", Color::ColorName::Orange)
    .value("Pear", Color::ColorName::Pear)
    .value("Purple", Color::ColorName::Purple)
    .value("Red", Color::ColorName::Red)
    .value("Rose", Color::ColorName::Rose)
    .value("Ruby", Color::ColorName::Ruby)
    .value("Teal", Color::ColorName::Teal)
    .value("White", Color::ColorName::White)
    .value("Yellow", Color::ColorName::Yellow)
    .export_values();

  py::class_<Arrow>(m, "_Arrow")
    .def(py::init([]{ return Arrow{}; }))
    .def_readwrite("source", &Arrow::source)
    .def_readwrite("target", &Arrow::target)
    .def_readwrite("color", &Arrow::color);

  py::class_<Axes>(m, "_Axes")
    .def(py::init([]{ return Axes{}; }))
    .def_readwrite("position", &Axes::position)
    .def_readwrite("orientation", &Axes::orientation)
    .def_readwrite("size", &Axes::size);

  py::class_<Box>(m, "_Box")
    .def(py::init([]{ return Box{}; }))
    .def_readwrite("center", &Box::center)
    .def_readwrite("orientation", &Box::orientation)
    .def_readwrite("dimensions", &Box::dimensions);

  py::class_<LineSegment>(m, "_LineSegment")
    .def(py::init([]{ return LineSegment{}; }))
    .def_readwrite("start", &LineSegment::start)
    .def_readwrite("end", &LineSegment::end)
    .def_readwrite("color", &LineSegment::color);

  py::class_<Plane>(m, "_Plane")
    .def(py::init([]{ return Plane{}; }))
    .def_readwrite("normal", &Plane::normal)
    .def_readwrite("position", &Plane::position)
    .def_readwrite("color", &Plane::color);

  py::class_<Sphere>(m, "_Sphere")
    .def(py::init([]{ return Sphere{}; }))
    .def_readwrite("center", &Sphere::center)
    .def_readwrite("radius", &Sphere::radius)
    .def_readwrite("color", &Sphere::color);

  py::class_<PrimitiveObject>(m, "_PrimitiveObject")
    .def(py::init([](const Arrow& arrow){ return arrow; }))
    .def(py::init([](const Axes& axes){ return axes; }))
    .def(py::init([](const Box& box){ return box; }))
    .def(py::init([](const Cylinder& cylinder){ return cylinder; }))
    .def(py::init([](const LineSegment& line_segment){ return line_segment; }))
    .def(py::init([](const Plane& plane){ return plane; }))
    .def(py::init([](const Sphere& sphere){ return sphere; }));

  py::class_<PointCloudConstPtr>(m, "_PointCloud")
    .def(py::init([]{ return PointCloudConstPtr(nullptr); }))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>>(&createPointCloud<float>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>>(&createPointCloud<double>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1f>>(&createPointCloud<float, float>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1d>>(&createPointCloud<float, double>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1f>>(&createPointCloud<double, float>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1d>>(&createPointCloud<double, double>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1s>>(&createPointCloud<float, int16_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1s>>(&createPointCloud<double, int16_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1su>>(&createPointCloud<float, uint16_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1su>>(&createPointCloud<double, uint16_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1i>>(&createPointCloud<float, int32_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1i>>(&createPointCloud<double, int32_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1u>>(&createPointCloud<float, uint32_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1u>>(&createPointCloud<double, uint32_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1l>>(&createPointCloud<float, int64_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1l>>(&createPointCloud<double, int64_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX1lu>>(&createPointCloud<float, uint64_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX1lu>>(&createPointCloud<double, uint64_t>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3f>, py::EigenDRef<MatrixX4b>>(&createPointCloud<float>)))
    .def(py::init(py::overload_cast<py::EigenDRef<MatrixX3d>, py::EigenDRef<MatrixX4b>>(&createPointCloud<double>)));

  py::class_<PointCloudWidgetProperties>(m, "_PointCloudWidget")
    .def("show", &PointCloudWidgetProperties::show, "show")
    .def("hide", &PointCloudWidgetProperties::hide, "hide")
    .def("set_viewport", [](PointCloudWidgetProperties& obj, unsigned int viewport){ obj.viewport(viewport); })
    .def("set_point_size", [](PointCloudWidgetProperties& obj, unsigned int point_size){ obj.pointSize(point_size); })
    .def("set_lut_range", [](PointCloudWidgetProperties& obj, const std::pair<double, double>& range){ obj.lutRange(range.first, range.second); })
    .def("set_data", [](PointCloudWidgetProperties& obj, const PointCloudConstPtr& data){ obj.data(data); })
    .def("set_data_callback", [](PointCloudWidgetProperties& obj, GetDataCallback<PointCloudConstPtr>& callback){ obj.dataCallback(callback); })
    .def("set_data_kwargs_callback", [](PointCloudWidgetProperties& obj, py::function callback){ obj.dataCallback(wrapReturn<PointCloudConstPtr>(callback)); })
    .def("set_key", [](PointCloudWidgetProperties& obj, const std::string& key){ obj.key(key); })
    .def("set_colors", [](PointCloudWidgetProperties& obj, const std::string& colors){ obj.colors(colors); })
    .def("set_colors", [](PointCloudWidgetProperties& obj, const Color& colors){ obj.colors(colors); })
    .def("set_colors", [](PointCloudWidgetProperties& obj, const std::tuple<int, int, int>& colors){ obj.colors(Color(colors)); })
    .def("set_colors", [](PointCloudWidgetProperties& obj, const std::tuple<float, float, float>& colors){ obj.colors(Color(colors)); })
    .def("set_dependencies", [](PointCloudWidgetProperties& obj, const std::set<std::string>& dependencies){ obj.dependencies(dependencies); });

  py::class_<PrimitiveObjectsWidgetProperties>(m, "_PrimitiveObjectsWidget")
    .def("show", &PrimitiveObjectsWidgetProperties::show, "show")
    .def("hide", &PrimitiveObjectsWidgetProperties::hide, "hide")
    .def("set_viewport", [](PrimitiveObjectsWidgetProperties& obj, unsigned int viewport){ obj.viewport(viewport); })
    .def("set_data", [](PrimitiveObjectsWidgetProperties& obj, const PrimitiveObjects& data){ obj.data(data); })
    .def("set_key", [](PrimitiveObjectsWidgetProperties& obj, const std::string& key){ obj.key(key); })
    .def("set_dependencies", [](PrimitiveObjectsWidgetProperties& obj, const std::set<std::string>& dependencies){ obj.dependencies(dependencies); })
    .def("set_wireframe", [](PrimitiveObjectsWidgetProperties& obj, bool wireframe){ obj.wireframe(wireframe); });

  py::class_<SpinnerWidgetProperties>(m, "_SpinnerWidget")
    .def("set_value", [](SpinnerWidgetProperties& obj, double value){ obj.value(value); })
    .def("get_value", [](SpinnerWidgetProperties& obj){ return obj.value(); })
    .def("set_step", [](SpinnerWidgetProperties& obj, double step){ obj.step(step); })
    .def("set_range", [](SpinnerWidgetProperties& obj, const std::pair<double, double>& range){ obj.range(range.first, range.second); })
    .def("set_range", [](SpinnerWidgetProperties& obj, size_t length){ obj.range(length); })
    .def("set_wrap_around", [](SpinnerWidgetProperties& obj, bool wrap_around){ obj.wrapAround(wrap_around); })
    .def("set_key", [](SpinnerWidgetProperties& obj, const std::string& key){ obj.key(key); })
    .def("set_keys", [](SpinnerWidgetProperties& obj, const std::pair<std::string, std::string>& keys){ obj.key(keys.first, keys.second); })
    .def("set_callback", [](SpinnerWidgetProperties& obj, Callback& callback){ obj.callback(callback); })
    .def("set_kwargs_callback", [](SpinnerWidgetProperties& obj, py::function callback){ obj.callback(wrap(callback)); });

  py::class_<TriggerWidgetProperties>(m, "_TriggerWidget")
    .def("set_key", [](TriggerWidgetProperties& obj, const std::string& key){ obj.key(key); })
    .def("set_keys", [](TriggerWidgetProperties& obj, const std::vector<std::string>& keys){ obj.keys(keys); })
    .def("set_callback", [](TriggerWidgetProperties& obj, Callback& callback){ obj.callback(callback); })
    .def("set_kwargs_callback", [](TriggerWidgetProperties& obj, py::function callback){ obj.callback(wrap(callback)); });

  py::class_<VariantWidgetProperties>(m, "_VariantWidget")
    .def("set_variants", [](VariantWidgetProperties& obj, const std::vector<std::string>& variants){ obj.variants(variants); })
    .def("set_value", [](VariantWidgetProperties& obj, const std::string& value){ obj.value(value); })
    .def("set_value_index", [](VariantWidgetProperties& obj, size_t value_index){ obj.valueIndex(value_index); })
    .def("get_value", [](VariantWidgetProperties& obj){ return obj.value(); })
    .def("set_keys", [](VariantWidgetProperties& obj, const std::pair<std::string, std::string>& keys){ obj.key(keys.first, keys.second); })
    .def("set_callback", [](VariantWidgetProperties& obj, Callback& callback){ obj.callback(callback); })
    .def("set_kwargs_callback", [](VariantWidgetProperties& obj, py::function callback){ obj.callback(wrap(callback)); });

  py::class_<ToggleWidgetProperties>(m, "_ToggleWidget")
    .def("set_value", [](ToggleWidgetProperties& obj, bool value){ obj.value(value); })
    .def("get_value", [](ToggleWidgetProperties& obj){ return obj.value(); })
    .def("set_key", [](ToggleWidgetProperties& obj, const std::string& key){ obj.key(key); })
    .def("set_callback", [](ToggleWidgetProperties& obj, Callback& callback){ obj.callback(callback); })
    .def("set_kwargs_callback", [](ToggleWidgetProperties& obj, py::function callback){ obj.callback(wrap(callback)); });

  py::class_<PointSelectorWidgetProperties>(m, "_PointSelectorWidget")
    .def("set_num_points", [](PointSelectorWidgetProperties& obj, unsigned int num_points){ obj.numPoints(num_points); })
    .def("set_key", [](PointSelectorWidgetProperties& obj, const std::string& key){ obj.key(key); })
    .def("set_callback", [](PointSelectorWidgetProperties& obj, Callback& callback){ obj.callback(callback); })
    .def("set_kwargs_callback", [](PointSelectorWidgetProperties& obj, py::function callback){ obj.callback(wrap(callback)); });

  py::class_<Text2DWidgetProperties>(m, "_Text2DWidget")
    .def("show", &Text2DWidgetProperties::show, "show")
    .def("hide", &Text2DWidgetProperties::hide, "hide")
    .def("set_key", [](Text2DWidgetProperties& obj, const std::string& key){ obj.key(key); })
    .def("set_position", [](Text2DWidgetProperties& obj, const std::pair<unsigned int, unsigned int>& position){ obj.position(position); })
    .def("set_text", [](Text2DWidgetProperties& obj, const std::string& text){ obj.text(text); })
    .def("set_viewport", [](Text2DWidgetProperties& obj, unsigned int viewport){ obj.viewport(viewport); })
    .def("set_color", [](Text2DWidgetProperties& obj, const Color& color){ obj.color(color); });

#ifdef VERSION_INFO
  m.attr("__version__") = VERSION_INFO;
#else
  m.attr("__version__") = "dev";
#endif
}


