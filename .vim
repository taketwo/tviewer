" Execute on every buffer enter event {{{

" }}}

if g:localvimrc_sourced_once_for_file
  finish
endif

" Execute only once for an edited file {{{

if filereadable(globpath(g:localvimrc_script_dir."/build", "build.ninja"))
  let b:builder = "ninja"
else
  let b:builder = "make"
endif

let &makeprg=b:builder."\ -C".g:localvimrc_script_dir."/build"

" }}}

if g:localvimrc_sourced_once
  finish
endif

" Execute only once for a running vim instance {{{

augroup AddSnippets
  autocmd!
  autocmd FileType rst :let b:mcf_make_target="doc"
augroup END

" }}}
