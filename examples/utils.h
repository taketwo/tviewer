/******************************************************************************
 * Copyright (c) 2018 Sergey Alexandrov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#pragma once

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

/// Generate a planar point cloud of a given type.
/// The cloud points are located at the nodes of a uniform grid spanning along X and Y axes.
/// The location of the grid is controlled with \a offset, the number of points with \a dims, and
/// the size of the grid cell with \a step.
template <typename PointT>
typename pcl::PointCloud<PointT>::Ptr generatePlanarPointCloud(const Eigen::Vector3f& offset,
                                                               const Eigen::Vector2i& dims,
                                                               float step) {
  typename pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>(dims[0], dims[1]));
  for (int i = 0; i < dims[0]; ++i)
    for (int j = 0; j < dims[1]; ++j) {
      cloud->at(i, j).getVector3fMap() = offset + Eigen::Vector3f(i, j, 0) * step;
    }
  return cloud;
}

// template <typename PointT> typename pcl::PointCloud<PointT>::Ptr
// generateRandomPointCloud (const Eigen::Vector3f& offset = {0, 0, 0},
                          // const Eigen::Vector3f& dims = {1, 1, 1},
                          // size_t num_points = 1000)
// {
  // using Generator = pcl::common::UniformGenerator<float>;
  // std::vector<Generator::Parameters> p;
  // for (size_t i = 0; i < 3; ++i)
    // p.emplace_back (offset[i], offset[i] + dims[i], time (0) + i);
  // typename pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
  // pcl::common::CloudGenerator<PointT, Generator> (p[0], p[1], p[2]).fill (num_points, 1, *cloud);
  // return cloud;
// }

// template <typename PointT>
// void generatePlanarPointCloud(const Eigen::Vector3f& offset,
                                                               // const Eigen::Vector2i& dims,
                                                               // float step,
                                                               // std::function<void(PointT&)> func) {
      // func(cloud->at(i, j));
// }

