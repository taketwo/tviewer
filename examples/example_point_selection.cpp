#include <tviewer/tviewer.h>

#include "utils.h"

int main(int argc, const char** argv) {
  auto cloud = generatePlanarPointCloud<pcl::PointXYZ>({0, 0, 0}, {12, 12}, 0.01f);

  tviewer::TViewer viewer(argc, argv);
  viewer.setWindowTitle("Point selection example");

  // Add a point cloud.
  viewer.addPointCloud("grid").data(cloud);

  // Add selector widget.
  auto selector = viewer.addPointSelector("selector");

  // In subsequent code we will (re)configure this widget to demonstrate different operation modes.
  // For now we only set a callback that is executed when selection is complete. This callback
  // prints the list of selected point indices that was passed to it.
  selector.callback([&](const std::vector<int>& indices) {
    std::cout << "Selected point indices: ";
    for (const auto& idx : indices)
      std::cout << idx << " ";
    std::cout << std::endl;
  });

  // Note that the widget does not cease to operate once the selection has been made. It is
  // immediately ready to accept the next selection. Therefore, each of the following examples will
  // remain active and allow you to repeat selections until you press "e" to terminate visualization
  // and thus jump to the next example.

  std::cout << "Example 1: single point selection" << std::endl;
  // In this mode selection is considered complete once the user has selected a single point. The
  // selected point will be highlighted and will stay visible until the user selects another point.
  //
  // In fact, this is the default mode, so we do not need to configure the widget at all.
  //
  // Just like in vanilla PCL visualizer, selection is done by clicking a point with Shift key
  // pressed.
  viewer.run(tviewer::KeepWindowOpen);

  std::cout << "Example 2: single point selection without visualization" << std::endl;
  // This mode is the same as before, but visualization is disabled, so the user can not see which
  // point was selected. The selected indiced will continue to be printed, of course.
  selector.showSelected(false);
  viewer.run(tviewer::KeepWindowOpen);

  std::cout << "Example 3: selection of fixed number (3) of points" << std::endl;
  // In this mode the widget awaits for the user to select a specified number of points (three in
  // this particular example).
  selector.numPoints(3);
  // We re-enable visualization of selected points because this is a generally useful thing.
  selector.showSelected();
  viewer.run(tviewer::KeepWindowOpen);

  std::cout << "Example 4: unrestricted multiple point selection" << std::endl;
  std::cout << "           press space when done" << std::endl;
  // In this mode the user can select as many points as she wants.
  selector.multiple();
  // To terminate point selection process the user should press "space" key.
  selector.key("space");
  // Note that there is no default selection termination key. You always have to set it explicitly,
  // otherwise the user will not be able to finish selection in this mode.
  viewer.run(tviewer::KeepWindowOpen);

  std::cout << "Example 5: unrestricted multiple point selection with deselection" << std::endl;
  std::cout << "           click selected point to deselect, press space when done" << std::endl;
  // You may have noticed before that clicking an an already selected point does nothing. For this
  // example we will reconfigure the widget to deselect points in this case.
  selector.enableDeselection();
  viewer.run(tviewer::KeepWindowOpen);

  return 0;
}
