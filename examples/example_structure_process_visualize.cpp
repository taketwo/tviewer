#include <tviewer/tviewer.h>

#include "utils.h"

int main(int argc, const char** argv) {
  // This is an example of an application structured in the "process/visualize" fashion. First it
  // performs all necessary computations, then presents the results to the user for visual
  // inspection.
  //
  // This example is only to demonstrate the structure, so we will not do any useful processing, but
  // rather simply generate a regular grid of points. During visual inspection one of the grid
  // points will be highlighted. The user will be able to interactively change which point is
  // highlighted by pressing "b" or "B" (changing point index up or down).

  /*************
   *  Process  *
   *************/

  // In the "process" part we will simply generate of a fixed pointcloud.
  // Real application will read inputs, apply point cloud processing algorithms, store results, etc.
  auto cloud = generatePlanarPointCloud<pcl::PointXYZ>({0, 0, 0}, {50, 50}, 0.01f);

  /***************
   *  Visualize  *
   ***************/

  // In the "visualize" part we will setup visualization and then kick off the GUI loop.
  // First, instantiate a TViewer. This does not show the window yet, so we can completely setup
  // visualization before showing it to the user.
  tviewer::TViewer viewer(argc, argv);
  viewer.setWindowTitle("Process/visualize example");

  viewer.addTrigger("trigger")
      .callback([](const tviewer::Kwargs& state) {
        auto key = state.get<std::string>("trigger");
        std::cout << "Triggered key: " << key << std::endl;
      })
      .keys({"b", "w"});
  auto toggle = viewer.addToggle("toggle").key("space");
  viewer
      .addSpinner("index")
      // .description("Index of highlighted point")
      .key("i")
      .range(0, cloud->size() - 1)
      .wrapAround();
  // .printOnChange();

  // Add a point cloud.
  viewer
      .addPointCloud("grid")
      // .description("XYZ point cloud grid")  // short description to display in help
      .key("a")  // use "a" to toggle visibility
      .dependencies({"index", "trigger"})
      .dataCallback([&](const tviewer::Kwargs& state) {
        auto index = state.get<double>("index");
        cloud->at(index).z += 0.02f;
        return cloud;
      })              // use cloud as data source
      .pointSize(10)  // large points
      .colors("z");   // colorize according to the "z" field values

  // Add a keyboard listener
  // We are in the

  // Finally, we start launch the visualization window and start interactive loop.
  // The user may interact with the visualization, control the camera as well as the visibility of
  // the visualization objects.
  viewer.run();

  // Once the user closes the window, the function will return. This is the end of or application as
  // well. In a real-world app you may want to do something else though, maybe save the results.

  return 0;
}
