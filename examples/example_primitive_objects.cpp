#include <tviewer/tviewer.h>

int main(int argc, const char** argv) {
  // This is an example of visualization of so-called "primitive objects". A primitive object is an
  // object that is described by a few parameters, such as:
  //  * Arrow
  //  * Axes
  //  * Box
  //  * Cone
  //  * Cylinder
  //  * Line segment
  //  * Plane
  //  * Sphere

  tviewer::TViewer viewer(argc, argv);
  viewer.setWindowTitle("Primitive objects example");

  // Let's start with flat (2D) arrows. An arrow is colored line segment connecting source and
  // target points, with a tip attached to the target.

  // You can create an arrow by providing source, target, and color:
  const tviewer::Arrow arrow1{Eigen::Vector3f(0, 0, 0), Eigen::Vector3f(1, 1, 1),
                              tviewer::Color::Pear};
  // Alternatively, you may use on of the static factory functions:
  auto arrow2 = tviewer::Arrow::fromVectors(Eigen::Vector3f(0, 0, 0), Eigen::Vector3f(1, 1, -1),
                                            tviewer::Color::Crimson);

  // Now that the arrows have been created, they can be added to the PrimitiveObjects widget:
  viewer.addPrimitiveObjects("arrows").data({arrow1, arrow2}).key("1");

  // Note that the objects are copied during the data() call, so the following modification has no
  // effect on the color of the displayed arrow:
  arrow2.color = tviewer::Color::Magenta;

  // TViewer has a special widget class that can display one or multiple primitive objects. The
  // objects displayed by a widget do not have to have the same kind.
  tviewer::Sphere sphere{Eigen::Vector3f(2, 2, 2), 0.2f, tviewer::Color::Indigo};
  tviewer::Cylinder cylinder{Eigen::Vector3f(0, 0, 0), Eigen::Vector3f(1, 0, 0), 0.3};
  tviewer::Box box{Eigen::Vector3f(-1, 0, 0), Eigen::Quaternionf{}, Eigen::Vector3f(0.2, 0.2, 0.2)};
  tviewer::Axes axes;
  tviewer::LineSegment line_segment{Eigen::Vector3f(1.3, 0, 0.3), Eigen::Vector3f(1.3, 0, -0.3),
                                    tviewer::Color::Purple};
  tviewer::Plane plane{Eigen::Vector3f(0.1, 0.0, 0), Eigen::Vector3f{0, 0, 0},
                       Eigen::Vector2f{1, 1}, tviewer::Color::Crimson};

  tviewer::Colors colors{tviewer::Color::MaximumRed, tviewer::Color::MaximumGreen,
                         tviewer::Color::MaximumBlue};
  tviewer::PrimitiveObjects plane_objects;
  for (size_t i = 0; i < 9; ++i) {
    Eigen::Vector3f origin(2.0 + 0.2 * i, 0, 0);
    Eigen::Vector3f normal =
        Eigen::AngleAxisf(0.1 * i, Eigen::Vector3f::UnitY()) * Eigen::Vector3f::UnitX();
    plane_objects.push_back(
        tviewer::Plane::fromVectorAndNormal(origin, normal, {0.1, 0.1 + 0.05 * i}, colors[i % 3]));
    plane_objects.push_back(tviewer::Sphere::fromVector(origin, 0.01, colors[i % 3]));
  }
  viewer.addPrimitiveObjects("planes").data(plane_objects).key("1");

  tviewer::PrimitiveObjects box_objects;
  for (size_t i = 0; i < 9; ++i) {
    Eigen::Vector3f origin(0, 2.0 + 0.25 * i, 0);
    Eigen::Isometry3f pose =
        Eigen::Isometry3f(Eigen::Translation3f(origin)) *
        Eigen::Isometry3f(Eigen::AngleAxisf(0.2 * i, Eigen::Vector3f::UnitZ()));
    const auto color = tviewer::getColor(0.1f * i, tviewer::ColorMap::COOL);
    box_objects.push_back(tviewer::Box::fromTransform(pose, {0.1f, 0.1f + 0.03f * i, 0.1f}, color));
    box_objects.push_back(tviewer::Sphere::fromVector(origin, 0.01, color));
  }
  viewer.addPrimitiveObjects("boxes").data(box_objects).key("2");

  tviewer::PrimitiveObjects cylinder_objects;
  for (size_t i = 0; i < 9; ++i) {
    Eigen::Vector3f start(0, -0.1 * (i + 1), 2.0 + 0.25 * i);
    Eigen::Vector3f end(0.1 * (i + 1), 0, 2.0 + 0.25 * i);
    const auto color = tviewer::getColor(0.1f * i, tviewer::ColorMap::SUMMER);
    cylinder_objects.push_back(tviewer::Cylinder::fromVectors(start, end, 0.05, color));
  }
  viewer.addPrimitiveObjects("cylinders").data(cylinder_objects).key("3");

  viewer.addPrimitiveObjects("objects").data({sphere, cylinder, box, axes, line_segment}).key("b");
  viewer.addText2D("text")
      .text("hello world\nsecond line")
      .color(tviewer::Color::Denim)
      .fontSize(16)
      .dataCallback([](const tviewer::Kwargs& kwargs) {
        return std::string("objects visible: ") + (kwargs.get<bool>("objects") ? "yes" : "no");
      })
      .dependencies("objects");

  viewer.run();

  return 0;
}
