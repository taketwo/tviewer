#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is an example application demonstrating the basics of constructing interactive
visualizations with tviewer.

We will display two point clouds. The first one will be static, whereas the second one
will be dynamically regenerated based on the keyboard input from the user.
"""

import numpy as np
import tviewer as tv

# We start by creating an instance of TViewer that will manage visualization.
# Note that this will not pop up any windows or start user interaction loop.
viewer = tv.TViewer()

# With the TViewer object in place, we can now configure the visualization by adding
# various widgets. Once we are done with that, we will launch visualization and
# user interaction by calling ``viewer.run()``.

# Let's add our first point cloud widget to the viewer.
# The only required parameter is the widget name, which has to be unique.
# The function returns a widget object that we can use later to set various properties
# related to how the point cloud is visualized.
w1 = viewer.add_point_cloud("square")

# If we launch the visualization now, we would see nothing but black screen. This
# is because the created widget has no data associated with it. This can be fixed by
# setting the ``data`` property of the widget. So let's create a Numpy array with
# four points and assign it to ``widget.data``:
w1.data = np.array([[-1, -1, 0], [-1, 1, 0], [1, 1, 0], [1, -1, 0]], dtype=np.float32)

# Note that the shape of the array should be (n, 3) and the data type should be either
# single or double precision floating point.

# If we launch the visualization now, we would see four white points arranged as a
# square. Great! But the visualization is not interactive and it is not possible to
# toggle the visibility of these points.

# This can be changed by assigning a toggle key to the widget. Let's use "s":
w1.key = "s"

# There are couple more things about this widget that can be changed.
# First, we can adjust the point size. There are only four points, so let's make them
# really large:
w1.point_size = 16

# Second, we can change the color of the points. For example, let's paint them yellow:
w1.colors = (255, 255, 0)

# Finally, we are done configuring the first point cloud widget. Let's launch
# visualization and check the result. Note that you can toggle visibility of the
# point cloud with "s". Also, just like with the standard PCL Visualizer, you can
# press "h" to get help, plus the list of widgets and their status. Once you are done,
# press "e" to exit, same as you would do with the standard PCL Visualizer. After that
# we will add more widgets and run visualization again, so we pass the ``keep_open``
# flag to prevent the window from closing.
viewer.run(keep_open=True)

# Okay, let's now turn to the second, dynamic point cloud. This will be a sinusoid wave
# with adjustable amplitude. The *z* coordinates of the points will be the values of
# the sine function evaluated on the grid in *xy* plane.

# First, the amplitude. It should be a positive real number, perhaps not too large.
# The user should be able to adjust it with the keyboard. This is a perfect use case
# for the spinner widget. Unlike the point cloud widget that we added before, this one
# is not represented in the visualizer and only serves to interact with the user.

# Let's add a spinner. Note that last time we specified widget properties one-by-one,
# but that was only for educational purposes. This time we will use a more convenient
# and concise way, specifying all settings at once:
# TODO: change keys to key
viewer.add_spinner("amplitude", keys=("up", "down"), value=0.1, step=0.05, range=(0, 1))

# Now that we have a user controllable amplitude value, let's create a point cloud
# widget that will depend on it. Instead of providing concrete numerical data, we will
# have a function that can be used to generate the data.

# By convention, such data generation function should return a Numpy array and should
# either take **kwargs arguments, or no arguments at all. In this case, we want the
# generated point cloud to depend on the amplitude, so we will have to have arguments.

# But what exactly are these **kwargs and where do they come from? These are the state
# values of all widgets registered in the viewer. What are these state values? Depends
# on the widget type. For point cloud widget the state is a boolean flag indicating
# whether the point cloud is visible or not. For the spinner widget, obviously, it's
# the controlled value itself. TViewer will take care of querying the widgets and
# packaging their states into a dictionary where keys correspond to the widget names.


def make_wave(**kwargs):
    # Get the amplitude from the kwargs dictionary
    amplitude = kwargs["amplitude"]
    # Generate grid
    s = np.linspace(-0.9, 0.9, 50)
    x, y = np.meshgrid(s, s, indexing="ij")
    # Evaluate z coordinates
    z = np.sin((x + y) * 5) * amplitude
    # Output array with point cloud data
    return np.stack([x.flatten(), y.flatten(), z.flatten()], axis=1)


# Finally, we can add a second point cloud widget. Again, we will configure it at the
# time of creation:
viewer.add_point_cloud(
    "wave", key="w", point_size=5, data=make_wave, colors="z", dependencies="amplitude"
)

# Note the last argument, "dependencies". This specifies upon the state of which other
# widgets the data generation function of this point cloud widget depends. In our case,
# it's the "amplitude" spinner. Each time the user changes its state through keyboard
# interaction, the data generation function will be called and the visualization will
# be updated.

# There is one last step that we probably want to do before we launch visualization.
# Currently, the second point cloud widget has no data associated with it! Indeed, we
# provided a generator function, but TViewer will not execute it automatically. Thus,
# if we were to start visualization now, there would be no point cloud until we change
# the amplitude value. In some cases this behavior is desirable, but this time we want
# to see a point cloud right away, so we make the following call to ensure that data
# are generated and added to visualizer:
viewer.update("wave")

# Setup complete, time to run. Remember that the amplitude can be changed using the
# arrow keys, and the visibility of the point clouds can be toggled using "s" and "w".
viewer.run()

# Done! Note that most of the lines in this file are comments. The visualization is
# created with just a few of lines and zero boiler-plate code.
