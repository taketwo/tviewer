#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is an example application demonstrating all supported ways to specify colors for
point cloud visualization.

We will display 10 grid-like point clouds one next to another. While their shapes will
be the same, their colors will be different.
"""

import numpy as np
import tviewer as tv


# First we define a helper function to generate a grid-like point cloud. Since we plan
# to display them side-by-side, there is a displacement parameter to shift point
# coordinates away from origin.


def make_grid(displacement=0):
    s = np.linspace(-0.9, 0.9, 50)
    x, y, z = np.meshgrid(s, s, 0, indexing="ij")
    xyz = np.stack([x.flatten() + 2 * displacement, y.flatten(), z.flatten()], axis=1)
    return np.array(xyz)


# As usual, we start by creating an instance of TViewer that will manage visualization.
viewer = tv.TViewer()

# Let's add our first point cloud. We will only specify point coordinates, no color
# data or color-related settings. It will be displayed using the default color, which
# happens to be... yep, white.
viewer.add_point_cloud("grid 1 (no color)", data=make_grid(0))

# The second point cloud will also have no color data, but we will set a fixed color
# (red) to uniformly paint it. The color is given by a triplet of floating point
# numbers in [0...1] range:
viewer.add_point_cloud("grid 2 (red color)", data=make_grid(1), colors=(1.0, 0.0, 0.0))

# The third point cloud will also have a fixed color, however we specify it using a
# triplet of integers in [0...255] range:
viewer.add_point_cloud("grid 3 (gray color)", data=make_grid(2), colors=(90, 90, 90))

# The next point cloud will be colored by colormapping the "y" coordinate (using viridis
# or jet palette, depending on your PCL version). Needless to say, you can also colormap
# "x" or "z" coordinate.
viewer.add_point_cloud("grid 4 ('y' colormap)", data=make_grid(3), colors="y")

# We have exhausted our options to colorize point cloud without actually providing
# color data. Let's now examine how exactly color data can be provided and which formats
# are supported.

# Just like the point coordinates, color data should be a Numpy array. For obvious
# reasons, this array should have the same first dimension as the point coordinates
# array. Both arrays should be tied into a tuple and passed as ``data`` parameter of
# the ``add_point_cloud()`` function or set to ``data`` property of already existing
# point cloud widget.

# First supported color data format is triplets of floating point numbers in [0...1]
# range. So let's create a new point cloud and an appropriately sized color array:
points4 = make_grid(4)
colors4 = np.ones((len(points4), 3), dtype=np.float32)
# So far it is all ones, meaning that all the points will be white. The following line
# sets the red channel to a sequence of evenly spaced numbers, which should result
# in a white → cyan gradient:
colors4[:, 2] = np.linspace(1, 0, len(points4))
# Finally, use the created arrays to add a new point cloud widget:
viewer.add_point_cloud("grid 5 (rgb colors, float)", data=(points4, colors4))

# Similarly to how the ``colors`` parameter supported both floating point and integer
# triplets for a fixed color, color data supports Numpy arrays of integer triplets
# with numbers in [0...255] range. Let's use the same array as before, but scale it
# by 255 and convert to bytes. The resulting point cloud will look the same:
viewer.add_point_cloud(
    "grid 6 (rgb colors, uint8)", data=(make_grid(5), (colors4 * 255).astype(np.uint8))
)

# The next color data format is suitable for visualization of categorical data, e.g.
# semantically labeled point clouds. Color data should be an array with a single
# integer number per point defining its "label". Points with same labels will be
# visualized with the same color.

# Let's create a new point cloud and a list of labels for it. The labels are cycling
# through (0, 1, 2):
points6 = make_grid(6)
colors6 = [i % 3 for i in range(len(points6))]
# Now use the created arrays to add a new point cloud widget. Note that we pass a
# Python list object without explicitly converting it to Numpy array, that's okay:
viewer.add_point_cloud("grid 6 (labels)", data=(points6, colors6))

# The last color data format is suitable for visualizing continuous functions over the
# points using colormapping. Color data should be an array with a single floating point
# number per point. These numbers will be mapped to colors (using viridis or jet
# palette, depending on your PCL version).

# Let's create one more point grid and color data by evaluating the function
# f(p) = sin(1.5 + 4 * p_x) at every grid point:
points7 = make_grid(7)
colors7 = [np.sin(1.5 + 4 * p[1]) for p in points7]
viewer.add_point_cloud("grid 7 (colormap)", data=(points7, colors7))

# The created widget will automatically adjust the colormap to the range of the color
# data such that the minimum and maximum color data value are mapped to the first and
# last element of the colormap table. In some situations you may want to have a fixed
# range though. This can be specified using ``lut_range``:
viewer.add_point_cloud(
    "grid 8 (colormap, custom range)",
    data=(make_grid(8), colors7),
    lut_range=[0.0, 1.0],
)

# One last thing before launching visualization: we will assign a toggle key for each
# widget and also set a larger point size.
for i, widget in enumerate(viewer.widgets()):
    widget.key = str(i + 1)
    widget.point_size = 4

viewer.run()
