#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import tviewer as tv
viewer = tv.TViewer()


def cb(**kwargs):
    print('this is an example callback, kwargs =', kwargs)


def get_model_cloud(**kwargs):
    print(kwargs)
    num_points = 10 * int(kwargs["index"])
    return np.random.rand(num_points, 3), np.arange(0, num_points, dtype=np.int32)


trigger = viewer.add_trigger("trigger", key="space", callback=cb)
toggle = viewer.add_toggle("toggle", key="w")
index = viewer.add_spinner("index", key="b", value=10, range=(0, 15))
model = viewer.add_point_cloud("model", key="m", dependencies="index", data=get_model_cloud, point_size=10, viewport=2)
boxes = viewer.add_primitive_objects("boxes", key="b", viewport=1)
#  boxes.data = [tv.Box([0, 0, 0], [0.2, 0.2, 0.2])]
boxes.data = [tv.Box([0, 0, 0], [0.2, 0.2, 0.2]), tv.Arrow([0, 0, 0], [1, 1, 1], tv.Color.Emerald)]

#  model.colors = [0.1, 0.4, 0.9]
#  model.colors = "y"

viewer.update("model")
viewer.add_viewport(0, 0, 0.5, 1)
viewer.add_viewport(0.5, 0, 1, 1)
viewer.run(keep_open=True)

#  viewer.update("model")
#  viewer.run()

if viewer["toggle"]:
    print("its true")
